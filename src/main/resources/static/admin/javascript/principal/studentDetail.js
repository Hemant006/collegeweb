var studentSanctionedResultDTOList = {};
var programList = [];
var instituteId = $("#sessionInstId").text();

var studentSanctionDetailDTOList = [];
var studentResultDetailDTOList = [];
var studentDetails = {};
var yearMasterDTOList = {};

function fetchStudentSanctionResultdetail(applicationId) {
	$.ajax({
		type: "GET",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		url: "/student/"+applicationId,
		headers: {
			'API-VERSION': 1
		},
		success: function (data) {
			if (data.success == 1) {
				/* console.log("fetched Data" + JSON.stringify(data)); */
				studentSanctionedResultDTOList = data.serviceResult;
				programList = studentSanctionedResultDTOList.programMasterDTOList;
				studentDetails = studentSanctionedResultDTOList.studentSanctionDetailDTOList;
				yearMasterDTOList = studentSanctionedResultDTOList.yearMasterDTOList;
				
				
				for (var i = 0; i < yearMasterDTOList.length; i++) {
					$("#studentSanctionedDiv").append(`<tr>
							 	<td>${yearMasterDTOList[i].displayYear}</td>
								
								<td>
								<input type="text" class="form-control" id="totalSanction${i}"  min="0" style="text-align: right;"
									name="totalSanction${i}"  value="100" onkeypress="return isNumber(event)" onkeyup="getTotalStudent(this.id)" >
									<div class="error" id="totalSanctionError${i}" style="color: red;"></div>
								</td>

								<td>
								<input type="text" class="form-control" id="totalEnrolled${i}"  min="0" style="text-align: right;"
									name="totalEnrolled${i}"  value="0" onkeypress="return isNumber(event)" onkeyup="getTotalStudent(this.id)">
									<div class="error" id="totalEnrolledError${i}" style="color: red;"></div>
								</td>

								

								<td>
								<input type="text" class="form-control boys" id="noOfBoys${i}"  min="0" style="text-align: right;"
									name="noOfBoys${i}"  value="0" onkeypress="return isNumber(event)" onkeyup="getTotalStudent(this.id)">
									<div class="error" id="noOfBoysError${i}" style="color: red;"></div>
								</td>

								<td>
								<input type="text" class="form-control girls" id="noOfGirls${i}"  min="0" style="text-align: right;"
									name="noOfGirls${i}"  value="0" onkeypress="return isNumber(event)" onkeyup="getTotalStudent(this.id)" >
									<div class="error" id="noOfGirlsError${i}" style="color: red;"></div>
								</td>

								


								<td>
								<input type="text" class="form-control" id="totalPresent${i}"  min="0" style="text-align: right;"
									name="totalPresent${i}"  value="0" onkeypress="return isNumber(event)" onkeyup="getTotalStudent(this.id)" >
									<div class ="error" id="totalPresentError${i}" style="color: red;"></div>
								</td>
								<td>
								<input type="text" class="form-control" id="totalPassed${i}"  min="0" style="text-align: right;"
									name="totalPassed${i}"  value="0" onkeypress="return isNumber(event)" onkeyup="getTotalStudent(this.id)" >
									<div class="error" id="totalPassedError${i}" style="color: red;"></div>
								</td>

								<td>
								<input type="text" class="form-control" id="percentage${i}"  min="0" style="text-align: right;"
									name="percentage${i}"  value="0" onkeypress="return isNumber(event)" onkeyup="getTotalStudent(this.id)">
									
								</td>
								<td hidden="">
								<input type="hidden" id="hiddenStudentSanctionId${i}" value="0"
									name="hiddenStudentSanctionId${i} ">
								</td>
								<td hidden="">
								<input type="hidden" id="hiddenSanctionYearId${i}" value="${i + 1}"
									name="hiddenSanctionYearId${i} ">
								</td>
								</tr>`);

				}


				//fillStudentDataTable(studentDetails);
					if(studentDetails!=null){
						console.log("studentDetails"+JSON.stringify(studentDetails));
						for (var i = 0; i < studentDetails.length; i++) {
							$("#hiddenStudentSanctionId"+i).val(studentDetails[i].id);
							$("#totalSanction"+i).val(studentDetails[i].totalSanctioned);
							$("#totalEnrolled"+i).val(studentDetails[i].totalEnrolled);
							$("#noOfBoys"+i).val(studentDetails[i].noOfBoys);
							$("#noOfGirls"+i).val(studentDetails[i].noOfGirls);
							$("#totalPresent"+i).val(studentDetails[i].totalPresent);
							$("#totalPassed"+i).val(studentDetails[i].totalPassed);
							$("#hiddenSanctionYearId"+i).val(studentDetails[i].instituteProgramMpgDTO.id);
							getTotalStudent("percentage"+i);
							
						}
					}

			}
			else {
				Swal.fire({
					title: 'ERROR',
					html: data.message,
					icon: 'error',
					confirmButtonColor: '#3085d6',
					confirmButtonText: "OK",
					closeOnConfirm: false,
					allowOutsideClick: false,
				}).then((result) => {
					if (result.value) {

					}
				})

			}
		},
		error: function (data, status, error) {
			console.log("error " + error);
			console.log("messageList " + JSON.stringify(data.responseJSON.messageList));
			console.log("data " + JSON.stringify(data));
			console.log("status " + status);
			showAlerts(data.responseJSON.message, data.statusText, "alertMessage", false, data.responseJSON.serviceResult, 20000);
			if (data.status == "401") {
				showAlerts(data.responseJSON.message, data.statusText, "alertMessage", false, data.responseJSON.serviceResult, 20000);
			}
			else if (data.status == "500") {
				showAlerts(data.responseJSON.message, data.statusText, "alertMessage", false, data.responseJSON.serviceResult, 20000);
			}
		}
	});
}



function saveStudentDetail() {

	for (var i = 0; i < 3; i++) {
		var sanction = {

			id: document.getElementById("hiddenStudentSanctionId" + i).value,
			totalSanctioned: document.getElementById("totalSanction" + i).value,
			totalEnrolled: document.getElementById("totalEnrolled" + i).value,
			noOfBoys: document.getElementById("noOfBoys" + i).value,
			noOfGirls: document.getElementById("noOfGirls" + i).value,
			totalPresent: document.getElementById("totalPresent" + i).value,
			totalPassed: document.getElementById("totalPassed" + i).value,
			instId: instituteId,
			yearMasterDTO: {
				id: document.getElementById("hiddenSanctionYearId" + i).value
			},

		};


		studentSanctionDetailDTOList.push(sanction);
	}

	console.log("studentSanctionDetailDTOList " + JSON.stringify(studentSanctionDetailDTOList));

	var studentInstituteProgramMpgDTOList = {
		programMasterDTO: {
			id: 1
		},
		id: document.getElementById("hiddenStudentInstituteMpgId").value,
		yearMasterDTO: {
			id: 1
		},
	}

	var data = {
		studentSanctionDetailDTOList: studentSanctionDetailDTOList,
		studentInstituteProgramMpgDTO: studentInstituteProgramMpgDTOList
	}
	console.log("before save" + JSON.stringify(data));
	var verfied = true;
	if (verfied) {
		$.ajax({
			type: "POST",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			url: "/student/",
			headers: {
				'API-VERSION': 1
			},
			data: JSON.stringify(data),
			success: function (data) {
				console.log("DATA " + JSON.stringify(data));
				if (data.success == 1) {
					showAlerts(data.message, data.messageList.NoError, "alertMessage", false, data.serviceResult, 2000);
				
					cleanError('error');

				}
				else {
					Swal.fire({
						title: 'ERROR',
						html: data.message,
						icon: 'error',
						confirmButtonColor: '#3085d6',
						confirmButtonText: "OK",
						closeOnConfirm: false,
						allowOutsideClick: false,
					}).then((result) => {
						if (result.value) {

						}
					})
				}
			},
			error: function (data, status, error) {
				console.log("error " + error);
				console.log("messageList " + JSON.stringify(data.responseJSON.messageList));
				console.log("data " + JSON.stringify(data));
				console.log("status " + status);
				showAlerts(data.responseJSON.message, data.statusText, "alertMessage", true, data.responseJSON.serviceResult, 20000);
				if (data.status == "417") {
					//alert("417");
					var messageList = data.responseJSON.messageList;
					let myMap = new Map();
					myMap = messageList;
					console.log("myMap" + JSON.stringify(myMap));
					for (const [key, value] of Object.entries(myMap)) {
						//console.log(`${key}: ${value}`);
						// console.log("Key "+key );
						//console.log("value "+value );
						document.getElementById("" + key).innerHTML = value;
					}
					showAlerts(data.responseJSON.message, data.statusText, "alertMessage", true, data.responseJSON.serviceResult, 20000);
				}
				else if (data.status == "409") {
					alert("Constraint Violation Error");
					let contraintViolationMsg = data.responseJSON.message.split('for key');
					console.log("words" + contraintViolationMsg[0]);
					showAlerts(contraintViolationMsg[0], data.statusText, "alertMessage", true, data.responseJSON.serviceResult, 20000);
				}
				else {
					Swal.fire({
						title: 'ERROR',
						html: "Something Went Wrong, Please Try Again",
						icon: 'error',
						confirmButtonColor: '#3085d6',
						confirmButtonText: "OK",
						closeOnConfirm: false,
						allowOutsideClick: false,
					}).then((result) => {
						if (result.value) {

						}
					})
				}
			}
		});
	}

}

function getTotalStudent(id) {
	var rowId = id[id.length - 1];
	var totalSanctionedval = $("#totalSanction" + rowId).val();

	$("#totalEnrolled" + rowId).val(totalSanctionedval);
	/* $("#totStudent" + rowId).val(totalSanctionedval); */

	var totalStudent = parseInt($("#noOfBoys" + rowId).val()) + parseInt($("#noOfGirls" + rowId).val());
	var totalEnroll = $("#totalEnrolled" + rowId).val();
	var totPresent = $("#totalPresent" + rowId).val()
	var totPass = $("#totalPassed" + rowId).val()

	var studentPercentage = ((totPass / totPresent) * 100).toFixed(0);

	if (totalEnroll == "") {
		$("#totalEnrolled" + rowId).val("0");
	} else if (totalStudent > totalEnroll) {
		alert("Total Student should not be grater than enrollled students")
		$("#noOfBoys" + rowId).val("0");
		$("#noOfGirls" + rowId).val("0");
		$("#totalPresent" + rowId).val("0");
		/* $("#totStudent" + rowId).val($("#totalEnrolled" + rowId).val()); */
	} else if (totPresent > totalStudent) {
		alert("Peresent Student should not be grater than total students")
		$("#totalPresent" + rowId).val(totalStudent);
		$("#totalPassed" + rowId).val("");
		$("#percentage" + rowId).val("");
	} else {
		$("#totalPresent" + rowId).val(totalStudent);
		if (totPass != 0) {
			$("#percentage" + rowId).val(studentPercentage + "%");
		}

	}

}



function fillStudentDataTable(data) {
	console.log("data " + JSON.stringify(data));
	if(data != null){
		$("#nextBtn").prop("disabled",false);
	}else{
		$("#nextBtn").prop("disabled",true);
	}

	var table = $('#student_result_data_table').DataTable({
		"columnDefs": [{
			"className": "dt-center",
			"targets": "_all"
		}],
		"autoWidth": true,
		"processing": true,
		"data": data,
		"iDisplayLength": 50,
		"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		destroy: true,
		"order": [[0, "asc"]],
		rowsGroup: [

			1, 9

		],

		"fnCreatedRow": function (row, data, index) {
			$('td', row).eq(0).html(index + 1);
		},
		bAutoWidth: false,

		columnDefs: [{
			"targets": 9,
			'orderable': false,
			render: function (data, type, row, meta) {

				var studentSanctionId = row.instituteProgramMpgDTO.id;

				return "<a "
					+ " target='_blank'><i class='fa fa-pencil fatbl text-info' onclick='fillStudentFormData(" + studentSanctionId + ")'></i> <i class='fa fa-trash fatbl text-info' onclick='deleteStudentData(" + studentSanctionId + ")'></i>"
					+ "</a>"
			}
		}],
		"columns": [
			{
				"data": null,

			},
			{
				"data": "instituteProgramMpgDTO.programMasterDTO.name"
			},
			{
				"data": "yearMasterDTO.displayYear"
			},
			{
				"data": "totalSanctioned"
			},
			{
				"data": "totalEnrolled"
			},
			{
				"data": "noOfBoys",

			}, {
				"data": "noOfGirls"
			}, {

				"data": "totalPresent"
			}, {
				"data": "totalPassed"
			},/* {
						mRender: function (data, type, row) {
							var studentSanctionId = row.instituteProgramMpgDTO.id;

							return "<a "
								+ " target='_blank'><i class='fa fa-pencil fatbl text-info' onclick='fillStudentFormData(" + studentSanctionId + ")'></i> <i class='fa fa-trash fatbl text-info' onclick='deleteStudentData(" + studentSanctionId + ")'></i>"
								+ "</a>"

						}
					} */
			{
				"data": "totalPassed",
				"title": "action"
			},

		]
	});
}


function fillStudentFormData(studentDetailId) {
if(studentDetails != null){
			student = studentDetails.filter((x) => x.instituteProgramMpgDTO.id == studentDetailId);


		for (var i = 0; i < student.length; i++) {
			$("#hiddenStudentSanctionId"+i).val(student[i].id);
			$("#totalSanction"+i).val(student[i].totalSanctioned);
			$("#totalEnrolled"+i).val(student[i].totalEnrolled);
			$("#noOfBoys"+i).val(student[i].noOfBoys);
			$("#noOfGirls"+i).val(student[i].noOfGirls);
			$("#totalPresent"+i).val(student[i].totalPresent);
			$("#totalPassed"+i).val(student[i].totalPassed);
			$("#hiddenSanctionYearId"+i).val(student[i].instituteProgramMpgDTO.id);
		}
	}
}

function deleteStudentData(id) {
	Swal.fire({
		title: 'Are you sure?',
		html: "You want to Delete Trust Details",
		icon: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		confirmButtonText: "YES",
		cancelButtonColor: '#d33',
		cancelButtonText: "Cancel",
		closeOnConfirm: false,
		closeOnCancel: false,
		allowOutsideClick: false
	}).then((result) => {
		if (result.value) {
			if (id != 0) {
				$.ajax({
					type: "DELETE",
					url: "/student/",
					headers: {
						'API-VERSION': 1
					},
					data: {studentDetailMapId: id},
					success: function (data) {
						if (data.success == 1) {
							$("#message4").removeClass('hide').addClass('alert alert-success alert-dismissible').slideDown().show();
							$().show();
							setTimeout(function () { $("#message4").hide(); location.reload() }, 3000);
							$('html,body').animate({ scrollTop: $("#message4").offset().top }, 'slow');
						} else {
							Swal.fire({
								title: 'ERROR',
								html: data.message,
								icon: 'error',
								confirmButtonColor: '#3085d6',
								confirmButtonText: "OK",
								closeOnConfirm: false,
								allowOutsideClick: false,
							}).then((result) => {
								if (result.value) {

								}
							})

						}
					},
					error: function (error) {
						Swal.fire({
							title: 'ERROR',
							html: "Something Went Wrong, Please Try Again",
							icon: 'error',
							confirmButtonColor: '#3085d6',
							confirmButtonText: "OK",
							closeOnConfirm: false,
							allowOutsideClick: false,
						}).then((result) => {
							if (result.value) {

							}
						})

					}
				});

			}
		}

	})
}
$("#nextBtn").click(function () {
	var applicationId =  $("#applicationId").val();
	//alert(applicationId);
	var encryptedAES = CryptoJS.AES.encrypt("applicationId="+applicationId, "My Secret Passphrase");		  
	  	location.href="/staff/home?"+encryptedAES;
});