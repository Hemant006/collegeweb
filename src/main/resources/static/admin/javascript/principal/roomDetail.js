var roomList = [];
var roomDetailDTOList=[];
function fetchRoomDetail(applicationId) {

	$.ajax({
		type: "GET",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		url: "/room/" + applicationId,
		headers: {
			'API-VERSION': 1
		},
		success: function (data) {
			if (data.success == 1) {
				console.log("Room Data" + JSON.stringify(data));
				roomList = data.serviceResult;
				roomDetailDTOList =data.serviceResult.roomDetailDTOList;
				var j = 0;
				var count = 1;
				for (i = 0; i < roomList.lookupTypeDTOList.length; i++) {
					if (roomList.lookupTypeDTOList[i].description == "1") {
						$("#roomDetailDiv").append(`<tr>
									 <td class="text-center">${count}</td>
									<td class="text-center">
									<p> <b>${roomList.lookupTypeDTOList[i].name}</b>
									</p>
									</td>
									<td class="text-center">
									<input type="text" class="form-control" id="instAnswer${i}"  min="0" style="text-align: right;"
										name="instAnswer${i}"  value="0" onkeypress="return isNumber(event)" maxlength="10" >
										<div id="instAnswerAlert${i}" style="color: red;"></div>
									</td>
									<td class="text-center">
									<input type="text" class="form-control" id="roomArea${i}"  min="0" style="text-align: right;"
										name="roomArea${i}"  value="0" onkeypress="return (event.charCode > 47 && event.charCode < 58) || (event.charCode ==46);"  maxlength="10">
										<div id="roomAreaAlert${i}" style="color: red;"></div>
									</td>
									<td class="text-center">
									<input type="text" class="form-control" id="roomRemark${i}"  style="text-align: right;"
										name="roomRemark${i}"  placeholder="Enter Remark"   maxlength="255">
										<div id="roomRemarkAlert${i}" style="color: red;"></div>
									</td>
									<td hidden="">
									<input type="hidden" id="hiddenRoomTransMpgId${i}" value="0"
										name="hiddenRoomTransMpgId${i} ">
									</td>
									</tr>`);
						count++;
					}
					else {
						$("#roomDetailDiv2").append(`<tr>
								 	<td class="text-center">${1}</td>
									<td class="text-center">
									<p> <b>${roomList.lookupTypeDTOList[i].name}</b>
									</p>
									</td>
									<td class="text-center"> 
									<div class="form-check-inline">
								  	<label class="form-check-label">
								    <input type="radio" class="form-check-input"  name="libDetail${i}" id="instAnswerYes${i}" value="1" checked> Yes
								  	</label>
									</div>
									
									<div class="form-check-inline">
								 	 <label class="form-check-label">
								    <input type="radio" class="form-check-input " name="libDetail${i}" id="instAnswerNo${i}" value="0"> No
								  </label>
								</div>
								</td>
									<td class="text-center">
									<input type="text" class="form-control" id="roomRemark${i}"   style="text-align: right;"
										name="roomRemark${i}"  placeholder="Enter Remark">
										<div id="roomRemarkAlert${i}" style="color: red;"></div>
									</td>
									<td hidden="">
									<input type="hidden" id="hiddenRoomTransMpgId${i}" value="0"
										name="hiddenRoomTransMpgId${i} ">
									</td>
									</tr>`);
						j++;
					}
				}
				$("#compLibraryDetailDiv").append(`<tr>
							</td>
							<td hidden="">
							<input type="hidden" id="hiddenRemark" value="0"
								name="hiddenRemark ">
							</td>
							</tr>`);

			/* 	console.log("roomList.lookupTypeDTOList" + JSON.stringify(roomList.roomDetailDTOList)); */
				if (roomList.roomDetailDTOList != null) {
					for (var k = 0; k < roomList.roomDetailDTOList.length; k++) {

						if (roomList.roomDetailDTOList[k].value == 1) {
							$("#instAnswerYes" + k).prop("checked", true);
						}
						if (roomList.roomDetailDTOList[k].value == 0) {
							$("#instAnswerNo" + k).prop("checked", true);
						}
						$("#instAnswer" + k).val(roomList.roomDetailDTOList[k].value);
						$("#roomArea" + k).val(roomList.roomDetailDTOList[k].area);
						$("#roomRemark" + k).val(roomList.roomDetailDTOList[k].remark);
						$("#hiddenRoomTransMpgId" + k).val(roomList.roomDetailDTOList[k].id);

					}
				}
			 	if (roomList.roomTransactionMpgDTO.id != null) {
					$("#hiddenRemark").val(roomList.roomTransactionMpgDTO.id);
					$("#saveBtn").prop("value", "UPDATE");
				} else {
					$("#nextBtn").prop("disabled", true);
				}

			}
			else {
				Swal.fire({
					title: 'ERROR',
					html: data.message,
					icon: 'error',
					confirmButtonColor: '#3085d6',
					confirmButtonText: "OK",
					closeOnConfirm: false,
					allowOutsideClick: false,
				}).then((result) => {
					if (result.value) {

					}
				})

			}
		},
		error: function (data, status, error) {
			console.log("error " + error);
			console.log("messageList " + JSON.stringify(data.responseJSON.messageList));
			console.log("data " + JSON.stringify(data));
			console.log("status " + status);
			showAlerts(data.responseJSON.message, data.statusText, "alertMessage", false, data.responseJSON.serviceResult, 20000);
			if (data.status == "401") {
				showAlerts(data.responseJSON.message, data.statusText, "alertMessage", false, data.responseJSON.serviceResult, 20000);
			}
			else if (data.status == "500") {
				showAlerts(data.responseJSON.message, data.statusText, "alertMessage", false, data.responseJSON.serviceResult, 20000);
			}
		}
	});
}
function saveRoomDetail() {
	var roomDetaillist = [];
	var answer = null;
	var area = null;
	var remark = null;
	for (var i = 0; i < roomList.lookupTypeDTOList.length; i++) {
		var lableId = roomList.lookupTypeDTOList[i].id;
		console.log("labled " + lableId);
		//alert("i"+i);
		//var x = document.getElementsByName("libDetail"+i).length;
		//alert("x"+JSON.stringify(x));
		if (document.getElementsByName("libDetail" + i).length == 0) {
			if (document.getElementById("instAnswer" + i).value != null) {
				answer = document.getElementById("instAnswer" + i).value;
			}
			if (document.getElementById("roomArea" + i).value != null) {
				area = document.getElementById("roomArea" + i).value;
			}
			if (document.getElementById("roomRemark" + i).value != null) {
				remark = document.getElementById("roomRemark" + i).value;
			}
		} else {
			if (document.getElementById("instAnswerYes" + i).checked == true) {
				answer = "1";
			} else if (document.getElementById("instAnswerNo" + i).checked == true) {
				answer = "0";
		}
			/* if (document.getElementById("roomArea" + i).value != null) {
				area = document.getElementById("roomArea" + i).value;
			} */
			if (document.getElementById("roomRemark" + i).value != null) {
				remark = document.getElementById("roomRemark" + i).value;
			}
		}
		//alert("answer"+answer);
		data = {
			id: document.getElementById("hiddenRoomTransMpgId" + i).value,
			value: answer,
			area : area,
			remark :remark,
			lookupTypeDTO: {
				id: lableId
			}
		};
		//InstituteUtilizedDTOList.push(data);
		roomDetaillist.push(data);
		console.log("List " + JSON.stringify(roomDetaillist));

	}
	var roomTransactionMpgDTO = {
		id: document.getElementById("hiddenRemark").value
	}

	var customRoomDetailDTO = {
		roomDetailDTOList: roomDetaillist,
		roomTransactionMpgDTO: roomTransactionMpgDTO
	}
	/* console.log("before save" + JSON.stringify(customRoomDetailDTO));
 */	var verfied = true;
	if (verfied) {
		$.ajax({
			type: "POST",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			url: "/room/",
			headers: {
				'API-VERSION': 1
			},
			data: JSON.stringify(customRoomDetailDTO),
			success: function (data) {
				console.log("DATA " + JSON.stringify(customRoomDetailDTO));
				if (data.success == 1) {
					showAlerts(data.message, data.messageList.NoError, "alertMessage", false, data.serviceResult, 2000);
					cleanError('error');

				}
				else {
					Swal.fire({
						title: 'ERROR',
						html: data.message,
						icon: 'error',
						confirmButtonColor: '#3085d6',
						confirmButtonText: "OK",
						closeOnConfirm: false,
						allowOutsideClick: false,
					}).then((result) => {
						if (result.value) {

						}
					})
				}
			},
			error: function (data, status, error) {
				console.log("error " + error);
				console.log("messageList " + JSON.stringify(data.responseJSON.messageList));
				console.log("data " + JSON.stringify(data));
				console.log("status " + status);
				showAlerts(data.responseJSON.message, data.statusText, "alertMessage", true, data.responseJSON.serviceResult, 20000);
				if (data.status == "417") {
					alert("417");
					var messageList = data.responseJSON.messageList;
					let myMap = new Map();
					myMap = messageList;
					console.log("myMap" + JSON.stringify(myMap));
					for (const [key, value] of Object.entries(myMap)) {
						//console.log(`${key}: ${value}`);
						// console.log("Key "+key );
						//console.log("value "+value );
						document.getElementById("" + key).innerHTML = value;
					}
					showAlerts(data.responseJSON.message, data.statusText, "alertMessage", true, data.responseJSON.serviceResult, 20000);
				}
				else if (data.status == "409") {
					alert("Constraint Violation Error");
					let contraintViolationMsg = data.responseJSON.message.split('for key');
					console.log("words" + contraintViolationMsg[0]);
					showAlerts(contraintViolationMsg[0], data.statusText, "alertMessage", true, data.responseJSON.serviceResult, 20000);
				}
				else {
					Swal.fire({
						title: 'ERROR',
						html: "Something Went Wrong, Please Try Again",
						icon: 'error',
						confirmButtonColor: '#3085d6',
						confirmButtonText: "OK",
						closeOnConfirm: false,
						allowOutsideClick: false,
					}).then((result) => {
						if (result.value) {

						}
					})
				}
			}
		});
	}
}
$("#nextBtn").click(function () {
	var applicationId = $("#applicationId").val();
	//alert(applicationId);
	var encryptedAES = CryptoJS.AES.encrypt("applicationId=" + applicationId, "My Secret Passphrase");
	location.href = "/fee/home?" + encryptedAES;
});
$("#btnBack").click(function () {
	var applicationId = $("#applicationId").val();
	//alert(applicationId);
	var encryptedAES = CryptoJS.AES.encrypt("applicationId=" + applicationId, "My Secret Passphrase");
	location.href = "/physicalEducationalFacilities/home?" + encryptedAES;
});