
var educationalPhysicalFacilitiesList = [];
var educationalPhyicalFacilityDetailDTOList = [];
	function fetchEducationPhyicalDetail(applicationId) {
		
	$.ajax({
		type: "GET",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		url: "/physicalEducationalFacilities/"+applicationId,
		headers: {
			'API-VERSION': 1
		},
		success: function (data) {
			if (data.success == 1) {
				console.log("Library Data" + JSON.stringify(data));
				educationalPhysicalFacilitiesList = data.serviceResult;
				educationalPhyicalFacilityDetailDTOList= data.serviceResult.educationalPhyicalFacilityDetailDTOList
				var j = 0;
				for (i = 0; i < educationalPhysicalFacilitiesList.lookupTypeDTOList.length; i++) {
					if (educationalPhysicalFacilitiesList.lookupTypeDTOList[i].description == "1") {
						$("#infrastrctureDetailDiv").append(`<tr>
								 	<td>${i + 1}</td>
									<td>
									<p> <b>${educationalPhysicalFacilitiesList.lookupTypeDTOList[i].name}</b>
									</p>
									</td>
									<td>
									<input type="text" class="form-control" id="noOfRooms${i}"  min="0" style="text-align: right;"value="0"  maxlength="10"
										name="noOfRooms${i}"  onkeypress="return isNumber(event)" placeholder="${educationalPhysicalFacilitiesList.lookupTypeDTOList[i].placeholder}">
										<div id="noOfRoomsAlert${i}" style="color: red;"></div>
									</td>
									<td>
									<input type="text" class="form-control" id="totalArea${i}"  min="0" style="text-align: right;" value="0"  maxlength="10"
										name="totalArea${i}"  onkeypress="return (event.charCode > 48 && event.charCode < 57) || (event.charCode ==46);" placeholder="${educationalPhysicalFacilitiesList.lookupTypeDTOList[i].placeholder}">
										<div id="totalAreaAlert${i}" style="color: red;"></div>
									</td>
									<td hidden="">
									<input type="hidden" id="hiddenInfraTransMpgId${i}" value="0"
										name="hiddenInfraTransMpgId${i} ">
									</td>
									</tr>`);
					}
				}
				 $("#compInfrastrctureDetailDiv").append(`<tr>
						  	
							<td hidden="">
							<input type="hidden" id="hiddenRemark" value="0"
								name="hiddenRemark ">
							</td>
							</tr>`); 
				if (educationalPhysicalFacilitiesList.educationalPhyicalFacilityDetailDTOList != null) {
					for (var k = 0; k < educationalPhysicalFacilitiesList.educationalPhyicalFacilityDetailDTOList.length; k++) {
						/* alert(JSON.stringify(educationalPhysicalFacilitiesList.libraryDetailDTOlist[k].answer)); */
						$("#noOfRooms" + k).val(educationalPhysicalFacilitiesList.educationalPhyicalFacilityDetailDTOList[k].noOFRooms);
						$("#totalArea" + k).val(educationalPhysicalFacilitiesList.educationalPhyicalFacilityDetailDTOList[k].totalSquareMeter);
						$("#hiddenInfraTransMpgId"+k).val(educationalPhysicalFacilitiesList.educationalPhyicalFacilityDetailDTOList[k].id);
					}
				}
				if(educationalPhyicalFacilityDetailDTOList.length ==0){
					$("#nextBtn").prop("disabled", true);
				}else{
					$("#saveBtn").prop("value", "UPDATE");
				}
				/* if (educationalPhysicalFacilitiesList.infrastructureTransactionMpgDTO.length != 0) {
					$("#remark").val(educationalPhysicalFacilitiesList.infrastructureTransactionMpgDTO.remark);
				} */
			}
			else {
				Swal.fire({
					title: 'ERROR',
					html: data.message,
					icon: 'error',
					confirmButtonColor: '#3085d6',
					confirmButtonText: "OK",
					closeOnConfirm: false,
					allowOutsideClick: false,
				}).then((result) => {
					if (result.value) {

					}
				})

			}
		},
		error: function (data, status, error) {
			console.log("error " + error);
			console.log("messageList " + JSON.stringify(data.responseJSON.messageList));
			console.log("data " + JSON.stringify(data));
			console.log("status " + status);
			showAlerts(data.responseJSON.message, data.statusText, "alertMessage", false, data.responseJSON.serviceResult, 20000);
			if (data.status == "401") {
				showAlerts(data.responseJSON.message, data.statusText, "alertMessage", false, data.responseJSON.serviceResult, 20000);
			}
			else if (data.status == "500") {
				showAlerts(data.responseJSON.message, data.statusText, "alertMessage", false, data.responseJSON.serviceResult, 20000);
			}
		}
	});
}
function saveEducationalPhyFacilitiesDetail() {
	var educationalPhysicalDetaillist = [];
	var noOfRoomsAnswer = null;
	var totalArea =null;
	for (var i = 0; i < educationalPhysicalFacilitiesList.lookupTypeDTOList.length; i++) {
		var lableId = educationalPhysicalFacilitiesList.lookupTypeDTOList[i].id;
		if (document.getElementsByName("infraDetail"+i).length == 0) {
			//alert(document.getElementById("noOfRooms"+i).value);
			if (document.getElementById("noOfRooms"+i).value != null) {
				noOfRoomsAnswer = document.getElementById("noOfRooms" + i).value;
			}

			if (document.getElementById("totalArea"+i).value != null) {
				totalArea = document.getElementById("totalArea"+i).value;
			}
		} 
		data = {
			id: document.getElementById("hiddenInfraTransMpgId"+i).value,
			noOFRooms: noOfRoomsAnswer,
			totalSquareMeter:totalArea,
			lookupTypeDTO: {
				id: lableId
			}
		};
		//InstituteUtilizedDTOList.push(data);
		educationalPhysicalDetaillist.push(data);
		console.log("List " + JSON.stringify(educationalPhysicalDetaillist));

	}
	var educationalPhysicalFacilitiesTransactionMpgDTO = {
		id: document.getElementById("hiddenRemark").value,
	}

	var customEducationalPhysicalDetailDTO = {
		educationalPhyicalFacilityDetailDTOList: educationalPhysicalDetaillist,
		educationalPhyicalFacilityTransactionMpgDTO: educationalPhysicalFacilitiesTransactionMpgDTO
	}
	console.log("before save" + JSON.stringify(customEducationalPhysicalDetailDTO));
	var verfied = true;
	if (verfied) {
		$.ajax({
			type: "POST",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			url: "/physicalEducationalFacilities/",
			headers: {
				'API-VERSION': 1
			},
			data: JSON.stringify(customEducationalPhysicalDetailDTO),
			success: function (data) {
				console.log("DATA " + JSON.stringify(customEducationalPhysicalDetailDTO));
				if (data.success == 1) {
					showAlerts(data.message, data.messageList.NoError, "alertMessage", false, data.serviceResult, 2000);

					cleanError('error');

				}
				else {
					Swal.fire({
						title: 'ERROR',
						html: data.message,
						icon: 'error',
						confirmButtonColor: '#3085d6',
						confirmButtonText: "OK",
						closeOnConfirm: false,
						allowOutsideClick: false,
					}).then((result) => {
						if (result.value) {

						}
					})
				}
			},
			error: function (data, status, error) {
				console.log("error " + error);
				console.log("messageList " + JSON.stringify(data.responseJSON.messageList));
				console.log("data " + JSON.stringify(data));
				console.log("status " + status);
				showAlerts(data.responseJSON.message, data.statusText, "alertMessage", true, data.responseJSON.serviceResult, 20000);
				if (data.status == "417") {
					alert("417");
					var messageList = data.responseJSON.messageList;
					let myMap = new Map();
					myMap = messageList;
					console.log("myMap" + JSON.stringify(myMap));
					for (const [key, value] of Object.entries(myMap)) {
						//console.log(`${key}: ${value}`);
						// console.log("Key "+key );
						//console.log("value "+value );
						document.getElementById("" + key).innerHTML = value;
					}
					showAlerts(data.responseJSON.message, data.statusText, "alertMessage", true, data.responseJSON.serviceResult, 20000);
				}
				else if (data.status == "409") {
					alert("Constraint Violation Error");
					let contraintViolationMsg = data.responseJSON.message.split('for key');
					console.log("words" + contraintViolationMsg[0]);
					showAlerts(contraintViolationMsg[0], data.statusText, "alertMessage", true, data.responseJSON.serviceResult, 20000);
				}
				else {
					Swal.fire({
						title: 'ERROR',
						html: "Something Went Wrong, Please Try Again",
						icon: 'error',
						confirmButtonColor: '#3085d6',
						confirmButtonText: "OK",
						closeOnConfirm: false,
						allowOutsideClick: false,
					}).then((result) => {
						if (result.value) {

						}
					})
				}
			}
		});
	}
}
$("#nextBtn").click(function () {
	var applicationId =  $("#applicationId").val();
	//alert(applicationId);
	var encryptedAES = CryptoJS.AES.encrypt("applicationId="+applicationId, "My Secret Passphrase");		  
	location.href="/room/home?"+encryptedAES;
});
$("#btnBack").click(function () {
	var applicationId = $("#applicationId").val();
	//alert(applicationId);
	var encryptedAES = CryptoJS.AES.encrypt("applicationId=" + applicationId, "My Secret Passphrase");
	location.href = "/infrastructure/home?" + encryptedAES;
});