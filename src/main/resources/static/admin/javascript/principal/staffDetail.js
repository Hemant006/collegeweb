var cIndex = null;
$("#nextBtn").click(function () {
    var applicationId = $("#applicationId").val();
    //alert(applicationId);
    var encryptedAES = CryptoJS.AES.encrypt("applicationId=" + applicationId, "My Secret Passphrase");
    location.href = "/lib/home?" + encryptedAES;
});
$("#btnBack").click(function () {
    var applicationId = $("#applicationId").val();
    //alert(applicationId);
    var encryptedAES = CryptoJS.AES.encrypt("applicationId=" + applicationId, "My Secret Passphrase");
    location.href = "/student/home?" + encryptedAES;
});
var eduList = [];
var degreeList = [];
var streamList = [];
var subjectList = [];
var curriculumList = [];
var staffEducationQualificationDTOList = [];
var ans = {};
function getData(json) {

    if (json.serviceResult.educationMasterDTOList instanceof Array)
        this.eduList = this.eduList.concat(json.serviceResult.educationMasterDTOList);
    // console.log("eduList len:"+eduList.length);
    if (json.serviceResult.educationMasterDTOList instanceof Array)
        this.degreeList = this.degreeList.concat(json.serviceResult.degreeMasterDTOList);
    if (json.serviceResult.streamMasterDTOList instanceof Array)
        this.streamList = this.streamList.concat(json.serviceResult.streamMasterDTOList);
    if (json.serviceResult.subjectMasterDTOList instanceof Array)
        this.subjectList = this.subjectList.concat(json.serviceResult.subjectMasterDTOList);
    if (json.serviceResult.curriculumMasterDTOList instanceof Array)
        this.curriculumList = this.curriculumList.concat(json.serviceResult.curriculumMasterDTOList);
    $("#educationLevel").append(new Option("--Select--", ""));
    initilizeDefaultValue("degree");
    initilizeDefaultValue("stream");
    initilizeDefaultValue("subject");
    $("#educationLevel option:first-child").attr("disabled", "disabled");
    for (var i = 0; i < eduList.length; i++) {
        $("#educationLevel").append(new Option(eduList[i].name, eduList[i].id));
    }
    //fetchStaffDetail(applicationId);
}
$(document).ready(function () {

    $("#curricuram").append(new Option("--Select--", ""));
    fututredatedisableFun('reliveDate');
    $("#educationLevel").change(function () {
        var text = "";
        if (educationLevel != null) {
            document.getElementById("educationLevelError").innerHTML = text;
        }
        onEduChange();
    })
    $("#degree").change(function () {
        var text = "";
        if (degree != null) {
            document.getElementById("degreeError").innerHTML = text;
        }
        degreeChange();
    })
    $("#stream").change(function () {
        var text = "";
        if (stream != null) {
            document.getElementById("streamError").innerHTML = text;
        }
        streamChange();
    })
    $("#subject").change(function () {
        var text = "";
        if (subject != null) {
            document.getElementById("subjectError").innerHTML = text;
        }
    })
    $("#staffType").change(function () {
        var text = "";
        if (staffType != null) {
            document.getElementById("staffTypeError").innerHTML = text;
        }
        staffChange();
    })
    $("#percentage").change(function () {
        var text = "";
        if (percentage != null) {
            document.getElementById("percentageError").innerHTML = text;
        }
    })
});
function initilizeDefaultValue(id) {
    $("#" + id).empty();
    $("#" + id).append(new Option("--Select--", ""));
    $("#" + id + " option:first-child").attr("disabled", "disabled");
}
function onEduChange() {
    var id1 = $("#educationLevel").val();

    initilizeDefaultValue("degree");
    initilizeDefaultValue("stream");
    initilizeDefaultValue("subject");
    var dataa = degreeList.filter((data1) => data1.educationLevelMasterDTO.id == id1);
    for (var i = 0; i < dataa.length; i++) {
        $("#degree").append(new Option(dataa[i].name, dataa[i].id));
    }
}
function degreeChange() {
    var id1 = $("#degree").val();

    initilizeDefaultValue("stream");
    initilizeDefaultValue("subject");
    var dataa = streamList.filter((data1) => data1.degreeMasterDTO.id == id1);
    // console.log(dataa.length);
    for (var i = 0; i < dataa.length; i++) {
        $("#stream").append(new Option(dataa[i].name, dataa[i].id));
    }
}
function streamChange() {
    var id1 = $("#stream").val();

    initilizeDefaultValue("subject");
    var dataa = subjectList.filter((data1) => data1.streamMasterDTO.id == id1);
    // console.log(dataa.length);
    for (var i = 0; i < dataa.length; i++) {
        $("#subject").append(new Option(dataa[i].name, dataa[i].id));
    }
}
function staffChange() {
    var id1 = $("#staffType").val();
    initilizeDefaultValue("curricuram");
    var dataa = curriculumList.filter((data1) => data1.id == id1);
    for (var i = 0; i < dataa.length; i++) {
        $("#curricuram").append(new Option(dataa[i].name, dataa[i].id));
    }
}
/* $(document).ready(function () {
fetchStaffdetail(applicationId);
}); */
var json = [];
function fetchStaffdetail(applicationId) {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: "/staff/" + applicationId,
        headers: {
            'API-VERSION': 1
        },
        success: function (data) {
            if (data.success == 1) {
                console.log("fetched Data" + JSON.stringify(data));
                getData(data);
                showdata(data.serviceResult.customMultipleStaffFetchDTOList);
                $("#educationQualificationDIV").hide();
            }
            else {
                console.log("wed");
                Swal.fire({
                    title: 'ERROR',
                    html: data.message,
                    icon: 'error',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "OK",
                    closeOnConfirm: false,
                    allowOutsideClick: false,
                }).then((result) => {
                    if (result.value) {

                    }
                })

            }
        },
        error: function (data, status, error) {
            // console.log("error " + error);
            // console.log("messageList " + JSON.stringify(data.responseJSON.messageList));
            // console.log("data " + JSON.stringify(data));
            // console.log("status " + status);
            showAlerts(data.responseJSON.message, data.statusText, "alertMessage", false, data.responseJSON.serviceResult, 20000);
            if (data.status == "401") {
                showAlerts(data.responseJSON.message, data.statusText, "alertMessage", false, data.responseJSON.serviceResult, 20000);
            }
            else if (data.status == "500") {
                showAlerts(data.responseJSON.message, data.statusText, "alertMessage", false, data.responseJSON.serviceResult, 20000);
            }
        }
    });
}

$("#addQualification").click(function () {

    var educationLevel = $("#educationLevel").val();
    var percentage = $("#percentage").val();
    var subject = $("#subject").val();
    var stream = $("#stream").val();
    var degree = $("#degree").val();
    /* if(educationLevel==null || percentage==null || subject==null || stream==null || degree==null || percentage==''){
        alert("Enter Education Detail");
        return;
    } */
    var text = "";
    var isValidEducation = true;
    if (educationLevel == null) {
        isValidEducation = false;
        text = "Please Select Education Level";
        document.getElementById("educationLevelError").innerHTML = text;
    }

    if (stream == null) {
        isValidEducation = false;
        text = "Please Select Stream Level";
        document.getElementById("streamError").innerHTML = text;
    }
    if (degree == null) {
        isValidEducation = false;
        text = "Please Select Degree Level";
        document.getElementById("degreeError").innerHTML = text;
    }
    if (subject == null) {
        isValidEducation = false;
        text = "Please Select Subject Level";
        document.getElementById("subjectError").innerHTML = text;
    }
    if (percentage == null || percentage == '') {
        isValidEducation = false;
        text = "Please Enter Percentage";
        document.getElementById("percentageError").innerHTML = text;
    }
    var mapdata = {
        "id": $("#staffEducationQualificationDTOListId").val(),
        "educationLevelMasterDTO": {
            "id": educationLevel
        },
        "degreeMasterDTO": {
            "id": degree
        },
        "streamMasterDTO": {
            "id": stream
        },
        "subjectMasterDTO": {
            "id": subject
        },
        "percentage": percentage
    };
    if (cIndex != null) {

        staffEducationQualificationDTOList[cIndex] = mapdata;
    } else {
        staffEducationQualificationDTOList.push(mapdata);
    }
    if (isValidEducation) {
        $("#educationQualificationDIV").show();
        fillEducationQualificationData(staffEducationQualificationDTOList);
    }

    cIndex = null;
    $("#percentage").val('');
    $("#educationLevel").val("");
    initilizeDefaultValue("degree");
    initilizeDefaultValue("stream");
    initilizeDefaultValue("subject");

});

function fillEducationQualificationData(data) {

    $('#fillEducationQualificationData')
        .DataTable(
            {

                // "columnDefs": [{
                // 	"className": "dt-center",
                // 	// "targets": 6,
                // 	// render : function(datas, type, row, meta) {



                // 	// 	let testdata = JSON.stringify(row).replace(/"/g, '\\"');


                // 	// 	var	action = '<i class="fa fa-edit fatbl text-info" data-toggle="tooltip" title="Edit Department" style="font-size:20px;" onclick=\'updateField("'+testdata+'")\'>'+
                // 	// 		'</i>';


                // 	// 	return action;
                // 	// }
                // }],
                "autoWidth": true,
                "processing": true,
                data: data,
                "iDisplayLength": 50,
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                destroy: true,
                "order": [[0, "asc"]],

                "fnCreatedRow": function (row, datas, index) {

                    var edudata = eduList.find(edu => edu.id == datas.educationLevelMasterDTO.id);
                    var degreedata = degreeList.find(degreeu => degreeu.id == datas.degreeMasterDTO.id);
                    var streamdata = streamList.find(streamu => streamu.id == datas.streamMasterDTO.id);
                    var subjectdata = subjectList.find(subjectu => subjectu.id == datas.subjectMasterDTO.id);
                    $('td', row).eq(0).html(index + 1);
                    $('td', row).eq(1).html(edudata.name);
                    $('td', row).eq(2).html(degreedata.name);
                    $('td', row).eq(3).html(streamdata.name);
                    $('td', row).eq(4).html(subjectdata.name);
                    var testdata = JSON.stringify(datas).replace(/"/g, '\\"');
                    var edit = '<i class="fa fa-edit fatbl text-info" data-toggle="tooltip" title="Edit Department"' +
                        'style="font-size:20px;" onclick=\'updateField("' + testdata + '","' + index + '")\'>' +
                        '</i> <a href=\'javascript:deleteQualification("' + index + '")\' class="btn btn-link text-danger" ><i class="fa fa-trash fatbl"></i></a>';
                    $('td', row).eq(6).html(edit);
                },
                bAutoWidth: false,
                "columns": [
                    {
                        "data": "educationLevelMasterDTO", title: "Sr.no"

                    },
                    {
                        "data": "educationLevelMasterDTO", title: "EducationLevel"

                    },
                    {
                        "data": "degreeMasterDTO", title: "Degree"

                    },
                    {
                        "data": "streamMasterDTO", title: "Stream"


                    },
                    {
                        "data": "subjectMasterDTO", title: "Subject"


                    },
                    {
                        "data": "percentage", title: "percentage"


                    },
                    {
                        "data": "educationLevelMasterDTO", title: "Action"

                    }
                ]
            });
    // $('#fillEducationQualificationData').focus();

    scroll("fillEducationQualificationData");
}
function showModalTable(datas) {
    $('#teaching_staff_data_table2')
        .DataTable(
            {

                "columnDefs": [{
                    "className": "dt-center",
                    "targets": "_all"
                }],
                "autoWidth": true,
                "processing": true,
                "scrollX": true,
                data: JSON.parse(datas),
                "iDisplayLength": 50,
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                destroy: true,
                "order": [[0, "asc"]],

                "fnCreatedRow": function (row, datas, index) {


                    $('td', row).eq(0).html(index + 1);


                },
                bAutoWidth: false,
                "columns": [
                    {
                        "data": "id", title: "Sr.no"

                    },
                    {
                        "data": "educationLevelMasterDTO.name", title: "EducationLevel"

                    },
                    {
                        "data": "degreeMasterDTO.name", title: "Degree"

                    },
                    {
                        "data": "streamMasterDTO.name", title: "Stream"


                    },
                    {
                        "data": "subjectMasterDTO.name", title: "Subject"


                    },
                    {
                        "data": "percentage", title: "percentage"


                    }
                ]
            }); 
    //	$('#exampleModalLong').modal('show');
    // $('#fillEducationQualificationData').focus();

    //scroll("fillEducationQualificationData");
}
function updateField(data, index) {
    cIndex = index;
    //alert(cIndex);
    var datas = JSON.parse(data);
    setValue("educationLevel", datas.educationLevelMasterDTO.id);
    setValue("educationLevel", datas.educationLevelMasterDTO.id);
    onEduChange();
    setValue("degree", datas.degreeMasterDTO.id);
    degreeChange();
    setValue("stream", datas.streamMasterDTO.id);
    streamChange();
    setValue("subject", datas.subjectMasterDTO.id);

    setValue("percentage", datas.percentage);


}
function editData(data) {
    $("#educationQualificationDIV").show();
    var datas = JSON.parse(data);
    staffEducationQualificationDTOList.length = 0;
    if (datas.staffEducationQualificationDTOList instanceof Array) {
        this.staffEducationQualificationDTOList = this.staffEducationQualificationDTOList.concat(datas.staffEducationQualificationDTOList);
    }
    //	this.staffEducationQualificationDTOList.concat(datas.staffEducationQualificationDTOList);
    fillEducationQualificationData(staffEducationQualificationDTOList);
    //	scroll("salutation");
    setValue("salutation", datas.staffBasicDetailDTO.salutation);
    setValue("firstname", datas.staffBasicDetailDTO.firstName);
    setValue("middlename", datas.staffBasicDetailDTO.middleName);
    setValue("lastname", datas.staffBasicDetailDTO.lastName);
    setValue("email", datas.staffBasicDetailDTO.email);
    setValue("phoneno", datas.staffBasicDetailDTO.phoneNo);
    setValue("pancard", datas.staffBasicDetailDTO.pancardNo);
    setValue("address", datas.staffBasicDetailDTO.address);
    setValue("teachingexp", datas.staffExperianceSalaryDTO.teachingExperiance);
    setValue("noteachingexp", datas.staffExperianceSalaryDTO.teachingExperiance);
    setValue("joiningDate", datas.staffExperianceSalaryDTO.joiningDate);
    setValue("reliveDate", datas.staffExperianceSalaryDTO.reliveDate);
    setValue("staffType", datas.staffExperianceSalaryDTO.staffType);
    setValue("salary", datas.staffExperianceSalaryDTO.salary);
    var curiculams = curriculumList.filter((data1) => data1.id == datas.staffExperianceSalaryDTO.staffType);
    for (var i = 0; i < curiculams.length; i++) {
        $("#curricuram").append(new Option(curiculams[i].name, curiculams[i].id));
    }
    setValue("curricuram", datas.staffExperianceSalaryDTO.curriculumMasterDTO.id);
    setValue("instituteStaffDetailMpgDTOId", datas.instituteStaffDetailMpgDTO.id);
    setValue("staffBasicDetailDTOId", datas.staffBasicDetailDTO.id);
    setValue("staffExperianceSalaryDTOId", datas.staffExperianceSalaryDTO.id);
}
function afterDelete(data) {
    alert(data);
    alert(data.message);
}
function showdata(data) {

    $('#teaching_staff_data_table')
        .DataTable(
            {
            	
                "columnDefs": [{
                    "className": "dt-center",
                    "targets": 10,
                    render: function (datas, type, row, meta) {



                        let testdata = JSON.stringify(row).replace(/"/g, '\\"');

                        var action = '<i class="fa fa-edit fatbl text-info" data-toggle="tooltip" title="Edit Department" style="font-size:20px;" onclick=\'editData("' + testdata + '")\'>' +
                            '</i><i class="fa fa-trash fatbl text-danger" data-toggle="tooltip" title="Edit Department" style="font-size:20px;" onclick=\'commonDelete("' + "instStaffMpgId" + '","' + row.instituteStaffDetailMpgDTO.id + '","' + "staff" + '",afterDelete)\'></i>';
                        return action;
                    }
                }],
                "scrollX": true,
                "autoWidth": true,
                "processing": true,
                data: data,
                "iDisplayLength": 50,
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                destroy: true,
                "order": [[0, "asc"]],

                "fnCreatedRow": function (row, datas, index) {

                    $('td', row).eq(0).html(index + 1);
                    $('td', row).eq(1).html(datas.staffBasicDetailDTO.firstName + " " + datas.staffBasicDetailDTO.lastName + " " + datas.staffBasicDetailDTO.middleName);
                    var edudata = datas.staffExperianceSalaryDTO.curriculumMasterDTO.staffType == 1 ? "Teaching" : "Non Teaching";
                    $('td', row).eq(7).html(edudata);
                    var list = JSON.stringify(datas.staffEducationQualificationDTOList).replace(/"/g, '\\"');
                    var preview = '<a href=\'javascript:showModalTable("' + list + '")\' class="btn btn-link text-info" data-toggle="modal" data-target="#educationQualificationModal"><i class="fa fa-eye fatbl"></i></a>';
                    $('td', row).eq(9).html(preview);

                },
                bAutoWidth: false,
                "columns": [
                    {
                        "data": null, title: "Sr.no"

                    },
                    {
                        "data": "staffBasicDetailDTO.firstName", title: "Full Name"

                    },
                    {
                        "data": "staffBasicDetailDTO.phoneNo", title: "PhoneNo"

                    },
                    {
                        "data": "staffExperianceSalaryDTO.salary", title: "Salary"

                    },
                    {
                        "data": "staffExperianceSalaryDTO.joiningDate", title: "JoiningDate"

                    },
                    {
                        "data": "staffExperianceSalaryDTO.reliveDate", title: "ReliveDate"

                    },
                    {
                        "data": "staffExperianceSalaryDTO.teachingExperiance", title: "TeachingExperiance"

                    },
                    {
                        "data": "staffExperianceSalaryDTO.staffType", title: "staffType"

                    },
                    {
                        "data": "staffExperianceSalaryDTO.curriculumMasterDTO.name", title: "Curriculum Area"

                    },
                    {
                        "data": null, title: "Education"

                    },
                    {
                        "data": "staffExperianceSalaryDTO.curriculumMasterDTO.name", title: "Action"

                    },

                ]
            });
    // $('#fillEducationQualificationData').focus();

    var table = $("#teaching_staff_data_table").DataTable();
    var isEmpty = table.rows().count() === 0;
    if (isEmpty) {
        $("#nextBtn").prop("disabled", true);
    }

}
function scroll(id) {
    $('html, body').animate({
        scrollTop: $("#" + id).offset().top
    }, 1000);
}
function getValue(id) {
    return $("#" + id).val();
}
function setValue(id, data) {
    return $("#" + id).val(data);
}
$("#saveBtn").click(function () {
    saveData();
});
function saveData() {

    var verfied = true;
    var text = "";
    var salutation = getValue("salutation");
    if ($.trim($("#salutation").val()) === "") {
        verfied = false;
        var text = "Please Select salutation";
        document.getElementById("salutationError").innerHTML = text;
    }
    var firstName = getValue("firstname");
    if (firstName === "") {
        verfied = false;
        var text = "Please Enter First Name";
        document.getElementById("firstnameError").innerHTML = text;
    }
    var middlename = getValue("firstname");
    if (middlename === "") {
        verfied = false;
        var text = "Please Enter Middle Name";
        document.getElementById("middlenameError").innerHTML = text;
    }
    var lastname = getValue("lastname");
    if (lastname === "") {
        verfied = false;
        var text = "Please Enter Last Name";
        document.getElementById("lastnameError").innerHTML = text;
    }
    var email = getValue("email");
    if (email === "") {
        verfied = false;
        var text = "Please Enter Email";
        document.getElementById("emailError").innerHTML = text;
    }
    var phoneno = getValue("phoneno");
    if (phoneno === "") {
        verfied = false;
        var text = "Please Enter Phone No";
        document.getElementById("phonenoError").innerHTML = text;
    }
    var pancard = getValue("pancard");
    if (pancard === "") {
        verfied = false;
        var text = "Please Enter Pancard No";
        document.getElementById("pancardError").innerHTML = text;
    }
    var address = getValue("address");
    if (address === "") {
        verfied = false;
        var text = "Please Enter Address";
        document.getElementById("addressError").innerHTML = text;
    }
    var teachingexp = getValue("teachingexp");
    if (teachingexp === "") {
        verfied = false;
        var text = "Please Enter Teaching expirence";
        document.getElementById("teachingexpError").innerHTML = text;
    }

    var noteachingexp = getValue("noteachingexp");
    if (noteachingexp === "") {
        verfied = false;
        var text = "Please Enter Non-Teaching expirence";
        document.getElementById("noteachingexpError").innerHTML = text;
    }
    /* var noteachingexp = getValue("noteachingexp"); */
    var joiningDate = getValue("joiningDate");
    if ($.trim($("#joiningDate").val()) === "") {
        verfied = false;
        var text = "Please Select joining Date";
        document.getElementById("joiningDateError").innerHTML = text;
    }

    var reliveDate = getValue("reliveDate");
    if ($.trim($("#reliveDate").val()) === "") {
        verfied = false;
        var text = "Please Select relive Date";
        document.getElementById("reliveDateError").innerHTML = text;
    }
    var staffType = getValue("staffType");
    if ($.trim($("#staffType").val()) === "") {
        verfied = false;
        var text = "Please Select Staff Type";
        document.getElementById("staffTypeError").innerHTML = text;
    }
    var curricuram = getValue("curricuram");
    if ($.trim($("#curricuram").val()) === "") {
        verfied = false;
        var text = "Please Select curricuram Area";
        document.getElementById("curricuramError").innerHTML = text;
    }
    var salary = getValue("salary");

    if ($.trim($("#salary").val()) === "") {
        verfied = false;
        var text = "Please Enter Salary";
        document.getElementById("salaryError").innerHTML = text;
    }

    if (staffEducationQualificationDTOList.length < 1) {
        verfied = false;
        $("#percentageError").text("Please Enter atleast one Education Detail");
    }
    if (staffEducationQualificationDTOList.length  ==0) {
    
        var educationLevel = $("#educationLevel").val();
        var percentage = $("#percentage").val();
        var subject = $("#subject").val();
        var stream = $("#stream").val();
        var degree = $("#degree").val();
        var text = "";

        if (educationLevel == null) {
            verfied = false;
            text = "Please Select Education Level";
            document.getElementById("educationLevelError").innerHTML = text;
        }

        if (stream == null) {
            verfied = false;
            text = "Please Select Stream Level";
            document.getElementById("streamError").innerHTML = text;
        }
        if (degree == null) {
            verfied = false;
            text = "Please Select Degree Level";
            document.getElementById("degreeError").innerHTML = text;
        }
        if (subject == null) {
            verfied = false;
            text = "Please Select Subject Level";
            document.getElementById("subjectError").innerHTML = text;
        }
        if (percentage == null) {
            verfied = false;
            text = "Please Enter Percentage";
            document.getElementById("percentageError").innerHTML = text;
        }
    }


    var senddata = JSON.stringify({
        "instituteStaffDetailMpgDTO": {
            "id": getValue("instituteStaffDetailMpgDTOId"),
            "instId": 38,
            "applicationMasterDTO": {
                "id": "1"
            }
        },
        "staffBasicDetailDTO": {
            "id": getValue("staffBasicDetailDTOId"),
            "salutation": salutation,
            "firstName": firstName,
            "middleName": middlename,
            "lastName": lastname,
            "email": email,
            "phoneNo": phoneno,
            "pancardNo": pancard,
            "address": address
        },
        "staffExperianceSalaryDTO": {
            "id": getValue("staffExperianceSalaryDTOId"),
            "salary": salary,
            "joiningDate": joiningDate,
            "reliveDate": reliveDate,
            "teachingExperiance": teachingexp,
            "nonTeachingExperiance": noteachingexp,
            "staffType": staffType,
            "curriculumMasterDTO": {
                "id": curricuram
            }
        },
        "staffEducationQualificationDTOList": staffEducationQualificationDTOList,
    });

    if (verfied) {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: "/staff/",
            headers: {
                'API-VERSION': 1
            },
            data: senddata,
            success: function (data) {
                console.log("DATA " + JSON.stringify(data));
                if (data.success == 1) {

                    showAlerts(data.message, data.messageList.NoError, "alertMessage", false, data.serviceResult, 2000);


                }
                else {
                    Swal.fire({
                        title: 'ERROR',
                        html: data.message,
                        icon: 'error',
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "OK",
                        closeOnConfirm: false,
                        allowOutsideClick: false,
                    }).then((result) => {
                        if (result.value) {

                        }
                    })
                }
            },
            error: function (data, status, error) {
                console.log("error " + error);
                console.log("messageList " + JSON.stringify(data.responseJSON.messageList));
                console.log("data " + JSON.stringify(data));
                console.log("status " + status);
                showAlerts(data.responseJSON.message, data.statusText, "alertMessage", true, data.responseJSON.serviceResult, 20000);
                if (data.status == "417") {
                    //alert("417");
                    var messageList = data.responseJSON.messageList;
                    let myMap = new Map();
                    myMap = messageList;
                    console.log("myMap" + JSON.stringify(myMap));
                    for (const [key, value] of Object.entries(myMap)) {
                        // console.log(`${key}: ${value}`);
                        // console.log("Key "+key );
                        // console.log("value "+value );
                        document.getElementById("" + key).innerHTML = value;
                    }
                    emailIdList = [];
                    phoneNoList = [];
                    showAlerts(data.responseJSON.message, data.statusText, "alertMessage", true, data.responseJSON.serviceResult, 20000);
                }
                else if (data.status == "409") {
                    alert("Constraint Violation Error");
                    let contraintViolationMsg = data.responseJSON.message.split('for key');
                    console.log("words" + contraintViolationMsg[0]);
                    showAlerts(contraintViolationMsg[0], data.statusText, "alertMessage", true, data.responseJSON.serviceResult, 20000);
                }
                else {
                    Swal.fire({
                        title: 'ERROR',
                        html: "Something Went Wrong, Please Try Again",
                        icon: 'error',
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "OK",
                        closeOnConfirm: false,
                        allowOutsideClick: false,
                    }).then((result) => {
                        if (result.value) {

                        }
                    })
                }
            }
        });
    }
}

function commonDelete(key, value, endpoint, resultss) {
    var mapObject = {};
    mapObject[key] = value;
    Swal.fire({
        title: 'Are you sure?',
        html: "You want to Staff Detail",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: "YES",
        cancelButtonColor: '#d33',
        cancelButtonText: "Cancel",
        closeOnConfirm: false,
        closeOnCancel: false,
        allowOutsideClick: false
    }).then((result) => {
        if (result.value) {
            if (value != 0) {

                $.ajax({
                    type: "DELETE",
                    url: "/" + endpoint + "/",
                    headers: {
                        'API-VERSION': 1
                    },
                    data: mapObject,
                    success: function (data) {
                        if (data.success == 1) {
                            showAlerts(data.message, data.messageList, "alertMessage", false, data.serviceResult, 2000);
                        } else {
                            Swal.fire({
                                title: 'ERROR',
                                html: data.message,
                                icon: 'error',
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "OK",
                                closeOnConfirm: false,
                                allowOutsideClick: false,
                            }).then((result) => {
                                if (result.value) {

                                }
                            })

                        }
                    },
                    error: function (_) {
                        alert(dbkey);
                        Swal.fire({
                            title: 'ERROR',
                            html: "Something Went Wrong, Please Try Again",
                            icon: 'error',
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "OK",
                            closeOnConfirm: false,
                            allowOutsideClick: false,
                        }).then((result) => {
                            if (result.value) {

                            }
                        })

                    }
                });

            }
        }

    })
}
function updatedata(mapObject, index) {
    cIndex = index;
    if (cIndex == null) {
        return;
    }
    var updateNewdata = {
        "id": $("#staffEducationQualificationDTOListId").val(),
        "educationLevelMasterDTO": {
            "id": educationLevel
        },
        "degreeMasterDTO": {
            "id": degree
        },
        "streamMasterDTO": {
            "id": stream
        },
        "subjectMasterDTO": {
            "id": subject
        },
        "percentage": percentage
    };
}
function deleteQualification(index) {

    var datas = JSON.parse(index);
    console.log("Current Object" + JSON.stringify(index));
    console.log(" List" + JSON.stringify(staffEducationQualificationDTOList));
    staffEducationQualificationDTOList.splice(index, 1);
    console.log(staffEducationQualificationDTOList);
    fillEducationQualificationData(staffEducationQualificationDTOList);
}
// onchange Remove Error Message
$("#salutation").change(function () {
    var text = "";
    if (salutation != null) {
        document.getElementById("salutationError").innerHTML = text;
    }
})
$("#firstname").keyup(function () {
    var text = "";
    if (firstname != null) {
        document.getElementById("firstnameError").innerHTML = text;
    }
})
$("#middlename").keyup(function () {
    var text = "";
    if (middlename != null) {
        document.getElementById("middlenameError").innerHTML = text;
    }

})
$("#lastname").keyup(function () {
    var text = "";
    if (lastname != null) {
        document.getElementById("lastnameError").innerHTML = text;
    }
})
$("#phoneno").keyup(function () {
    var text = "";
    if (phoneno != null) {
        document.getElementById("phonenoError").innerHTML = text;
    }
})
$("#pancard").keyup(function () {
    var text = "";
    if (pancard != null) {
        document.getElementById("pancardError").innerHTML = text;
    }
})
$("#email").keyup(function () {
    var text = "";
    if (email != null) {
        document.getElementById("emailError").innerHTML = text;
    }
})
$("#address").keyup(function () {
    var text = "";
    if (address != null) {
        document.getElementById("addressError").innerHTML = text;
    }
})

$("#teachingexp").keyup(function () {
    var text = "";
    if (teachingexp != null) {
        document.getElementById("teachingexpError").innerHTML = text;
    }
})
$("#noteachingexp").keyup(function () {
    var text = "";
    if (noteachingexp != null) {
        document.getElementById("noteachingexpError").innerHTML = text;
    }
})
$("#salary").keyup(function () {
    var text = "";
    if (salary != null) {
        document.getElementById("salaryError").innerHTML = text;
    }
})

$("#joiningDate").change(function () {
    var text = "";
    if (joiningDate != null) {
        document.getElementById("joiningDateError").innerHTML = text;
    }
})
$("#reliveDate").change(function () {
    var text = "";
    if (reliveDate != null) {
        document.getElementById("reliveDateError").innerHTML = text;
    }
})
$("#curricuram").change(function () {
    var text = "";
    if (curricuram != null) {
        document.getElementById("curricuramError").innerHTML = text;
    }
})
