var instituteProgramMpgDTO = [];
var instituteMediumMpgDTOList = [];
function FetchInstituteProgramDetail() {
	$.ajax({
		type: "GET",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		url: "/instExt/",
		headers: {
			'API-VERSION': 1
		},
		success: function (data) {
			if (data.success == 1) {

				//showAlerts(data.message,data.messageList.NoError,"alertMessage",false,data.serviceResult,2000);
				data = data.serviceResult;
				instituteProgramMpgDTO = data.instituteProgramMpgDTOList;
				console.log("instituteProgramMpgDTO " + JSON.stringify(instituteProgramMpgDTO));
				if (instituteProgramMpgDTO.length > 0) {
					for (var i = 0; i < instituteProgramMpgDTO.length; i++) {

						programData = instituteProgramMpgDTO[i].instituteProgramDetailDTO.programMasterDTO;

						$('#programId').append(
							'<option value="' + programData.id + '">'
							+ programData.name + '</option>').trigger('chosen:updated');

					}

				} else {
					$("#programId").append('<option value=""  selected="" disabled="">'
						+ "-- No Program Is Available --" + '</option>');
				}




			}
			else {
				Swal.fire({
					title: 'ERROR',
					html: data.message,
					icon: 'error',
					confirmButtonColor: '#3085d6',
					confirmButtonText: "OK",
					closeOnConfirm: false,
					allowOutsideClick: false,
				}).then((result) => {
					if (result.value) {

					}
				})

			}
		},
		error: function (data, status, error) {
			console.log("error " + error);
			console.log("messageList " + JSON.stringify(data.responseJSON.messageList));
			console.log("data " + JSON.stringify(data));
			console.log("status " + status);
			showAlerts(data.responseJSON.message, data.statusText, "alertMessage", false, data.responseJSON.serviceResult, 20000);
			if (data.status == "401") {
				showAlerts(data.responseJSON.message, data.statusText, "alertMessage", false, data.responseJSON.serviceResult, 20000);
			}
			else if (data.status == "500") {
				showAlerts(data.responseJSON.message, data.statusText, "alertMessage", false, data.responseJSON.serviceResult, 20000);
			}
		}
	});

}

function saveCreatedApplicationDetail() {
	var applicationMasterDTO = {
		id: document.getElementById("applicationId").value,
	}
	var programMasterDTO = {
		id: document.getElementById("programId").value
	}


	var data = {
		programMasterDTO: programMasterDTO,
		applicationMasterDTO: applicationMasterDTO
	};
	console.log("before save" + JSON.stringify(data));
	var verifed = true;
	if (verifed) {
		Swal.fire({
			title: 'Are you sure?',
			html: "You want to Apply For this program",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			confirmButtonText: "YES",
			cancelButtonColor: '#d33',
			cancelButtonText: "Cancel",
			closeOnConfirm: false,
			closeOnCancel: false,
			allowOutsideClick: false
		}).then((result) => {
			if (result.value) {
				$.ajax({
					type: "POST",
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					url: "/createApplication/",
					headers: {
						'API-VERSION': 1
					},
					data: JSON.stringify(data),
					success: function (data) {
						console.log("DATA " + JSON.stringify(data));
						if (data.success == 1) {
							Swal.fire({
								title: 'congratulations',
								html: "You have successfully applied for",
								icon: 'success',
								showCancelButton: false,
								confirmButtonColor: '#3085d6',
								confirmButtonText: "Proceed Further",
								cancelButtonColor: '#d33',
								cancelButtonText: "Cancel",
								closeOnConfirm: false,
								closeOnCancel: false,
								allowOutsideClick: false
							}).then((result) => {
								if (result.value) {
									
									window.location.href = '/student/home'; 
										
								}

							})
						}
						else {
							Swal.fire({
								title: 'ERROR',
								html: data.message,
								icon: 'error',
								confirmButtonColor: '#3085d6',
								confirmButtonText: "OK",
								closeOnConfirm: false,
								allowOutsideClick: false,
							}).then((result) => {
								if (result.value) {

								}
							})

						}
					},
					error: function (data, status, error) {
						console.log("error " + error);
						console.log("messageList " + JSON.stringify(data.responseJSON.messageList));
						console.log("data " + JSON.stringify(data));
						console.log("status " + status);

						var messageList = data.responseJSON.messageList;
						let myMap = new Map();
						myMap = messageList;
						console.log("myMap" + JSON.stringify(myMap));
						for (const [key, value] of Object.entries(myMap)) {
							//console.log(`${key}: ${value}`);
							// console.log("Key "+key );
							//console.log("value "+value );
							document.getElementById("" + key).innerHTML = value;
						}
						showAlerts(data.responseJSON.serviceResult, data.statusText, "alertMessage", false, data.responseJSON.message, 2000);
					}
				});

			}

		})
	}




}