var instituteProgramDetailDTO = [];
var instituteProgramMpgDTO = [];
var instituteMediumMpgDTO = [];
var mediumMasterList = [];
var otherProgramMasterDTO = [];
var programMasterDTO = [];

var instituteId = $("#sessionInstId").text();

function saveInstituteAdditionalDetail() {

	var checkedMediumList = [];
	var mediumList = [];
	var verfied = true;

	var text = "";
	if ($.trim($("#programId").val()) === "") {
		verfied = false;
		var text = "Please select program";
		document.getElementById("programIdError").innerHTML = text;
	}
	if ($.trim($("#shiftId").val()) === "") {
		verfied = false;
		var text = "Please select shift type ";
		document.getElementById("shiftIdError").innerHTML = text;
	}
	if ($.trim($("#shiftStartTime").val()) === "") {
		verfied = false;
		var text = "Please select shift start time";
		document.getElementById("shiftStartTimeError").innerHTML = text;
	}

	if ($.trim($("#shiftEndTime").val()) === "") {
		verfied = false;
		var text = "Please select shift end time";
		document.getElementById("shiftEndTimeError").innerHTML = text;
	}

	$('#mediumDiv input:checked').each(function () {
		/* mediumList.push($(this).attr('value')); */

		checkedMediumList = {
			id: $(this).attr('value'),
		};
		mediumList.push(checkedMediumList);
	});

	if (mediumList.length > 0) {
		verfied = true;
	} else {
		verfied = false;
		var text = "Please check atleast one medium";
		document.getElementById("programMediumError").innerHTML = text;
	}



	var instituteProgramDetailDTO = {
		id: document.getElementById("instOtherCourseMpgId").value,
		programMasterDTO: {
			id: document.getElementById("programId").value
		},
		shiftId: document.getElementById("shiftId").value,
		shiftStartTime: document.getElementById("shiftStartTime").value,
		shiftEndTime: document.getElementById("shiftEndTime").value
	};

	var instituteProgramMpgDTO = {
		id: document.getElementById("instAdditionalDetailId").value
	};

	var instituteMediumMpgDTO = {
		id: document.getElementById("instMediumId").value,
		mediumMasterDTOList: mediumList,
		instId: instituteId
	};
	var applicationMasterDTO = {
		id: document.getElementById("applicationId").value,
	}

	var data = {
		//otherProgramName:document.getElementById("otherProgramName").value
		//otherProgramName: null,
		instituteProgramDetailDTO: instituteProgramDetailDTO,
		instituteProgramMpgDTO: instituteProgramMpgDTO,
		instituteMediumMpgDTO: instituteMediumMpgDTO,
		/* applicationMasterDTO: applicationMasterDTO */
	};
	console.log("before save" + JSON.stringify(data));

	if (verfied) {

		$.ajax({
			type: "POST",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			url: "/instExt/",
			headers: {
				'API-VERSION': 1
			},
			data: JSON.stringify(data),
			success: function (data) {
				console.log("DATA " + JSON.stringify(data));
				if (data.success == 1) {
					showAlerts(data.message, data.messageList.NoError, "alertMessage", false, data.serviceResult, 2000);
					cleanValidationDto('instAdditionalDTO');
					$('#mediumDiv input:checked').each(function () {
						$(".instAdditionalDTO").prop('checked', false);
					});
					cleanError('error');
				}
				else {
					Swal.fire({
						title: 'ERROR',
						html: data.message,
						icon: 'error',
						confirmButtonColor: '#3085d6',
						confirmButtonText: "OK",
						closeOnConfirm: false,
						allowOutsideClick: false,
					}).then((result) => {
						if (result.value) {

						}
					})

				}
			},
			error: function (data, status, error) {
				console.log("error " + error);
				console.log("messageList " + JSON.stringify(data.responseJSON.messageList));
				console.log("data " + JSON.stringify(data));
				console.log("status " + status);

				var messageList = data.responseJSON.messageList;
				let myMap = new Map();
				myMap = messageList;
				console.log("myMap" + JSON.stringify(myMap));
				for (const [key, value] of Object.entries(myMap)) {
					//console.log(`${key}: ${value}`);
					// console.log("Key "+key );
					//console.log("value "+value );
					document.getElementById("" + key).innerHTML = value;
				}
				showAlerts(data.responseJSON.serviceResult, data.statusText, "alertMessage", false, data.responseJSON.message, 2000);
			}
		});
	}

}

function FetchInstituteAdditionalDetail() {
	$.ajax({
		type: "GET",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		url: "/instExt/",
		headers: {
			'API-VERSION': 1
		},
		success: function (data) {
			if (data.success == 1) {
				//alert("fetched");
				console.log("fetched Data" + JSON.stringify(data));
				//showAlerts(data.message,data.messageList.NoError,"alertMessage",false,data.serviceResult,2000);
				data = data.serviceResult;

				instituteProgramDetailDTO = data.instituteProgramDetailDTOList;
				instituteProgramMpgDTO = data.instituteProgramMpgDTOList;
				mediumMasterList = data.mediumMasterDTOList;
				otherProgramMasterDTO = data.otherProgramMasterDTOList;
				programMasterDTO = data.programMasterDTOList;
				

				if (programMasterDTO.length > 0) {
					$("#programId").append(
						'<option value=""  selected="" disabled="">'
						+ "-- Select --" + '</option>');
					for (var i = 0; i < programMasterDTO.length; i++) {
						programData = programMasterDTO[i];
						var isExists = instituteProgramMpgDTO.some((data) => data.instituteProgramDetailDTO.programMasterDTO.id == programData.id);
						if (!isExists) {
							$('#programId').append(
								'<option value="' + programData.id + '">'
								+ programData.name + '</option>').trigger('chosen:updated');
						}

					}
					/*
					$("#programId").append(
					'<option value="-1">'
					+ "Other" + '</option>'); */
				} else {
					$("#programId").append(
						'<option value=""  selected="selected" disabled="">'
						+ "-- No Program Available --" + '</option>');
				}
				addMedium(mediumMasterList);
				fillInstituteAdditionalDataTable(instituteProgramMpgDTO);


			}
			else {
				Swal.fire({
					title: 'ERROR',
					html: data.message,
					icon: 'error',
					confirmButtonColor: '#3085d6',
					confirmButtonText: "OK",
					closeOnConfirm: false,
					allowOutsideClick: false,
				}).then((result) => {
					if (result.value) {

					}
				})

			}
		},
		error: function (data, status, error) {
			console.log("error " + error);
			console.log("messageList " + JSON.stringify(data.responseJSON.messageList));
			console.log("data " + JSON.stringify(data));
			console.log("status " + status);
			showAlerts(data.responseJSON.message, data.statusText, "alertMessage", false, data.responseJSON.serviceResult, 20000);
			if (data.status == "401") {
				showAlerts(data.responseJSON.message, data.statusText, "alertMessage", false, data.responseJSON.serviceResult, 20000);
			}
			else if (data.status == "500") {
				showAlerts(data.responseJSON.message, data.statusText, "alertMessage", false, data.responseJSON.serviceResult, 20000);
			}
		}
	});

}

function addMedium(mediumMasterList) {

	for (var j = 0; j < mediumMasterList.length; j++) {
		var div = document.createElement('div');
		div.setAttribute("id", "mediumDiv" + j);
		div.innerHTML = '<p class="checkbox-inline ml-2"><input type="checkbox" value="' + mediumMasterList[j].id + '" id="programMedium' + j + '" name="programMedium' + j + '" class="mr-1 instAdditionalDTO">' + mediumMasterList[j].name + '</p>';

		document.getElementById('mediumDiv').appendChild(div);
	}



}


$("#programId").change(function () {

	if ($("#programId").val() == -1) {
		$("#otherProgramDiv").show();
	} else {
		$("#otherProgramDiv").hide();
	}

	$("#otherProgramName").val("");
});

$("#shiftStartTime").change(function () {

	$("#otherProgramName").val("");
});
function fillInstituteAdditionalDataTable(data) {

	$('#inst_additional_data_table')
		.DataTable(
			{

				"columnDefs": [{
					"className": "dt-center",
					"targets": "_all"
				}],
				"autoWidth": true,
				"processing": true,
				"data": data,
				"iDisplayLength": 50,
				"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				destroy: true,
				"order": [[0, "asc"]],

				"fnCreatedRow": function (row, data, index) {
					$('td', row).eq(0).html(index + 1);
				},
				bAutoWidth: false,
				"columns": [
					{
						"data": null,

					},
					{
						"data": "instituteProgramDetailDTO.programMasterDTO.name"
					},
					{
						mRender: function (data, type, row) {
							var FetchMediumList = [];
							var medium = row.instituteMediumMpgDTOList;

							for (var i = 0; i < medium.length; i++) {
								var m = medium[i].mediumMasterDTOList[0].name;
								FetchMediumList.push(m);
							}

							return FetchMediumList;
						}

					},

					{
						mRender: function (data, type, row) {
							var shiftType = row.instituteProgramDetailDTO.shiftId;
							if (shiftType == 1) {
								shiftType = 'Morning';
							} else if (shiftType == 2) {
								shiftType = 'Evening';
							}
							return shiftType;
						}
					},
					{
						"data": "instituteProgramDetailDTO.shiftStartTime"
					},
					{
						"data": "instituteProgramDetailDTO.shiftEndTime"

					},
					{
						mRender: function (data, type, row) {
							var app_id = row.applicationMasterDTO.id;
							var inst_program_mog_id = row.id;

							return "<a "
								+ " target='_blank'><i class='fa fa-pencil fatbl text-info' onclick='fillInstData(" + inst_program_mog_id + ")'></i> <i class='fa fa-trash fatbl text-info' onclick='deleteApplicatioData(" + app_id + ")'></i>"
								+ "</a>"


						}
					},
					 {
						mRender: function (data, type, row) {
							var appplication_id = null;
							if (row.applicationMasterDTO != null) {
								appplication_id = row.applicationMasterDTO.id;
							}

							return "<a "
								+ " target='_blank'><i class='fa fa-arrow-right fatbl text-info' onclick='nextPage(" + appplication_id + ")'></i>"
								+ "</a>"


						}
					} 


				]
			});
}
function nextPage(applicationId) {
	//alert(applicationId);
	var encryptedAES = CryptoJS.AES.encrypt("applicationId=" + applicationId, "My Secret Passphrase");
	location.href = "/inst/home?" + encryptedAES;

}

function deleteApplicatioData(applicationId) {
	Swal.fire({
		title: 'Are you sure?',
		html: "You want to Delete this application",
		icon: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		confirmButtonText: "YES",
		cancelButtonColor: '#d33',
		cancelButtonText: "Cancel",
		closeOnConfirm: false,
		closeOnCancel: false,
		allowOutsideClick: false
	}).then((result) => {
		if (result.value) {

			if (applicationId != 0) {
				$.ajax({
					type: "DELETE",
					url: "/instExt/",
					headers: {
						'API-VERSION': 1
					},
					data: { applicationId: applicationId },
					success: function (data) {
						if (data.success == 1) {
							showAlerts(data.message, data.messageList, "alertMessage", false, data.serviceResult, 2000);
						} else {
							Swal.fire({
								title: 'ERROR',
								html: data.message,
								icon: 'error',
								confirmButtonColor: '#3085d6',
								confirmButtonText: "OK",
								closeOnConfirm: false,
								allowOutsideClick: false,
							}).then((result) => {
								if (result.value) {

								}
							})

						}
					},
					error: function (error) {
						Swal.fire({
							title: 'ERROR',
							html: "Something Went Wrong, Please Try Again",
							icon: 'error',
							confirmButtonColor: '#3085d6',
							confirmButtonText: "OK",
							closeOnConfirm: false,
							allowOutsideClick: false,
						}).then((result) => {
							if (result.value) {

							}
						})

					}
				});

			}


		}

	})
}

function fillInstData(inst_program_mog_id) {

	$('#mediumDiv input:checked').each(function () {

		$(this).prop("checked", false);

	});
	console.log(inst_program_mog_id);
	console.log("instituteProgramMpgDTO " + JSON.stringify(instituteProgramMpgDTO));
	var instDataById = instituteProgramMpgDTO.filter((x) => x.id == inst_program_mog_id);


	console.log("instDataById" + JSON.stringify(instDataById[0]));

	if (instDataById != null) {
		$("#instAdditionalDetailId").val(instDataById[0].id)
		$("#instOtherCourseMpgId").val(instDataById[0].instituteProgramDetailDTO.id);
//		$("#programId").val(instDataById[0].instituteProgramDetailDTO.programMasterDTO.id);
//		$("#programId").empty();
		var filterList = programMasterDTO.filter((data)=>data.id==instDataById[0].instituteProgramDetailDTO.programMasterDTO.id);
		if(filterList.length>0){
			$("#programId").append(new Option(filterList[0].name,filterList[0].id));
			$("#programId").val(filterList[0].id);
		}
		
		$("#shiftId").val(instDataById[0].instituteProgramDetailDTO.shiftId);
		$("#shiftStartTime").val(instDataById[0].instituteProgramDetailDTO.shiftStartTime);
		$("#shiftEndTime").val(instDataById[0].instituteProgramDetailDTO.shiftEndTime);

		for (var i = 0; i < instDataById[0].instituteMediumMpgDTOList.length; i++) {
			console.log("mediumid " + JSON.stringify(instDataById[0].instituteMediumMpgDTOList[i].mediumMasterDTOList[0].id));
			if ($("#programMedium" + i).val() == instDataById[0].instituteMediumMpgDTOList[i].mediumMasterDTOList[0].id) {
				$("#programMedium" + i).prop("checked", true);
			}
		}

		$("#applicationId").val(instDataById[0].applicationMasterDTO.id);
		$("#programId").prop("disabled", true);
	}


}
