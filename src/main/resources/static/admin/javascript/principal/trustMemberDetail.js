var phoneNoList = [];
var emailIdList = [];
var trustMemberDetailList = [];
var trustMemberDetailMpgList = [];
var roleMasterList = [];
var trustMasterList = [];
var instituteId = $("#sessionInstId").text();


function saveTrusMembertDetail() {

	var verfied = true;
	var text = "";

	if ($.trim($("#trustNameDD").val()) === "") {
		verfied = false;
		var text = "Please select trust";
		document.getElementById("trustNameDDError").innerHTML = text;
	}

	if (document.getElementById("existingTrustMember").checked) {
		if ($.trim($("#trustMemberNameDD").val()) === "") {
			verfied = false;
			var text = "Please select trust member";
			document.getElementById("trustMemberNameDDError").innerHTML = text;
		}
	}

	if ($.trim($("#trustMemberName").val()) === "") {
		verfied = false;
		var text = "Please Enter  name";
		document.getElementById("name").innerHTML = text;
	}
	if ($.trim($("#trustMemberPhoneNo").val()) === "") {
		verfied = false;
		var text = "Please Enter phone no ";
		document.getElementById("phoneNo").innerHTML = text;
	}
	if ($.trim($("#trustMemberPanNo").val()) === "") {
		verfied = false;
		var text = "Please Enter pan card no";
		document.getElementById("panNo").innerHTML = text;
	}
	if ($.trim($("#trustMemberEmail").val()) === "") {
		verfied = false;
		var text = "Please Enter email address";
		document.getElementById("email").innerHTML = text;
	}
	if ($.trim($("#trustMemberAddress").val()) === "") {
		verfied = false;
		var text = "Please Enter address";
		document.getElementById("address").innerHTML = text;
	}
	if ($.trim($("#trustMemberRole").val()) === "") {
		verfied = false;
		var text = "Please select role";
		document.getElementById("role").innerHTML = text;
	}

	var data = {
		id: document.getElementById("hiddenTrustMemberId").value,
		name: document.getElementById("trustMemberName").value,
		phoneNo: document.getElementById("trustMemberPhoneNo").value,
		pancardNo: document.getElementById("trustMemberPanNo").value,
		email: document.getElementById("trustMemberEmail").value,
		address: document.getElementById("trustMemberAddress").value,
		roleMasterDTO: {
			id: document.getElementById("trustMemberRole").value
		},
		trustMasterDTO: {
			id: document.getElementById("trustNameDD").value
		},
		isMapped: isMapped,
		trustMemberMpgId: document.getElementById("hiddenTrustMemberDetailMpgId").value
		/* trustMemberDetailDTOList:trustMemberDetailList,
		trustMemberDetailMpgDTO:trustMemberDetailMpgList */
	};
	console.log("before save" + JSON.stringify(data));
	/* verfied = false; */
	if (verfied) {

		$.ajax({
			type: "POST",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			url: "/trustMember/",
			headers: {
				'API-VERSION': 1
			},
			data: JSON.stringify(data),
			success: function (data) {
				console.log("DATA " + JSON.stringify(data));
				if (data.success == 1) {
					showAlerts(data.message, data.messageList.NoError, "alertMessage", false, data.serviceResult, 2000);
					fetchTrustMemberdetail();
					cleanValidationDto('trustMasterDTO');
					cleanValidationDto('roleMasterDTO');
					cleanValidationDto('TrustMemberDetailDTO');
					cleanError('error');
				}
				else {
					Swal.fire({
						title: 'ERROR',
						html: data.message,
						icon: 'error',
						confirmButtonColor: '#3085d6',
						confirmButtonText: "OK",
						closeOnConfirm: false,
						allowOutsideClick: false,
					}).then((result) => {
						if (result.value) {

						}
					})

				}
			},
			error: function (data, status, error) {
				console.log("error " + error);
				console.log("messageList " + JSON.stringify(data.responseJSON.messageList));
				console.log("data " + JSON.stringify(data));
				console.log("status " + status);

				var messageList = data.responseJSON.messageList;
				let myMap = new Map();
				myMap = messageList;
				console.log("myMap" + JSON.stringify(myMap));
				for (const [key, value] of Object.entries(myMap)) {
					//console.log(`${key}: ${value}`);
					// console.log("Key "+key );
					//console.log("value "+value );
					document.getElementById("" + key).innerHTML = value;
				}
				showAlerts(data.responseJSON.serviceResult, data.statusText, "alertMessage", false, data.responseJSON.message, 2000);
			}
		});
	}

}
var isMapped = 0;
function showTrustMemberDiv() {

	if (document.getElementById("existingTrustMember").checked) {
		$("#existingTrustMemberDiv").show();
		isMapped = 1;
	}

	if (document.getElementById("newTrustMember").checked) {
		$("#existingTrustMemberDiv").hide();
		$(".TrustMemberDetailDTO").prop("disabled", false);
		$(".TrustMemberDetailDTO").val("");
		$(".roleMasterDTO").val("");
		isMapped = 0;
	}

}


function fetchTrustMemberdetail() {
	$.ajax({
		type: "GET",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		url: "/trustMember/",
		headers: {
			'API-VERSION': 1
		},
		success: function (data) {
			if (data.success == 1) {
				console.log("fetched Data" + JSON.stringify(data));
				trustMemberDetailMpgList = data.serviceResult;
				trustMasterList = trustMemberDetailMpgList.trustMasterDTOList;
				trustMemberDetailList = trustMemberDetailMpgList.trustMemberDetailDTOList;
				roleMasterList = trustMemberDetailMpgList.roleMasterDTOList;

				trustMasterList = trustMasterList.filter((x) => x.instId == instituteId);
				//newtrustMemberDetailList = trustMemberDetailMpgList.trustMemberDetailDTOList.filter((x) => x.instId == instituteId);
				newtrustMemberDetailList = trustMemberDetailMpgList.trustMemberDetailMpgDTOList;

				if(newtrustMemberDetailList.length == 0){
					$("#btnNext").hide();
				}else{
					$("#btnNext").show();
				}

				if(trustMemberDetailList.length == 0){
					$("#newTrustMember").prop("checked",true);
					$("#isMappedDiv").hide();
				}else{
					$("#isMappedDiv").show();
				}

				$("#trustNameDD").empty();

				if (trustMasterList.length > 0) {
					$("#trustNameDD").append(
						'<option value=""  selected="" disabled="">'
						+ "-- Select Trust--" + '</option>');

					for (var i = 0; i < trustMasterList.length; i++) {
						trustData = trustMasterList[i];
						$('#trustNameDD').append(
							'<option value="' + trustData.id + '">'
							+ trustData.name + '</option>').trigger('chosen:updated');

					}
				} else {
					$("#trustNameDD").append(
						'<option value=""  selected="selected" disabled="">'
						+ "-- No Trust Is Available --" + '</option>');
				}

				$("#trustMemberNameDD").empty();
				if (trustMemberDetailList.length > 0) {
					$("#trustMemberNameDD").append(
						'<option value=""  selected="" disabled="">'
						+ "-- Select --" + '</option>');

					for (var i = 0; i < trustMemberDetailList.length; i++) {
						trustMemberData = trustMemberDetailList[i];
						$('#trustMemberNameDD').append(
							'<option value="' + trustMemberData.id + '">'
							+ trustMemberData.name + '</option>').trigger('chosen:updated');

					}
				} else {
					$("#trustMemberNameDD").append(
						'<option value=""  selected="selected" disabled="">'
						+ "-- No Trust Member Is Available --" + '</option>');
				}

				$("#trustMemberRole").empty();

				if (roleMasterList.length > 0) {
					$("#trustMemberRole").append(
						'<option value=""  selected="" disabled="">'
						+ "-- Select Role--" + '</option>');

					for (var i = 0; i < roleMasterList.length; i++) {
						roleData = roleMasterList[i];
						$('#trustMemberRole').append(
							'<option value="' + roleData.id + '">'
							+ roleData.roleName + '</option>').trigger('chosen:updated');

					}
				} else {
					$("#trustMemberRole").append(
						'<option value=""  selected="selected" disabled="">'
						+ "-- No Role Available --" + '</option>');
				}

				fillTrustMemberDataTable(newtrustMemberDetailList);
			}
			else {
				Swal.fire({
					title: 'ERROR',
					html: data.message,
					icon: 'error',
					confirmButtonColor: '#3085d6',
					confirmButtonText: "OK",
					closeOnConfirm: false,
					allowOutsideClick: false,
				}).then((result) => {
					if (result.value) {

					}
				})

			}
		},
		error: function (data, status, error) {
			console.log("error " + error);
			console.log("messageList " + JSON.stringify(data.responseJSON.messageList));
			console.log("data " + JSON.stringify(data));
			console.log("status " + status);
			showAlerts(data.responseJSON.message, data.statusText, "alertMessage", false, data.responseJSON.serviceResult, 20000);
			if (data.status == "401") {
				showAlerts(data.responseJSON.message, data.statusText, "alertMessage", false, data.responseJSON.serviceResult, 20000);
			}
			else if (data.status == "500") {
				showAlerts(data.responseJSON.message, data.statusText, "alertMessage", false, data.responseJSON.serviceResult, 20000);
			}
		}
	});
}

function fillTrustMemberDataTable(data) {
	console.log("Filled data" + JSON.stringify(data));
	$('#trust_member_data_table')
		.DataTable(
			{

				"columnDefs": [{
					"className": "dt-center",
					"targets": "_all"
				}],
				"autoWidth": true,
				"processing": true,
				"data": data,
				"iDisplayLength": 50,
				"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				destroy: true,
				"order": [[0, "asc"]],

				"fnCreatedRow": function (row, data, index) {
					$('td', row).eq(0).html(index + 1);
				},
				bAutoWidth: false,
				"columns": [
					{
						"data": null,

					},
					{
						mRender: function (data, type, row) {
							return row.trustMemberDetailDTO.name;
						}
					},
					{
						mRender: function (data, type, row) {
							return row.trustMemberDetailDTO.pancardNo;
						}
					},
					{
						mRender: function (data, type, row) {
							return row.trustMemberDetailDTO.address;
						}
					},
					{
						mRender: function (data, type, row) {
							return row.trustMemberDetailDTO.phoneNo;
						}
					},
					{
						mRender: function (data, type, row) {
							return row.trustMemberDetailDTO.email;
						}
					},
					{
						"data": "trustMasterDTO.name"
					},
					{
						"data": "roleMasterDTO.roleName",
					},
					{
						mRender: function (data, type, row) {
							var trust_member_mpg_id = row.id;
							var isMappedd = row.isMapped;
							if (isMappedd == 1) {
								return "<a "
									+ " target='_blank'><i class='fa fa-trash fatbl text-info' onclick='deleteTrustMemberData(" + trust_member_mpg_id + ")' ></i>"
									+ "</a>"
							} else {
								return "<a "
									+ " target='_blank'><i class='fa fa-pencil fatbl text-info' onclick='fillTrustMemberFormDataForEdit(" + trust_member_mpg_id + ",1)'></i> <i class='fa fa-trash fatbl text-info' onclick='deleteTrustMemberData(" + trust_member_mpg_id + ")' ></i>"
									+ "</a>"
							}




						}
					}


				]
			});

}

function fillTrustMemberFormData(trust_member_id) {

	var trustMemberDataById = [];
	trustMemberDataById = trustMemberDetailList.filter((x) => x.id == trust_member_id);


	console.log("trustMemberDataById " + JSON.stringify(trustMemberDataById));
	if (trustMemberDataById != null) {
		$("#trustMemberName").focus();
		$("#trustMemberName").val(trustMemberDataById[0].name);
		$("#trustMemberPanNo").val(trustMemberDataById[0].pancardNo);
		$("#trustMemberPhoneNo").val(trustMemberDataById[0].phoneNo);
		$("#trustMemberEmail").val(trustMemberDataById[0].email);
		$("#trustMemberAddress").val(trustMemberDataById[0].address);
		$("#trustMemberRole").val(trustMemberDataById[0].roleMasterDTO.id);
		$("#hiddenTrustMemberId").val(trust_member_id);
		$("#hiddenTrustMemberDetailMpgId").val(0);



	}

}


function fillTrustMemberFormDataForEdit(trust_member_mpg_id) {

	var trustMemberDataById = [];
	trustMemberDataById = newtrustMemberDetailList.filter((x) => x.id == trust_member_mpg_id);


	console.log("trustMemberDataById " + JSON.stringify(trustMemberDataById));
	if (trustMemberDataById != null) {
		$("#isMappedDiv").hide();
		$("#trustMemberDiv").show();
		$("#trustMemberName").focus();
		$("#trustMemberName").val(trustMemberDataById[0].trustMemberDetailDTO.name);
		$("#trustMemberPanNo").val(trustMemberDataById[0].trustMemberDetailDTO.pancardNo);
		$("#trustMemberPhoneNo").val(trustMemberDataById[0].trustMemberDetailDTO.phoneNo);
		$("#trustMemberEmail").val(trustMemberDataById[0].trustMemberDetailDTO.email);
		$("#trustMemberAddress").val(trustMemberDataById[0].trustMemberDetailDTO.address);
		$("#trustMemberRole").val(trustMemberDataById[0].roleMasterDTO.id);
		$("#trustNameDD").val(trustMemberDataById[0].trustMasterDTO.id);
		$("#hiddentrustMasterId").val(trustMemberDataById[0].trustMasterDTO.id);
		$("#hiddenTrustMemberDetailMpgId").val(trust_member_mpg_id);
		$("#hiddenTrustMemberId").val(trustMemberDataById[0].trustMemberDetailDTO.id);

	}

}

$("#trustMemberNameDD").change(function () {
	fillTrustMemberFormData($("#trustMemberNameDD").val(), 0);
	$(".TrustMemberDetailDTO").prop("disabled", true);


});

$("#trustNameDD").change(function () {
	$("#trustMemberDiv").show();
	$("#hiddentrustMasterId").val($("#trustNameDD").val());
	selectTrustName();
	$(".TrustMemberDetailDTO").prop("disabled", false);
	$(".TrustMemberDetailDTO").val("");
	$(".roleMasterDTO").val("");
});

function selectTrustName() {
	var id = $("#trustNameDD").val();
	$("#trustMemberNameDD").empty();
	$("#trustMemberNameDD").append(
		'<option value=""  selected="" disabled="">'
		+ "-- Select --" + '</option>');

	var list = newtrustMemberDetailList.filter((x) => x.trustMasterDTO.id == id);

	if (list.length > 0) {
		for (var i = 0; i < trustMemberDetailList.length; i++) {
			var isExistsOrNot = list.some((datas) => (datas.id == trustMemberDetailList[i].id ));

			if (isExistsOrNot == false) {
				console.log("insideif");
				$("#trustMemberNameDD").append(new Option(trustMemberDetailList[i].name, trustMemberDetailList[i].id));
			}
		}
	}
	if (list.length == 0) {
		for (var i = 0; i < trustMemberDetailList.length; i++) {
			$("#trustMemberNameDD").append(new Option(trustMemberDetailList[i].name, trustMemberDetailList[i].id));

		}
	} 
	/* 
	if (list.length > 0) {
		for (var i = 0; i < trustMemberDetailList.length; i++) {
			var isExistsOrNot = list.some((datas) => (datas.trustMemberDetailDTO.name == trustMemberDetailList[i].name && datas.trustMasterDTO.name == trustMemberDetailList[i].trustMasterDTO.name));

			if (isExistsOrNot == false) {
				console.log("insideif");
				$("#trustMemberNameDD").append(new Option(trustMemberDetailList[i].name, trustMemberDetailList[i].id));
			}
		}
	}
	if (list.length == 0) {
		for (var i = 0; i < trustMemberDetailList.length; i++) {
			$("#trustMemberNameDD").append(new Option(trustMemberDetailList[i].name, trustMemberDetailList[i].id));

		}
	} */
}
function deleteTrustMemberData(trustMemberMpgId) {
	Swal.fire({
		title: 'Are you sure?',
		html: "You want to Delete Trust Member",
		icon: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		confirmButtonText: "YES",
		cancelButtonColor: '#d33',
		cancelButtonText: "Cancel",
		closeOnConfirm: false,
		closeOnCancel: false,
		allowOutsideClick: false
	}).then((result) => {
		if (result.value) {

			if (trustMemberMpgId != 0) {
				$.ajax({
					type: "DELETE",
					url: "/trustMember/",
					headers: {
						'API-VERSION': 1
					},
					data: { trustMemberMpgId: trustMemberMpgId },
					success: function (data) {
						if (data.success == 1) {
							showAlerts(data.message, data.messageList, "alertMessage", false, data.serviceResult, 2000);
							fetchTrustMemberdetail();
						} else {
							Swal.fire({
								title: 'ERROR',
								html: data.message,
								icon: 'error',
								confirmButtonColor: '#3085d6',
								confirmButtonText: "OK",
								closeOnConfirm: false,
								allowOutsideClick: false,
							}).then((result) => {
								if (result.value) {

								}
							})

						}
					},
					error: function (error) {
						Swal.fire({
							title: 'ERROR',
							html: "Something Went Wrong, Please Try Again",
							icon: 'error',
							confirmButtonColor: '#3085d6',
							confirmButtonText: "OK",
							closeOnConfirm: false,
							allowOutsideClick: false,
						}).then((result) => {
							if (result.value) {

							}
						})

					}
				});

			}


		}

	})
}