var instBasicDetail = [];
var instituteId = $("#sessionInstId").text();

function saveInstituteBasicDetail() {
	var applicationId = document.getElementById("applicationId").value;
	var verfied = true;
	var text = "";
	if ($.trim($("#acType").val()) === "") {
		verfied = false;
		var text = "Please Enter accreditation type";
		document.getElementById("acTypeError").innerHTML = text;
	}
	if ($.trim($("#acGrade").val()) === "") {
		verfied = false;
		var text = "Please Enter accreditation grade";
		document.getElementById("acGradeError").innerHTML = text;
	}
	if ($.trim($("#acFromDate").val()) === "") {
		verfied = false;
		var text = "Please select accreditation from date";
		document.getElementById("acFromDateError").innerHTML = text;
	}
	if ($.trim($("#acToDate").val()) === "") {
		verfied = false;
		var text = "Please select  accreditation to date";
		document.getElementById("acToDateError").innerHTML = text;
	}
	if ($.trim($("#afType").val()) === "") {
		verfied = false;
		var text = "Please Enter affiliation type";
		document.getElementById("afTypeError").innerHTML = text;
	}
	if ($.trim($("#afLetterNo").val()) === "") {
		verfied = false;
		var text = "Please Enter affiliatyion letter no";
		document.getElementById("afLetterNoError").innerHTML = text;
	}
	var data = {
		id: document.getElementById("instBasicDetailId").value,
		accrediationType: document.getElementById("acType").value,
		accrediationGrade: document.getElementById("acGrade").value,
		affiliationType: document.getElementById("afType").value,
		affiliationLetterNo: document.getElementById("afLetterNo").value,
		instId: instituteId,
		accrediationFromDate: document.getElementById("acFromDate").value,
		accrediationToDate: document.getElementById("acToDate").value,
		adminBlockDetailDTO: {
			id: document.getElementById("blockId").value
		},
		applicationId: document.getElementById("applicationId").value
	};
	console.log("before save" + JSON.stringify(data));
	if (verfied) {

		$.ajax({
			type: "POST",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			url: "/inst/",
			headers: {
				'API-VERSION': 1
			},
			data: JSON.stringify(data),
			success: function (data) {
				console.log("DATA " + JSON.stringify(data));
				if (data.success == 1) {
					showAlerts(data.message, data.messageList.NoError, "alertMessage", false, data.serviceResult, 2000);
					fetchInstBasicdetail(applicationId);
					cleanError('error');
				}
				else {
					Swal.fire({
						title: 'ERROR',
						html: data.message,
						icon: 'error',
						confirmButtonColor: '#3085d6',
						confirmButtonText: "OK",
						closeOnConfirm: false,
						allowOutsideClick: false,
					}).then((result) => {
						if (result.value) {

						}
					})

				}
			},
			error: function (data, status, error) {
				console.log("error " + error);
				console.log("messageList " + JSON.stringify(data.responseJSON.messageList));
				console.log("data " + JSON.stringify(data));
				console.log("status " + status);

				var messageList = data.responseJSON.messageList;
				let myMap = new Map();
				myMap = messageList;
				console.log("myMap" + JSON.stringify(myMap));
				for (const [key, value] of Object.entries(myMap)) {
					//console.log(`${key}: ${value}`);
					// console.log("Key "+key );
					//console.log("value "+value );
					document.getElementById("" + key).innerHTML = value;
				}
				showAlerts(data.responseJSON.serviceResult, data.statusText, "alertMessage", false, data.responseJSON.message, 2000);
			}
		});
	}

}


function fetchInstBasicdetail(applicationId) {
	$.ajax({
		type: "GET",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		url: "/inst/" + applicationId,
		headers: {
			'API-VERSION': 1
		},
		success: function (data) {
			if (data.success == 1) {
				//alert("fetched");
				console.log("fetched Data" + JSON.stringify(data));
				//showAlerts(data.message,data.messageList.NoError,"alertMessage",false,data.serviceResult,2000);
				instBasicDetail = data.serviceResult;
				if (instBasicDetail != null) {
					$("#acType").val(instBasicDetail.accrediationType);
					$("#acGrade").val(instBasicDetail.accrediationGrade);
					$("#afType").val(instBasicDetail.affiliationType);
					$("#afLetterNo").val(instBasicDetail.affiliationLetterNo);
					$("#instBasicDetailId").val(instBasicDetail.id);
					$("#acFromDate").val(instBasicDetail.accrediationFromDate);
					$("#acToDate").val(instBasicDetail.accrediationToDate);
					$("#nextBtn").prop("disabled", false);
					$("#saveBtn").prop("value", "Update");
				} else {
					$("#nextBtn").prop("disabled", true);

				}


			}
			else {
				Swal.fire({
					title: 'ERROR',
					html: data.message,
					icon: 'error',
					confirmButtonColor: '#3085d6',
					confirmButtonText: "OK",
					closeOnConfirm: false,
					allowOutsideClick: false,
				}).then((result) => {
					if (result.value) {

					}
				})

			}
		},
		error: function (data, status, error) {

			console.log("error " + error);
			console.log("messageList " + JSON.stringify(data.responseJSON.messageList));
			console.log("data " + JSON.stringify(data));
			console.log("status " + status);
			showAlerts(data.responseJSON.message, data.statusText, "alertMessage", false, data.responseJSON.serviceResult, 20000);
			if (data.status == "401") {
				showAlerts(data.responseJSON.message, data.statusText, "alertMessage", false, data.responseJSON.serviceResult, 20000);
			}
			else if (data.status == "500") {
				showAlerts(data.responseJSON.message, data.statusText, "alertMessage", false, data.responseJSON.serviceResult, 20000);
			}
		}
	});

}

$("#acFromDate").change(function () {
	var acFromDate = $("#acFromDate").val();

	$('#acToDate').attr('min', acFromDate);
});

$("#nextBtn").click(function () {
	var applicationId = $("#applicationId").val();
	//alert(applicationId);
	var encryptedAES = CryptoJS.AES.encrypt("applicationId=" + applicationId, "My Secret Passphrase");
	location.href = "/student/home?" + encryptedAES;
});

