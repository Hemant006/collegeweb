var libraryList = [];
function fetchLibraryDetail(applicationId) {

	$.ajax({
		type: "GET",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		url: "/lib/" + applicationId,
		headers: {
			'API-VERSION': 1
		},
		success: function (data) {
			if (data.success == 1) {
				console.log("Library Data" + JSON.stringify(data));
				libraryList = data.serviceResult;
				console.log("libraryList" + JSON.stringify(libraryList));
				console.log("size" + libraryList.lookupTypeDTOList.length);
				var j = 0;
				var count = 1;
				for (i = 0; i < libraryList.lookupTypeDTOList.length; i++) {

					if (libraryList.lookupTypeDTOList[i].description == "1") {
						$("#libraryDetailDiv").append(`<tr>
									 <td class="text-center">${count}</td>
									<td class="text-center">
									<p> <b>${libraryList.lookupTypeDTOList[i].name}</b>
									</p>
									</td>
									<td>
									<input type="text" class="form-control text-center" id="instAnswer${i}"  min="0" style="text-align: right;"
										name="instAnswer${i}"  value="0" onkeypress="return isNumber(event)" maxlength="10">
										<div id="instAnswerAlert${i}" style="color: red;"></div>
									</td>
									<td hidden="">
									<input type="hidden" id="hiddenLibTransMpgId${i}" value="0"
										name="hiddenLibTransMpgId${i} ">
									</td>
									</tr>`);
						count++;
					}
					else {
						$("#compLibraryDetailDiv").append(`<tr>
								 	<td class="text-center">${j + 1}</td>
									<td class="text-center">
									<p> <b>${libraryList.lookupTypeDTOList[i].name}</b>
									</p>
									</td>
									<td class="text-center"> 
									<div class="form-check-inline text-center">
								  	<label class="form-check-label">
								    <input type="radio" class="form-check-input text-center"  name="libDetail${i}" id="instAnswerYes${i}" value="1" checked> Yes
								  	</label>
									</div>
									
									<div class="form-check-inline">
								 	 <label class="form-check-label">
								    <input type="radio" class="form-check-input text-center " name="libDetail${i}" id="instAnswerNo${i}" value="0"> No
								  </label><div id="libDetailAlert${i}" style="color: red;"></div>
								</div>
								</td>
									<td hidden="">
									<input type="hidden" id="hiddenLibTransMpgId${i}" value="0"
										name="hiddenLibTransMpgId${i} ">
									</td>
									</tr>`);
						j++;
					}

				}
				$("#compLibraryDetailDiv").append(`<tr>
						 	<td class="text-center">${j + 1}
						 	</td >
							<td class="text-center">
							<p> <b>${"Remark"}</b>
							</p>
							</td>
							<td colspan="3">
							<textarea class="form-control" id="remark"  onchange="" maxlength="255"></textarea>
							<div id="remarkAlert" style="color: red;"></div>
						
							</textarea>
							</td>
							<td hidden="">
							<input type="hidden" id="hiddenRemark" value="0"
								name="hiddenRemark ">
							</td>
							</tr>`);

				console.log("libraryList.lookupTypeDTOList" + JSON.stringify(libraryList.libraryDetailDTOlist));
				if (libraryList.libraryDetailDTOlist != null) {
					for (var k = 0; k < libraryList.libraryDetailDTOlist.length; k++) {
						//alert(libraryList.libraryDetailDTOlist[k].answer);
						$("#instAnswer" + k).val(libraryList.libraryDetailDTOlist[k].answer);
						if (libraryList.libraryDetailDTOlist[k].answer == 1 && libraryList.libraryDetailDTOlist[k].answer != '') {
							$("#instAnswerYes" + k).prop("checked", true);
						}
						if (libraryList.libraryDetailDTOlist[k].answer == 0 && libraryList.libraryDetailDTOlist[k].answer != '') {
							$("#instAnswerNo" + k).prop("checked", true);
						}
						$("#hiddenLibTransMpgId" + k).val(libraryList.libraryDetailDTOlist[k].id);
							
					}
					
				}
				if (libraryList.libraryTransactionMpgDTO.id != null) {
					$("#remark").val(libraryList.libraryTransactionMpgDTO.remark);
					$("#hiddenRemark").val(libraryList.libraryTransactionMpgDTO.id);
					$("#nextBtn").prop("disabled", false);
					$("#saveBtn").prop("value", "UPDATE");
				}else{
					$("#nextBtn").prop("disabled", true);
				}

			}
			else {
				Swal.fire({
					title: 'ERROR',
					html: data.message,
					icon: 'error',
					confirmButtonColor: '#3085d6',
					confirmButtonText: "OK",
					closeOnConfirm: false,
					allowOutsideClick: false,
				}).then((result) => {
					if (result.value) {

					}
				})

			}
		},
		error: function (data, status, error) {
			console.log("error " + error);
			console.log("messageList " + JSON.stringify(data.responseJSON.messageList));
			console.log("data " + JSON.stringify(data));
			console.log("status " + status);
			showAlerts(data.responseJSON.message, data.statusText, "alertMessage", false, data.responseJSON.serviceResult, 20000);
			if (data.status == "401") {
				showAlerts(data.responseJSON.message, data.statusText, "alertMessage", false, data.responseJSON.serviceResult, 20000);
			}
			else if (data.status == "500") {
				showAlerts(data.responseJSON.message, data.statusText, "alertMessage", false, data.responseJSON.serviceResult, 20000);
			}
		}
	});
}
function saveLibraryDetail() {
	var libraryDetaillist = [];
	var answer = null;
	var verfied = true;
		var text = "Please Enter Value";
	for (var i = 0; i < libraryList.lookupTypeDTOList.length; i++) {
		if (document.getElementsByName("libDetail" + i).length == 0) {
		
			if (document.getElementById("instAnswer" + i).value == '') {
				document.getElementById("instAnswerAlert" + i).innerHTML = text;
				verfied = false;
			}
			
		}
	}

	for (var i = 0; i < libraryList.lookupTypeDTOList.length; i++) {
		var lableId = libraryList.lookupTypeDTOList[i].id;
		if (document.getElementsByName("libDetail" + i).length == 0) {
			if (document.getElementById("instAnswer" + i).value != null) {
				answer = document.getElementById("instAnswer" + i).value;
			}
		} else {
			if (document.getElementById("instAnswerYes" + i).checked == true) {
					answer = "1";
				} else if (document.getElementById("instAnswerNo" + i).checked == true ) {
					answer = "0";
				}

		}
		//alert("answer"+answer);
		data = {
			id: document.getElementById("hiddenLibTransMpgId" + i).value,
			answer: answer,
			lookupTypeDTO: {
				id: lableId
			}
		};
		//InstituteUtilizedDTOList.push(data);
		libraryDetaillist.push(data);
		console.log("List " + JSON.stringify(libraryDetaillist));

	}
	var libraryTransactionMpgDTO = {
		id: document.getElementById("hiddenRemark").value,
		remark: document.getElementById("remark").value
	}

	var customLibraryDetailDTO = {
		libraryDetailDTOlist: libraryDetaillist,
		libraryTransactionMpgDTO: libraryTransactionMpgDTO
	}
	console.log("before save" + JSON.stringify(customLibraryDetailDTO));

	if (verfied) {
		$.ajax({
			type: "POST",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			url: "/lib/",
			headers: {
				'API-VERSION': 1
			},
			data: JSON.stringify(customLibraryDetailDTO),
			success: function (data) {
				console.log("DATA " + JSON.stringify(customLibraryDetailDTO));
				if (data.success == 1) {
					showAlerts(data.message, data.messageList.NoError, "alertMessage", false, data.serviceResult, 2000);
					cleanError('error');

				}
				else {
					Swal.fire({
						title: 'ERROR',
						html: data.message,
						icon: 'error',
						confirmButtonColor: '#3085d6',
						confirmButtonText: "OK",
						closeOnConfirm: false,
						allowOutsideClick: false,
					}).then((result) => {
						if (result.value) {

						}
					})
				}
			},
			error: function (data, status, error) {
				console.log("error " + error);
				console.log("messageList " + JSON.stringify(data.responseJSON.messageList));
				console.log("data " + JSON.stringify(data));
				console.log("status " + status);
				showAlerts(data.responseJSON.message, data.statusText, "alertMessage", true, data.responseJSON.serviceResult, 20000);
				if (data.status == "417") {
					alert("417");
					var messageList = data.responseJSON.messageList;
					let myMap = new Map();
					myMap = messageList;
					console.log("myMap" + JSON.stringify(myMap));
					for (const [key, value] of Object.entries(myMap)) {
						//console.log(`${key}: ${value}`);
						// console.log("Key "+key );
						//console.log("value "+value );
						document.getElementById("" + key).innerHTML = value;
					}
					showAlerts(data.responseJSON.message, data.statusText, "alertMessage", true, data.responseJSON.serviceResult, 20000);
				}
				else if (data.status == "409") {
					alert("Constraint Violation Error");
					let contraintViolationMsg = data.responseJSON.message.split('for key');
					console.log("words" + contraintViolationMsg[0]);
					showAlerts(contraintViolationMsg[0], data.statusText, "alertMessage", true, data.responseJSON.serviceResult, 20000);
				}
				else {
					Swal.fire({
						title: 'ERROR',
						html: "Something Went Wrong, Please Try Again",
						icon: 'error',
						confirmButtonColor: '#3085d6',
						confirmButtonText: "OK",
						closeOnConfirm: false,
						allowOutsideClick: false,
					}).then((result) => {
						if (result.value) {

						}
					})
				}
			}
		});
	}
}
$("#nextBtn").click(function () {
	var applicationId = $("#applicationId").val();
	//alert(applicationId);
	var encryptedAES = CryptoJS.AES.encrypt("applicationId=" + applicationId, "My Secret Passphrase");
	location.href = "/infrastructure/home?" + encryptedAES;
});
$("#btnBack").click(function () {
    var applicationId = $("#applicationId").val();
    //alert(applicationId);
    var encryptedAES = CryptoJS.AES.encrypt("applicationId=" + applicationId, "My Secret Passphrase");
    location.href = "/staff/home?" + encryptedAES;
});