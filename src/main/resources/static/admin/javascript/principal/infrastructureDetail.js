
var infrastructureList = [];
function fetchInfrastructureDetail(applicationId) {

	$.ajax({
		type: "GET",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		url: "/infrastructure/" + applicationId,
		headers: {
			'API-VERSION': 1
		},
		success: function (data) {
			if (data.success == 1) {
				//console.log("Library Data" + JSON.stringify(data));
				infrastructureList = data.serviceResult;
				console.log("Library Data" + JSON.stringify(infrastructureList));
				var j = 0;
				var count = 1;
				for (i = 0; i < infrastructureList.lookupTypeDTOList.length; i++) {
					if (infrastructureList.lookupTypeDTOList[i].description == "1") {
						$("#infrastrctureDetailDiv").append(`<tr>
									 <td class="text-center">${count}</td>
									<td class="text-center">
									<p> <b>${infrastructureList.lookupTypeDTOList[i].name}</b>
									</p>
									</td>
									<td>
									<input type="text" class="form-control" id="instAnswer${i}"  min="0" style="text-align: right;" maxlength="10"
										name="instAnswer${i}" onkeypress="return (event.charCode > 47 && event.charCode < 58) || (event.charCode ==46);"
										placeholder="${infrastructureList.lookupTypeDTOList[i].placeholder}">
										<div id="instAnswerAlert${i}" style="color: red;"></div>
									</td>
									<td hidden="">
									<input type="hidden" id="hiddenInfraTransMpgId${i}" value="0"
										name="hiddenInfraTransMpgId${i} ">
									</td>
									</tr>`);
						count++;
					}
				}
				$("#compInfrastrctureDetailDiv").append(`<tr>
				 	<td class="text-center">${count}</td>			
							<td class="text-center">
							<p> <b>${"Remark"}</b>
							</p>
							</td>
							<td colspan="3">
							<textarea class="form-control" id="remark"  onchange="" maxlength="255"></textarea>
							<div id="remarkAlert" style="color: red;"></div>
						
							</textarea>
							</td>
							<td hidden="">
							<input type="hidden" id="hiddenRemark" value="0"
								name="hiddenRemark ">
							</td>
							</tr>`);
				if (infrastructureList.infrastructureDetailDTOList != null) {
					for (var k = 0; k < infrastructureList.infrastructureDetailDTOList.length; k++) {
						/* alert(JSON.stringify(infrastructureList.libraryDetailDTOlist[k].answer)); */
						$("#instAnswer" + k).val(infrastructureList.infrastructureDetailDTOList[k].answer);
						$("#hiddenInfraTransMpgId" + k).val(infrastructureList.infrastructureDetailDTOList[k].id);
					}

				}
				if (infrastructureList.infrastructureTransactionMpgDTO.id != null) {
					$("#remark").val(infrastructureList.infrastructureTransactionMpgDTO.remark);
					$("#hiddenRemark").val(infrastructureList.infrastructureTransactionMpgDTO.id);
					$("#nextBtn").prop("disabled", false);
					$("#saveBtn").prop("value", "UPDATE");
				} else {
					$("#nextBtn").prop("disabled", true);
				}
			}
			else {
				Swal.fire({
					title: 'ERROR',
					html: data.message,
					icon: 'error',
					confirmButtonColor: '#3085d6',
					confirmButtonText: "OK",
					closeOnConfirm: false,
					allowOutsideClick: false,
				}).then((result) => {
					if (result.value) {

					}
				})

			}
		},
		error: function (data, status, error) {
			console.log("error " + error);
			console.log("messageList " + JSON.stringify(data.responseJSON.messageList));
			console.log("data " + JSON.stringify(data));
			console.log("status " + status);
			showAlerts(data.responseJSON.message, data.statusText, "alertMessage", false, data.responseJSON.serviceResult, 20000);
			if (data.status == "401") {
				showAlerts(data.responseJSON.message, data.statusText, "alertMessage", false, data.responseJSON.serviceResult, 20000);
			}
			else if (data.status == "500") {
				showAlerts(data.responseJSON.message, data.statusText, "alertMessage", false, data.responseJSON.serviceResult, 20000);
			}
		}
	});
}
function saveInfrastrctureDetail() {
	var infrastctureDetaillist = [];
	var answer = null;
	var verfied = true;
		var text = "Please Enter Value";
	for (var i = 0; i < infrastructureList.lookupTypeDTOList.length; i++) {
			if (document.getElementById("instAnswer" + i).value == '') {
				document.getElementById("instAnswerAlert" + i).innerHTML = text;
				verfied = false;
			}
	}
	for (var i = 0; i < infrastructureList.lookupTypeDTOList.length; i++) {
		var lableId = infrastructureList.lookupTypeDTOList[i].id;
			if (document.getElementById("instAnswer" + i).value != null) {
				answer = document.getElementById("instAnswer" + i).value;
			}
		data = {
			id: document.getElementById("hiddenInfraTransMpgId" + i).value,
			answer: answer,
			lookupTypeDTO: {
				id: lableId
			}
		};
		//InstituteUtilizedDTOList.push(data);
		infrastctureDetaillist.push(data);
		console.log("List " + JSON.stringify(infrastctureDetaillist));

	}
	var infrastrctureTransactionMpgDTO = {
		id: document.getElementById("hiddenRemark").value,
		remark: document.getElementById("remark").value
	}

	var customInfrastrctureDetailDTO = {
		infrastructureDetailDTOList: infrastctureDetaillist,
		infrastructureTransactionMpgDTO: infrastrctureTransactionMpgDTO
	}
	console.log("before save" + JSON.stringify(customInfrastrctureDetailDTO));
	
	if (verfied) {
		$.ajax({
			type: "POST",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			url: "/infrastructure/",
			headers: {
				'API-VERSION': 1
			},
			data: JSON.stringify(customInfrastrctureDetailDTO),
			success: function (data) {
				console.log("DATA " + JSON.stringify(customInfrastrctureDetailDTO));
				if (data.success == 1) {
					showAlerts(data.message, data.messageList.NoError, "alertMessage", false, data.serviceResult, 2000);

					cleanError('error');

				}
				else {
					Swal.fire({
						title: 'ERROR',
						html: data.message,
						icon: 'error',
						confirmButtonColor: '#3085d6',
						confirmButtonText: "OK",
						closeOnConfirm: false,
						allowOutsideClick: false,
					}).then((result) => {
						if (result.value) {

						}
					})
				}
			},
			error: function (data, status, error) {
				console.log("error " + error);
				console.log("messageList " + JSON.stringify(data.responseJSON.messageList));
				console.log("data " + JSON.stringify(data));
				console.log("status " + status);
				showAlerts(data.responseJSON.message, data.statusText, "alertMessage", true, data.responseJSON.serviceResult, 20000);
				if (data.status == "417") {
					alert("417");
					var messageList = data.responseJSON.messageList;
					let myMap = new Map();
					myMap = messageList;
					console.log("myMap" + JSON.stringify(myMap));
					for (const [key, value] of Object.entries(myMap)) {
						//console.log(`${key}: ${value}`);
						// console.log("Key "+key );
						//console.log("value "+value );
						document.getElementById("" + key).innerHTML = value;
					}
					showAlerts(data.responseJSON.message, data.statusText, "alertMessage", true, data.responseJSON.serviceResult, 20000);
				}
				else if (data.status == "409") {
					alert("Constraint Violation Error");
					let contraintViolationMsg = data.responseJSON.message.split('for key');
					console.log("words" + contraintViolationMsg[0]);
					showAlerts(contraintViolationMsg[0], data.statusText, "alertMessage", true, data.responseJSON.serviceResult, 20000);
				}
				else {
					Swal.fire({
						title: 'ERROR',
						html: "Something Went Wrong, Please Try Again",
						icon: 'error',
						confirmButtonColor: '#3085d6',
						confirmButtonText: "OK",
						closeOnConfirm: false,
						allowOutsideClick: false,
					}).then((result) => {
						if (result.value) {

						}
					})
				}
			}
		});
	}
}

function showRentalPropertyDiv() {
	if (document.getElementById("isRentalProperty").checked) {
		$("#rentalPropertyDiv").show();

	}
	if (document.getElementById("isnotRentalProperty").checked) {
		$("#rentalPropertyDiv").hide();

	}

}
$("#nextBtn").click(function () {
	var applicationId = $("#applicationId").val();
	//alert(applicationId);
	var encryptedAES = CryptoJS.AES.encrypt("applicationId=" + applicationId, "My Secret Passphrase");
	location.href = "/physicalEducationalFacilities/home?" + encryptedAES;
});
$("#btnBack").click(function () {
	var applicationId = $("#applicationId").val();
	//alert(applicationId);
	var encryptedAES = CryptoJS.AES.encrypt("applicationId=" + applicationId, "My Secret Passphrase");
	location.href = "/lib/home?" + encryptedAES;
});