var phoneNoList = [];
var emailIdList = [];

var emailPhoneNoMpgList = [];

var instituteId = $("#sessionInstId").text();


var i = 1;
var j = 1;
function saveTrustDetail() {

	var verfied = true;
	var text = "";
	if ($.trim($("#trustName").val()) === "") {
		verfied = false;
		var text = "Please Enter trust name";
		document.getElementById("name").innerHTML = text;
	}
	if ($.trim($("#trustWebUrl").val()) === "") {
		verfied = false;
		var text = "Please Enter website url ";
		document.getElementById("websiteUrl").innerHTML = text;
	}
	if ($.trim($("#trustAddress").val()) === "") {
		verfied = false;
		var text = "Please Enter trust address";
		document.getElementById("address").innerHTML = text;
	}
	if (isMapped == 1) {
		if ($.trim($("#trustNameDD").val()) === "") {
			verfied = false;
			var text = "Please Select trust";
			document.getElementById("trustNameDDError").innerHTML = text;
		}
	}



	var phoneList = {};
	for (var k = 0; k < i; k++) {
		phoneList = {
			id: $("#hiddenTrustPhoneId" + k).val(),
			phoneNo: $("#trustPhoneNo" + k).val().trim(),
			type: 2
		};
		if ($.trim($("#trustPhoneNo" + k).val()) === "") {
			verfied = false;
			var text = "Please Enter phone Number";
			document.getElementById("phoneNoMpgDTOList[" + k + "].phoneNo").innerHTML = text;
		} else {
			phoneNoList.push(phoneList);
		}

	}

	for (var k = 0; k < j; k++) {
		var emailList = {
			id: $("#hiddenTrustEmailId" + k).val(),
			email: $("#trustEmail" + k).val().trim(),
			type: 1
		};

		if ($.trim($("#trustEmail" + k).val()) === "") {
			verfied = false;
			var text = "Please Enter Email Address";
			document.getElementById("emailIdMpgDTOList[" + k + "].email").innerHTML = text;
		} else {
			emailIdList.push(emailList);
		}


	}

	console.log("emailIdList" + JSON.stringify(emailIdList));

	console.log("PhoneList" + JSON.stringify(phoneNoList));
	console.log("isMapped " + isMapped);
	var instituteTrustMpgList = {
		id: document.getElementById("hiddenInstTrustMpgId").value,
		trustMasterDTO: {
			id: document.getElementById("hiddenTrustMasterId").value
		},
		isMapped: isMapped
	}
	var data = {
		id: document.getElementById("hiddenTrustMasterId").value,
		name: document.getElementById("trustName").value.trim(),
		websiteUrl: document.getElementById("trustWebUrl").value.trim(),
		address: document.getElementById("trustAddress").value.trim(),
		phoneNoMpgDTOList: phoneNoList,
		emailIdMpgDTOList: emailIdList,
		instituteTrustMpgDTOList: instituteTrustMpgList


	}
	console.log("before save" + JSON.stringify(data));
	if (verfied) {
		$.ajax({
			type: "POST",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			url: "/trust/",
			headers: {
				'API-VERSION': 1
			},
			data: JSON.stringify(data),
			success: function (data) {
				console.log("DATA " + JSON.stringify(data));
				if (data.success == 1) {
					fillTrustDataTable(instTrustDetail);
					showAlerts(data.message, data.messageList.NoError, "alertMessage", false, data.serviceResult, 1000);
					cleanValidationDto('TrustMasterDTO');
					cleanValidationDto('emailIdMpgDTOList');
					cleanValidationDto('phoneNoMpgDTOList');
					cleanError('error');
					$("#trustNameDD").val("");

					fetchTrustMasterdetail();

				}
				else {
					Swal.fire({
						title: 'ERROR',
						html: data.message,
						icon: 'error',
						confirmButtonColor: '#3085d6',
						confirmButtonText: "OK",
						closeOnConfirm: false,
						allowOutsideClick: false,
					}).then((result) => {
						if (result.value) {

						}
					})
				}
			},
			error: function (data, status, error) {
				console.log("error " + error);
				console.log("messageList " + JSON.stringify(data.responseJSON.messageList));
				console.log("data " + JSON.stringify(data));
				console.log("status " + status);
				showAlerts(data.responseJSON.message, data.statusText, "alertMessage", true, data.responseJSON.serviceResult, 1000);
				if (data.status == "417") {
					alert("417");
					var messageList = data.responseJSON.messageList;
					let myMap = new Map();
					myMap = messageList;
					console.log("myMap" + JSON.stringify(myMap));
					for (const [key, value] of Object.entries(myMap)) {
						//console.log(`${key}: ${value}`);
						// console.log("Key "+key );
						//console.log("value "+value );
						document.getElementById("" + key).innerHTML = value;
					}
					emailIdList = [];
					phoneNoList = [];
					showAlerts(data.responseJSON.message, data.statusText, "alertMessage", true, data.responseJSON.serviceResult, 1000);
				}
				else if (data.status == "409") {
					alert("Constraint Violation Error");
					let contraintViolationMsg = data.responseJSON.message.split('for key');
					console.log("words" + contraintViolationMsg[0]);
					console.log("data.statusText" + data.statusText);
					console.log("data.responseJSON.serviceResult " + data.responseJSON.serviceResult);
					showAlerts(contraintViolationMsg[0], data.statusText, "alertMessage", true, data.responseJSON.serviceResult, 1000);
					//showAlerts(data.message, data.messageList, "alertMessage", false, data.serviceResult, 2000);
				}
				else {
					Swal.fire({
						title: 'ERROR',
						html: "Something Went Wrong, Please Try Again",
						icon: 'error',
						confirmButtonColor: '#3085d6',
						confirmButtonText: "OK",
						closeOnConfirm: false,
						allowOutsideClick: false,
					}).then((result) => {
						if (result.value) {

						}
					})
				}
			}
		});
	}

}
var trustMasterdetail = [];
var instTrustDetail = [];
function fetchTrustMasterdetail() {
	$.ajax({
		type: "GET",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		url: "/trust/",
		headers: {
			'API-VERSION': 1
		},
		success: function (data) {
			if (data.success == 1) {
				//alert("fetched");
				//console.log("fetched Data" + JSON.stringify(data));
				//showAlerts(data.message,data.messageList.NoError,"alertMessage",false,data.serviceResult,2000);
				trustMasterdetail = data.serviceResult.trustMasterDTOList;
				console.log("fetch data:"+JSON.stringify(data.serviceResult));
				/* for (var i = 0; i < trustMasterdetail.length; i++) {
					if (trustMasterdetail[i].instId == instituteId) {
						instTrustDetail.push(trustMasterdetail[i]);

					}
				} */
				
				if(trustMasterdetail.length == 0){
					
					$("#newTrust").prop("checked", true);
					$("#isMappedDiv").hide();
					$("#btnNext").hide();
				}else{
					$("#isMappedDiv").show();
					$("#btnNext").show();
				}

				if(data.serviceResult.instituteTrustMpgDTOList.length>0){
					for(var i=0;i<data.serviceResult.instituteTrustMpgDTOList.length;i++){
						var isExistsOrNot = trustMasterdetail.some((datas) => datas.name == data.serviceResult.instituteTrustMpgDTOList[i].trustMasterDTO.name);
					    var getIndex = trustMasterdetail.findIndex((datas) => datas.name == data.serviceResult.instituteTrustMpgDTOList[i].trustMasterDTO.name);
					    if (isExistsOrNot) {
					    	trustMasterdetail.splice(getIndex, 1);
					        
					    }
					}
					
				}
				instTrustDetail = data.serviceResult.instituteTrustMpgDTOList
				//console.log("instTrustDetail" + JSON.stringify(instTrustDetail));
				//console.log("trustMasterdetail" + JSON.stringify(trustMasterdetail));

				fillTrustDataTable(instTrustDetail);

				$("#trustNameDD").empty();

				if (trustMasterdetail.length > 0) {
					$("#trustNameDD").append(
						'<option value=""  selected="" disabled="">'
						+ "-- Select --" + '</option>');


					for (var i = 0; i < trustMasterdetail.length; i++) {
						trustData = trustMasterdetail[i];
						$('#trustNameDD').append(
							'<option value="' + trustData.id + '">'
							+ trustData.name + '</option>').trigger('chosen:updated');

					}
				} else {
					$("#trustNameDD").append(
						'<option value=""  selected="selected" disabled="">'
						+ "-- No Trust Available --" + '</option>');
				}

			}
			else {
				Swal.fire({
					title: 'ERROR',
					html: data.message,
					icon: 'error',
					confirmButtonColor: '#3085d6',
					confirmButtonText: "OK",
					closeOnConfirm: false,
					allowOutsideClick: false,
				}).then((result) => {
					if (result.value) {

					}
				})

			}
		},
		error: function (data, status, error) {
			console.log("error " + error);
			console.log("messageList " + JSON.stringify(data.responseJSON.messageList));
			console.log("data " + JSON.stringify(data));
			console.log("status " + data.status);
			//showAlerts(data.responseJSON.message, data.statusText, "alertMessage", false, data.responseJSON.serviceResult, 20000);
			if (data.status == "401") {
				
				showAlerts(data.responseJSON.message, data.statusText, "alertMessage", false, data.responseJSON.serviceResult, 1000);
			}
			else if (data.status == "500") {
				showAlerts(data.responseJSON.message, data.statusText, "alertMessage", false, data.responseJSON.serviceResult, 1000);
			}else if(data.status == "424"){
				showAlerts(data.responseJSON.message, data.statusText, "alertMessage", false, data.responseJSON.serviceResult, 2000);
				Swal.fire({
					title: data.responseJSON.message,
					icon: 'error',
					showCancelButton: false,
					confirmButtonColor: '#3085d6',
					confirmButtonText: "OK",
					cancelButtonColor: '#d33',
					cancelButtonText: "Cancel",
					closeOnConfirm: false,
					closeOnCancel: false,
					allowOutsideClick: false
                    }).then((result) =>{
                   if (result.value) {
                	  
					}
				})
                
			}
		}
	});
}
/*
function addPhoneNo() {

if (i <= 3) {

var div = document.createElement('div');
div.setAttribute("id", "trustPhoneNoDiv" + i);
div.innerHTML = '<label class="control-label">Phone No - ' +  (i + 1)  + '</label><span class="required"> <font color="red">*</font></span> <input type="text" class="form-control phoneNoMpgDTOList" id="trustPhoneNo' + i + '" name="trustPhoneNo' + i + '" maxlength="10" onkeypress="return (event.charCode > 47 && event.charCode < 58);" placeholder="Enter Trust Phone no">'
+ ' <input type="button"  class="btn btn-info"  onclick="addPhoneNo()" value="+" />'
+ ' <input type="button" class="btn btn-info" value="-" onclick="removePhoneNo(this,trustPhoneNo' + i + ')"><div class="error" id="phoneNoMpgDTOList[' + i + '].phoneNo" style="color: red;"></div> <input type="hidden" id="hiddenTrustPhoneId' + i + '"  value="0">';
document.getElementById('trustPhoneNoDiv').appendChild(div);
i++;
}
}
function removePhoneNo(div, phoneNo) {
phoneNoList = phoneNoList.filter((x) => x.phoneNo != phoneNo.value);
console.log("After Remove" + JSON.stringify(phoneNoList));
document.getElementById('trustPhoneNoDiv').removeChild(div.parentNode);
i--;
}

function addEmailId() {
if (j <= 3) {

var div = document.createElement('div');
div.setAttribute("id", "trustEmailIdDiv" + j);
div.innerHTML = '<label class="control-label">Emaill Address - ' +  (j + 1)  + '</label><span class="required"> <font color="red">*</font></span> <input type="text" class="form-control emailIdMpgDTOList" id="trustEmail' + j + '" name="trustEmail' + j + '" maxlength="255"  placeholder="Enter Trust Email Id" >'
+ ' <input type="button"  class="btn btn-info"  onclick="addEmailId()" value="+" />'
+ ' <input type="button" class="btn btn-info" value="-" onclick="removeEmailId(this,trustEmail' + j + ')"> <div class="error" id="emailIdMpgDTOList[' + j + '].email" style="color: red;"></div> <input type="hidden" id="hiddenTrustEmailId' + j + '" value="0">';
document.getElementById('trustEmailIdDiv').appendChild(div);
j++;
}
}
function removeEmailId(div, emailId) {
emailIdList = emailIdList.filter((x) => x.email != emailId.value);
console.log("After Remove" + JSON.stringify(emailIdList));
document.getElementById('trustEmailIdDiv').removeChild(div.parentNode);
j--;
}
*/
function addPhoneNo() {
	var no = 0;
	if (i <= 3) {
		var div = document.createElement('div');
		if ($("#trustPhoneNoDiv").find("#trustPhoneNoDiv1").length == 0) {
			div.setAttribute("id", "trustPhoneNoDiv" + 1);
			no = 1;
		} else if ($("#trustPhoneNoDiv").find("#trustPhoneNoDiv2").length == 0) {
			div.setAttribute("id", "trustPhoneNoDiv" + 2);
			no = 2;
		} else if ($("#trustPhoneNoDiv").find("#trustPhoneNoDiv3").length == 0) {
			div.setAttribute("id", "trustPhoneNoDiv" + 3);
			no = 3;
		}
		div.setAttribute("class", "col-md");
		div.innerHTML = '<label class="control-label ">Phone No </label> <span class="required"> <font color="red">*</font> </span>' +
			'<div class="input-group mb-3"> <input type="text" class="form-control emailIdMpgDTOList" id="trustPhoneNo' + no + '" name="trustPhoneNo' + no + '" maxlength="10"' +
			'onkeypress="return (event.charCode > 47 && event.charCode < 58);" placeholder="Enter Trust Phone no">' +
			'<div class="input-group-append add"> <button class="btn btn-info" id="" onclick="addPhoneNo()"><i class="fa fa-plus"></i></button>' +
			'<button class="btn btn-danger minus" id="" onclick="removePhoneNo(this,trustPhoneNo' + no + ')"><i class="fa fa-minus"></i></button> </div> </div>' +
			'<div class="error" id="phoneNoMpgDTOList[' + no + '].phoneNo" style="color: red;"></div>' +
			'<input type="hidden" id="hiddenTrustPhoneId' + no + '" value="0">'
			;
		document.getElementById('nestedPhone').appendChild(div);
		i++;

		if (i == 4) {

			var list = document.getElementById("nestedPhone").lastChild;
			$("#" + list.id).find(".btn-info").remove();


		}

	}
}
function removePhoneNo(div, phoneNo) {

	console.log("phoneNoList" + JSON.stringify(phoneNoList));
	phoneNoList = phoneNoList.filter((x) => x.phoneNo == phoneNo.value);
	console.log("After Remove" + JSON.stringify(phoneNoList));
	div.parentNode.parentNode.parentNode.remove();
	i--;
	if (i != 4) {

		var list1 = document.getElementById("nestedPhone").lastChild;
		var isAvailable = $("#" + list1.id).find(".btn-info");
		if (isAvailable.length == 0) {
			$("#" + list1.id).find(".input-group-append").prepend('<button class="btn btn-info" onclick="addPhoneNo()"><i class="fa fa-plus"></i></button>');
		}


	}

}

function addEmailId() {
	var no1 = 0;
	if (j <= 3) {
		var div = document.createElement('div');
		if ($("#trustEmailIdDiv").find("#trustEmailIdDiv1").length == 0) {
			div.setAttribute("id", "trustEmailIdDiv" + 1);
			no1 = 1;
		} else if ($("#trustEmailIdDiv").find("#trustEmailIdDiv2").length == 0) {
			div.setAttribute("id", "trustEmailIdDiv" + 2);
			no1 = 2;
		} else if ($("#trustEmailIdDiv").find("#trustEmailIdDiv3").length == 0) {
			div.setAttribute("id", "trustEmailIdDiv" + 3);
			no1 = 3;
		}
		div.setAttribute("class", "col-md");
		div.innerHTML = '<label class="control-label ">Email Address </label> <span class="required"> <font color="red">*</font> </span>' +
			'<div class="input-group mb-3"> <input type="text" class="form-control emailIdMpgDTOList" id="trustEmail' + no1 + '" name="trustEmail' + no1 + '" maxlength="255"' +
			' placeholder="Enter Trust Email Id">' +
			'<div class="input-group-append add"> <button class="btn btn-info" id="" onclick="addEmailId()"><i class="fa fa-plus"></i></button>' +
			'<button class="btn btn-danger minus" id="" onclick="removeEmailId(this,trustEmail' + no1 + ')"><i class="fa fa-minus"></i></button> </div> </div>' +
			'<div class="error" id="emailIdMpgDTOList[' + no1 + '].email" style="color: red;"></div>' +
			'<input type="hidden" id="hiddenTrustEmailId' + no1 + '" value="0">'
			;
		document.getElementById('nestedEmail').appendChild(div);
		j++;
		if (j == 4) {

			var list = document.getElementById("nestedEmail").lastChild;
			$("#" + list.id).find(".btn-info").remove();


		}
	}
}
function removeEmailId(div, emailId) {
	emailIdList = emailIdList.filter((x) => x.email != emailId.value);
	console.log("After Remove" + JSON.stringify(emailIdList));
	div.parentNode.parentNode.parentNode.remove();
	j--;
	if (j != 4) {

		var list1 = document.getElementById("nestedEmail").lastChild;
		var isAvailable = $("#" + list1.id).find(".btn-info");
		if (isAvailable.length == 0) {
			$("#" + list1.id).find(".input-group-append").prepend('<button class="btn btn-info" onclick="addEmailId()"><i class="fa fa-plus"></i></button>');
		}


	}
}

function fillTrustDataTable(data) {

	$('#trust_data_table')
		.DataTable(
			{

				"columnDefs": [{
					"className": "dt-center",
					"targets": "_all"
				}],
				"autoWidth": true,
				"processing": true,
				"data": data,
				"iDisplayLength": 50,
				"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				destroy: true,
				"order": [[0, "asc"]],

				"fnCreatedRow": function (row, data, index) {
					$('td', row).eq(0).html(index + 1);
				},
				bAutoWidth: false,
				"columns": [
					{
						"data": null,

					},
					{
						mRender: function (data, type, row) {
							return row.trustMasterDTO.name;
						}

						
					},
					{
						mRender: function (data, type, row) {
							return row.trustMasterDTO.address;
						}

						
					},

					{
						mRender: function (data, type, row) {
							var FetchphoneNoList = [];
							var phone = row.trustMasterDTO.emailPhoneNoMpgDTOList;
							for (var i = 0; i < phone.length; i++) {
								if (phone[i].type == 2) {
									FetchphoneNoList.push(phone[i].emailPhone);
								}
							}
							return FetchphoneNoList;
						}
					},
					{
						mRender: function (data, type, row) {
							var FetchEmailList = [];
							var email = row.trustMasterDTO.emailPhoneNoMpgDTOList;
							for (var i = 0; i < email.length; i++) {
								if (email[i].type == 1) {
									FetchEmailList.push(email[i].emailPhone);
								}
							}
							return FetchEmailList;
						}
					},
					{
						mRender: function (data, type, row) {
							return row.trustMasterDTO.websiteUrl;
						}

						
					}, {
						mRender: function (data, type, row) {
							var trust_mpg_id = row.id;
							var isMappedd = row.isMapped;

							if (isMappedd == 0) {
								return "<a "
									+ " target='_blank'><i class='fa fa-pencil fatbl text-info' onclick='fillTrustFormDataForEdit(" + trust_mpg_id + ")'></i> <i class='fa fa-trash fatbl text-info' onclick='deleteTrustData(" + trust_mpg_id + ")'></i>"
									+ "</a>"
							} else {
								return "<a "
									+ " target='_blank'><i class='fa fa-trash fatbl text-info' onclick='deleteTrustData(" + trust_mpg_id + ")'></i>"
									+ "</a>"
							}



						}
					}


				]
			});

}


$("#trustNameDD").change(function () {

	var trustid = $("#trustNameDD").val();
	$("#newTrustDiv").show();
	fillTrustFormData(trustid);
	$("#hiddenInstTrustMpgId").val(0);
	$(".TrustMasterDTO").prop("disabled", true);
	$(".emailIdMpgDTOList").prop("disabled", true);

});

function fillTrustFormData(trustid) {
		$("#emailDiv").remove();
		$("#phoneDiv").remove();

	console.log("trustid"+trustid);
	var trustDataById = [];
	var emaill = [];
	var phn = [];
	var email_type = 1;
	var phn_type = 2;

	trustDataById = trustMasterdetail.filter((x) => x.id == trustid);
	console.log("trustDataById"+JSON.stringify(trustDataById));
	if (trustDataById != null) {
		console.log(trustDataById);
		//console.log("trustDataById.id " + trustDataById[0].id);
		$("#trustName").focus();

		$("#hiddenId").val(trustid);
		$("#hiddenTrustMasterId").val(trustid);

		$("#hiddenInstTrustMpgId").val(trustDataById[0].id);

		$("#trustName").val(trustDataById[0].name);
		$("#trustWebUrl").val(trustDataById[0].websiteUrl);
		$("#trustAddress").val(trustDataById[0].address);

		emailPhoneNoMpgDTO = trustDataById[0].emailPhoneNoMpgDTOList;

		phn = emailPhoneNoMpgDTO.filter((x) => x.type == phn_type);
		emaill = emailPhoneNoMpgDTO.filter((x) => x.type == email_type);

		for (var a = 0; a < emaill.length; a++) {
			if (a > 0) {
				addEmailId();
			}
			$("#trustEmail" + a).val(emaill[a].emailPhone);
			$("#hiddenTrustEmailId" + a).val(emaill[a].id);

		}

		for (var a = 0; a < phn.length; a++) {
			if (a > 0) {
				addPhoneNo();
			}
			$("#trustPhoneNo" + a).val(phn[a].emailPhone);
			$("#hiddenTrustPhoneId" + a).val(phn[a].id);
		}
		$("#trustNameDD").val(trustDataById[0].id);
		$("#existingTrustDiv").show();
		$("#isMappedDiv").show();
		$(".add").prop("disabled",true);
		$(".minus").prop("disabled",true);
		/*  isMapped = trustDataById[0].instituteTrustMpgDTOList.isMapped; */


	}

}

function fillTrustFormDataForEdit(trustMpgid) {
	console.log("trustMpgid"+trustMpgid);
	$("#emailDiv").remove();
	$("#phoneDiv").remove();
	var trustDataById = [];
	var emaill = [];
	var phn = [];
	var email_type = 1;
	var phn_type = 2;

	trustDataById = instTrustDetail.filter((x) => x.id == trustMpgid);
	console.log("trustDataById"+JSON.stringify(trustDataById));
	if (trustDataById != null) {
		console.log(trustDataById);
		//console.log("trustDataById.id " + trustDataById[0].id);
		$("#trustName").focus();
		/* $("#emailDiv").empty();
		$("#phoneDiv").empty(); */
		$(".TrustMasterDTO").prop("disabled", false);
		$(".emailIdMpgDTOList").prop("disabled", false);
		$(".add").prop("disabled",false);
		$(".minus").prop("disabled",false);


		//$("#hiddenId").val(trustMpgid);
		$("#hiddenTrustMasterId").val(trustDataById[0].trustMasterDTO.id);

		$("#hiddenInstTrustMpgId").val(trustMpgid);

		$("#trustName").val(trustDataById[0].trustMasterDTO.name);
		$("#trustWebUrl").val(trustDataById[0].trustMasterDTO.websiteUrl);
		$("#trustAddress").val(trustDataById[0].trustMasterDTO.address);

		emailPhoneNoMpgDTO = trustDataById[0].trustMasterDTO.emailPhoneNoMpgDTOList;

		phn = emailPhoneNoMpgDTO.filter((x) => x.type == phn_type);
		emaill = emailPhoneNoMpgDTO.filter((x) => x.type == email_type);
		$("#nestedEmail").empty();
		j=1;
		console.log("phn " + phn.length);
		for (var a = 0; a < emaill.length; a++) {
			if (a > 0) {
				addEmailId();
			}
			$("#trustEmail" + a).val(emaill[a].emailPhone);
			$("#hiddenTrustEmailId" + a).val(emaill[a].id);

		}
		$("#nestedPhone").empty();
		i=1;
		for (var a = 0; a < phn.length; a++) {
			if (a > 0) {
				addPhoneNo();
			}
			$("#trustPhoneNo" + a).val(phn[a].emailPhone);
			$("#hiddenTrustPhoneId" + a).val(phn[a].id);
		}
		$("#trustNameDD").val(trustDataById[0].trustMasterDTO.id);
		$("#trustListDiv").hide();
		$("#isMappedDiv").hide();

		/*  isMapped = trustDataById[0].instituteTrustMpgDTOList.isMapped; */


	}

}

function deleteTrustData(trustMpgId) {

	Swal.fire({
		title: 'Are you sure?',
		html: "You want to Delete Trust Details",
		icon: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		confirmButtonText: "YES",
		cancelButtonColor: '#d33',
		cancelButtonText: "Cancel",
		closeOnConfirm: false,
		closeOnCancel: false,
		allowOutsideClick: false
	}).then((result) => {
		if (result.value) {
			if (trustMpgId != 0) {
				$.ajax({
					type: "DELETE",
					url: "/trust/",
					headers: {
						'API-VERSION': 1
					},
					data: { trustMpgId: trustMpgId },
					success: function (data) {
						if (data.success == 1) {
							showAlerts(data.message, data.messageList, "alertMessage", false, data.serviceResult, 2000);
							cleanValidationDto('TrustMasterDTO');
							cleanValidationDto('emailIdMpgDTOList');
							cleanValidationDto('phoneNoMpgDTOList');
							cleanError('error');
							$("#trustNameDD").val("");
							$(".TrustMasterDTO").prop("disabled", false);
	                        $(".emailIdMpgDTOList").prop("disabled", false);
							fetchTrustMasterdetail();
						} else {
							Swal.fire({
								title: 'ERROR',
								html: data.message,
								icon: 'error',
								confirmButtonColor: '#3085d6',
								confirmButtonText: "OK",
								closeOnConfirm: false,
								allowOutsideClick: false,
							}).then((result) => {
								if (result.value) {

								}
							})

						}
					},
					error: function (error) {
						Swal.fire({
							title: 'ERROR',
							html: "Trust Member is Already Exit,So You can not Delete particular Trust",
							icon: 'error',
							confirmButtonColor: '#3085d6',
							confirmButtonText: "OK",
							closeOnConfirm: false,
							allowOutsideClick: false,
						}).then((result) => {
							if (result.value) {

							}
						})

					}
				});

			}
		}

	})
}
var isMapped = 0;
function showTrustDiv() {
	$("#emailDiv").remove();
		$("#phoneDiv").remove();


	if (document.getElementById("existingTrust").checked) {
		$("#existingTrustDiv").show();
		$("#newTrustDiv").hide();
		$("#trustNameDD").val("");
		isMapped = 1;
	}

	if (document.getElementById("newTrust").checked) {
		$("#existingTrustDiv").hide();
		$("#newTrustDiv").show();
		$(".TrustMasterDTO").prop("disabled", false);
		$(".emailIdMpgDTOList").prop("disabled", false);
		$(".TrustMasterDTO").val("");
		$(".emailIdMpgDTOList").val("");
		$(".add").prop("disabled",false);
		$(".minus").prop("disabled",false);
		isMapped = 0;
	}

}
/* $('#IPRApplicationNo').bind("cut copy paste",function(e) {
    e.preventDefault();
}); */