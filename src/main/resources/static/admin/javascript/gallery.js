$(document).ready(function() {
	$("#fileInput").attr("accept", "image/png,image/jpg,image/jpeg");
	eventdataTable();
	categoryListInDropdown();
	galleryDataTable();
});

function eventdataTable() {
	if ($.fn.dataTable.isDataTable('#gallerytbl')) {
		$('#gallerytbl').DataTable().destroy();
		$('#gallerytbl').empty();
	}

	$.ajax({
		type: "get",
		url: "/admin/categoryByInst",
		dataType: "json",
		success: function(data, textStatus, jqXHR) {
			$('#gallerytbl').DataTable({
				"columnDefs": [{
					"targets": 2,
					'orderable': false,
					render: function(data,
						type, row, meta) {
						var action = `<span class='text-center'><i id="deleteDetail" class="fa fa-trash-o fatbl text-danger btn btn-link" data-toggle="tooltip" title="Delete Detail" style="font-size:20px;" onclick="commonDelete('Id',` + row.id + `,'admin/category',eventdataTable)"></i></span>`;
						return action;
					}
				}],
				"searching": true,
				"lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
				"retrieve": true,
				"data": data,
				"createdRow": function(row, data, dataIndex) {
					this.api().cell($('td:eq(0)', row)).data(dataIndex + 1);
				},
				"columns": [{
					"title": "Sr.No",
					"data": "id",
					"sDefaultContent": "-"

				}, {
					"title": "Event Title",
					"data": "name",
					"sDefaultContent": "-"

				}, {
					"title": "Action",
					"data": "id",
					"sDefaultContent": "-"
				}]

			});
		},
		error: function(jqXHR, textStatus, errorThrown) {
			console.log("Something really bad happened "
				+ textStatus);
			$("#ajaxResponse").html(jqXHR.responseText);
		}
	});

}

function galleryDataTable() {
	if ($.fn.dataTable.isDataTable('#gallerytbl2')) {
		$('#gallerytbl2').DataTable().destroy();
		$('#gallerytbl2').empty();
	}

	$.ajax({
		type: "get",
		url: "/admin/galleryDetailByInst",
		dataType: "json",
		success: function(data, textStatus, jqXHR) {
			$('#gallerytbl2').DataTable({
				

				"columnDefs": [{					
							"targets" : 4,
							'orderable': false,
							render : function(data, type, row, meta) {
								var downloadDoc = `<span class='text-center'><i id="downloadFile" class="fa fa-download fatbl text-primary" data-toggle="tooltip" title="Download File" style="font-size:20px;" onclick="downloadDoc('`+row.docPath+`','Event_File')"></i></span>`;
									return downloadDoc;
								}
									
						},{
					"targets": 5,

					'orderable': false,

					render: function(data,

						type, row, meta) {
							
						var action = `<span class='text-center'><i id="deleteDetail" class="fa fa-trash-o fatbl text-danger btn btn-link" data-toggle="tooltip" title="Delete Detail" style="font-size:20px;" onclick="commonDelete('Id',` + row.id + `,'admin/gallery',galleryDataTable)"></i></span>`;
						return action;

					}

				}],

				"searching": true,

				"lengthMenu": [

					[10, 25, 50, 100, -1],

					[10, 25, 50, 100, "All"]],

				"retrieve": true,

				"data": data,

				"createdRow": function(row, data,

					dataIndex) {

					this.api().cell($('td:eq(0)', row))

						.data(dataIndex + 1);

				},

				"columns": [{

					"title": "Sr.No",

					"data": "id",

					"sDefaultContent": "-"

				}, {

					"title": "Event Id",

					"data": "id",

					"sDefaultContent": "-"

				}, {

					"title": "Event Title",

					"data": "name",

					"sDefaultContent": "-"

				}, {

					"title": "Event Category",

					"data": "category",

					"sDefaultContent": "-"

				}, {

					"title": "Event File",

					"data": "id",

					"sDefaultContent": "-"

				},{

					"title": "Action",

					"data": "id",

					"sDefaultContent": "-"

				}]

			});
		},
		error: function(jqXHR, textStatus, errorThrown) {
			console.log("Something really bad happened "
				+ textStatus);
			$("#ajaxResponse").html(jqXHR.responseText);
		}
	});



}

function categoryListInDropdown() {
	$("#category").empty();
	$.getJSON("/admin/categoryByInst", {
		ajax: 'true',
		cache: 'false'
	}, function(data) {
		$("#category").append(
			'<option value=""  selected="">'
			+ "Select" + '</option>');
		$.each(data, function(index, value) {

			// APPEND OR INSERT DATA TO SELECT ELEMENT.
			$('#category').append(
				'<option value="' + value.id + '">'
				+ value.name + '</option>').trigger(
					'chosen:updated');
		});
	});

}
