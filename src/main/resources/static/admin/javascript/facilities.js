$(document).ready(function(){
	deptByInst();
	detail();
});

function detail(){
	if ($.fn.dataTable.isDataTable('#detailTbl')) {
		$('#detailTbl').DataTable().destroy();
		$('#detailTbl').empty();
	}	
	$.ajax({type:"get", 
		url:"/admin/facilitiesByInst", 
			dataType:"json",
				success : function(data, textStatus, jqXHR) {
					$('#detailTbl').DataTable({
						"columnDefs" : [{
							"targets" : 6,
							'orderable': false,
							render : function(data, type, row, meta) {
								var downloadDoc = `<span class='text-center'><i id="downloadFile" class="fa fa-download fatbl text-primary" data-toggle="tooltip" title="Download File" style="font-size:20px;" onclick="downloadDoc('`+row.doc_path+`','Facilities')"></i></span>`;
									return downloadDoc;
								}
									
						},{
							"targets" : 7,
							'orderable': false,
							render : function(data, type, row, meta) {
								let testdata = JSON.stringify(row).replace(/"/g, '\"');
								var action = '<i class="fa fa-edit fatbl text-info" data-toggle="tooltip" title="Edit Department" style="font-size:20px;" onclick=\'programEdit("' + testdata + '")\'>' +
		                            '</i><i class="fa fa-trash fatbl text-danger" data-toggle="tooltip" title="Edit Department" style="font-size:20px;" onclick=\'commonDelete("' + "Id" + '","' + row.id + '","' + "admin/facilitie" + '",detail)\'></i>';
		                        return action;
							}
									
						}],
						"lengthMenu": [[10, 25, 50,100, -1], [10, 25, 50,100,"All"]],
						"searching" : true,
						"retrieve":true,
	                    "data": data,
	                    "createdRow": function(row, data, dataIndex){
	                    	this.api().cell($('td:eq(0)', row)).data(dataIndex+1);
	                    },
	                    "columns" : [ {
	                    	"title":"Sr.No",
							"data" : "name",
							"sDefaultContent" : "-"
						}, {
							"title":"Facility Name",
							"data" : "name",
							"sDefaultContent" : "-"
						}, {
							"title":"Department Name",
							"data" : "department",
							"sDefaultContent" : "-"
						}, {
							"title":"Description",
							"data" : "description",
							"sDefaultContent" : "-"
						},{
							"title":"Sequence",
							"data" : "sequence",
							"sDefaultContent" : "-"
						},{
							"title":"Lab / Workshop",
							"data" : "lab_workshop",
							"sDefaultContent" : "-"
						},{
							"title":"Download",
							"data" : null,
							"sDefaultContent" : "-"
						},{
							"title":"Action",
							"data" : null,
							"sDefaultContent" : "-"
						}]
	                });
				},
				error : function(jqXHR, textStatus, errorThrown) {
					console.log("Something really bad happened "
							+ textStatus);
					$("#ajaxResponse").html(jqXHR.responseText);
				}
			});
	}