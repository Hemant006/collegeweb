function showAlerts (message, type,divId,isClosing,heading, closeDelay) {
    divId = divId || "alerts-container";
    var $cont = $("#"+divId);
    type = type || "info";
    heading = heading || null;

    if (isClosing === undefined) {
        isClosing = true;
    }
    console.log(isClosing)
    // create the alert div
    var alert = $('<div>').addClass("fade in show alert alert-" + type);

    if(isClosing === true){
        alert.append(
            $('<button type="button" class="close" data-dismiss="alert">').append("&times;")
        );
    }
    if(heading !== null){
        alert.append('<h4 class="alert-heading">'+heading+'</h4><hr/>')

    }
    alert.append(message);

    // add the alert div to top of alerts-container, use append() to add to bottom
    $cont.prepend(alert);

    // if closeDelay was passed - set a timeout to close the alert
    if (closeDelay!=null)
      //  window.setTimeout(function() { alert.alert("close") }, closeDelay);
       window.setTimeout(function() { $("#"+divId).empty(); }, closeDelay);
       
    $('html,body').animate({scrollTop:$("#alertMessage").offset().top}, 'slow');

}


const cleanValidationDto = (dtoClassName) => {
    $(`.${dtoClassName}`).each(function () {
        document.getElementById(this.id).value = "";
        /* document.getElementById(this.id + "Error").innerHTML = ""; */
    });

}


function makeAsDataTable(jsTagId) {
    alert("called" + jsTagId);
    jsTagId.DataTable({

        dom: 'lBfrtip',
        buttons: [
            'excel', 'print'
        ],
        lengthMenu: [
            [10, 25, 50, -1],
            ['10 row', '25 row', '50 row', 'Show all']
        ],
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "processing": true
    });


}

function blockSpecialChar(e) {
    var k = e.keyCode;
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8   || (k >= 48 && k <= 57) || (k==32));
}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

const cleanError = (className) => {
    $(`.${className}`).each(function () {
       
        $("."+className).text("");
    });

}