$(document).ready(function(){
	detail();
});

function detail(){
	if ($.fn.dataTable.isDataTable('#detailTbl')) {
		$('#detailTbl').DataTable().destroy();
		$('#detailTbl').empty();
	}	
	$.ajax({type:"get", 
		url:"/admin/docByInst", 
			dataType:"json",
				success : function(data, textStatus, jqXHR) {
					$('#detailTbl').DataTable({
						columnDefs: [
							{ orderable: false, targets: 2 },
							{ orderable: false, targets: 3 }
						],
						"lengthMenu": [[10, 25, 50,100, -1], [10, 25, 50,100,"All"]],
						"searching" : true,
						"retrieve":true,
	                    "data": data,
	                    "createdRow": function(row, data, dataIndex){
							this.api().cell($('td:eq(0)', row)).data(dataIndex+1);
							var downlaodDoc= `<span class='text-center'><i id="downloadFile" class="fa fa-download fatbl text-primary" data-toggle="tooltip" title="Download File" style="font-size:20px;" onclick="downloadDoc('` + data.docPath + `', 'InformationImage')"></i></span><br>`;
							var action= `<span class='text-center'><i id="deleteDetail" class="fa fa-trash-o fatbl text-danger" data-toggle="tooltip" title="Delete Detail" style="font-size:20px;" onclick="commonDelete('Id',` + data.id + `,'admin/uploadDoc', detail)"></i></span><br>`;
							$('td', row).eq(2).html(downlaodDoc);
							$('td', row).eq(3).html(action);
	                    },
	                    "columns" : [ {
	                    	"title":"Sr.No",
							"data" : "title",
							"sDefaultContent" : "-"
						}, {
							"title":"Title",
							"data" : "title",
							"sDefaultContent" : "-"
						}, {
							"title":"Download",
							"data" : null,
							"sDefaultContent" : "-"
						},{
							"title":"Action",
							"data" : null,
							"sDefaultContent" : "-"
						}]
	                });
				},
				error : function(jqXHR, textStatus, errorThrown) {
					console.log("Something really bad happened "
							+ textStatus);
					$("#ajaxResponse").html(jqXHR.responseText);
				}
			});
	}