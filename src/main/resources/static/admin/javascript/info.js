$(document).ready(function () {
	rti();
	dropDownChnage();
	detail();
	pageList();
});

function pageList() {
	$("#pageId").empty();
	$.getJSON("/common/page", {
		ajax: 'true',
		cache: 'false'
	}, function (data) {
		$("#pageId").append('<option value=""  selected="" disabled="">' + "Select" + '</option>');
		$.each(data, function (index, value) {
			$('#pageId').append('<option value="' + value.id + '">' + value.name + '</option>').trigger('chosen:updated');
		});
	});
}

function getInfoDetail(pageId) {
	$.ajax({
		type: "get",
		url: "/admin/infoDetai?pageId=" + pageId,
		success: function (data) {
			if (data.length > 0) {
				$("#pageInstMpgId").val(data[0].id);
				$("#pageInstMpngId").val(data[0].id);
				document.getElementById("btnsubmit").innerHTML = "UPDATE";
				CKEDITOR.instances['description'].setData(data[0].description)
			} else {
				$("#pageInstMpgId").val(0);
				$("#pageInstMpngId").val(0);
				document.getElementById("btnsubmit").innerHTML = "Save";
			}
		},
		error: function (jqXHR, textStatus, errorThrown) {
			console.log("Something really bad happened " + textStatus);
			$("#ajaxResponse").html(jqXHR.responseText);
		}
	});
}

function detailFormValidate() {
	var pageId = $("#pageId").val();
	var pageInstMpgId = $("#pageInstMpgId").val();
	if (pageId == null || pageId == "") {
		$('html,body').animate({ scrollTop: $("#mainSection").offset().top }, 'slow');
		document.getElementById("programAlert").innerHTML = "Please select program name.";
		return false;
	} else {
		document.getElementById("programAlert").innerHTML = "";
		if (pageInstMpgId == null || pageInstMpgId == "") {
			alert("Kindly first upload program details.");
			return false;
		} else {
			return true;
		}
	}
}


function detail() {
	if ($.fn.dataTable.isDataTable('#detailTbl')) {
		$('#detailTbl').DataTable().destroy();
		$('#detailTbl').empty();
	}
	$.ajax({
		type: "get",
		url: "/admin/infoDetaiList",
		dataType: "json",
		success: function (data, textStatus, jqXHR) {
			console.dir(data);
			$('#detailTbl').DataTable({
				columnDefs: [
					{ orderable: false, targets: 3 },
					{ orderable: false, targets: 4 }
				],
				"lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
				"searching": true,
				"retrieve": true,
				"data": data,
				"createdRow": function (row, data, dataIndex) {
					if (data.cpdocumentMpgDTOList!=null ) {
						this.api().cell($('td:eq(0)', row)).data(dataIndex + 1);
						var docTitle = "";
						var downlaodDoc = "";
						var action = "";
						for (var i = 0; i < data.cpdocumentMpgDTOList.length; i++) {
							docTitle +=  data.cpdocumentMpgDTOList[i].docTitle + "<br>";
							downlaodDoc += `<span class='text-center'><i id="downloadFile" class="fa fa-download fatbl text-primary" data-toggle="tooltip" title="Download File" style="font-size:20px;" onclick="downloadDoc('` + data.cpdocumentMpgDTOList[i].docPath + `', 'InformationImage')"></i></span><br>`;
							action += `<span class='text-center'><i id="deleteDetail" class="fa fa-trash-o fatbl text-danger" data-toggle="tooltip" title="Delete Detail" style="font-size:20px;" onclick="commonDelete('Id',` + data.cpdocumentMpgDTOList[i].id + `,'admin/info', detail)"></i></span><br>`;
						}
						$('td', row).eq(0).html(data.id);
						$('td', row).eq(1).html(data.pageName);
						$('td', row).eq(2).html(docTitle);
						$('td', row).eq(3).html(downlaodDoc);
						$('td', row).eq(4).html(action);
					}else{
						$(row).empty();
					}
				},
				
				"columns": [{
					"title": "Sr.No",
					"data": null,
					"sDefaultContent": "-"
				}, {
					"title": "Program Name",
					"data": null,
					"sDefaultContent": "-"
				}, {
					"title": "Title",
					"data": null,
					"sDefaultContent": "-"
				}, {
					"title": "Download",
					"data": null,
					"sDefaultContent": "-"
				}, {
					"title": "Action",
					"data": null,
					"sDefaultContent": "-"
				}]
			});
		},
		error: function (jqXHR, textStatus, errorThrown) {
			console.log("Something really bad happened " + textStatus);
			$("#ajaxResponse").html(jqXHR.responseText);
		}
	});
}

function dropDownChnage() {
	$("#pageId").change(function () {
		if (this.value == 8) {
			$("#s1").hide();
		} else {
			$("#s1").show();
		}
		var sValue = this.value;

		if(sValue==1){
			rti();
		}else if(sValue==2){
			tpo();
		}else if(sValue==3){
			womenCell();
		}else if(sValue==4){
			grievanceCell();
		}else if(sValue==5){
			ssip();
		}else if(sValue==6){
			antiRaggingInformation();
		}else if(sValue==7){
			newsLatter();
		}
		clear();
		getInfoDetail(this.value);
	});
}
function clear() {
	$("#c1Field1").val("");
	$("#c2Field1").val("");
}

function rti() {
	$("#c1").text("RTI Information");
	$("#c1Field1").text("RTI Information");
	$("#c2").text("Upload RTI");
	$("#c2Field1").text("RTI Title");
	$("#title").attr("placeholder", "RTI Title");
	$("#c2Button").text("Upload RTI");
}

function tpo() {
	$("#c1").text("TPO Information");
	$("#c1Field1").text("TPO Information");
	$("#c2").text("Upload TPO");
	$("#title").attr("placeholder", "TPO Title");
	$("#c2Field1").text("Upload TPO");
	$("#c2Button").text("Upload TPO");
}

function womenCell() {
	$("#c1").text("Women Cell Information");
	$("#c1Field1").text("Women Cell Information");
	$("#c2").text("Upload Women Cell Details");
	$("#c2Field1").text("Title");
	$("#title").attr("placeholder", "Women Cell Title");
	$("#c2Button").text("Upload Women Cell");

}

function grievanceCell() {
	$("#c1").text("Grievance cell Information");
	$("#c1Field1").text("Grievance cell Information");
	$("#c2").text("Upload Grievance cell Details");
	$("#c2Field1").text("Title");
	$("#title").attr("placeholder", "Grievancecell Title");
	$("#c2Button").text("Upload Upload Grievance cell Details");
}

function ssip() {
	$("#c1").text("Student Startup & Innovation Information");
	$("#c1Field1").text("Student Startup & Innovation Information");
	$("#c2").text("Upload Student Startup & Innovation Details");
	$("#c2Field1").text("Title");
	$("#title").attr("placeholder", "Ssip Title");
	$("#c2Button").text("Upload Student Startup & Innovation Details");
}

function antiRaggingInformation() {
	$("#c1").text("Anti Ragging Information");
	$("#c1Field1").text("Anti Ragging Information");
	$("#c2").text("Upload Anti Ragging Details");
	$("#c2Field1").text("Title");
	$("#title").attr("placeholder", "AntiRaggingTitle");
	$("#c2Button").text("Upload Anti Ragging Details");
}

function newsLatter() {
	$("#c1").text("Newsletter Information");
	$("#c1Field1").text("Newsletter Information");
	$("#c2").text("Upload Newsletter Details");
	$("#c2Field1").text("Title");
	$("#title").attr("placeholder", "Newsletter Title");
	$("#c2Button").text("Upload Newsletter Details");
}
