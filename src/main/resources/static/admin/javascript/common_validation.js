function validateImage(id){
	var ext = $('#'+id).val().split('.').pop().toLowerCase();
	if(ext!=null){
		if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
		    alert('invalid extension!');
		    document.getElementById(id).value="";
		}
	}
};

function validatePdfAndImage(id,evt){
    var f = evt.target.files[0];
	var filesize = f.size / 1024;
    var ext = $('#'+id).val().split('.').pop().toLowerCase();
	if(ext!=null){
		if($.inArray(ext, ['png','jpg','jpeg','pdf']) == -1) {
            document.getElementById(id).value="";
		    alert('invalid extension!');
		}else{
            if (filesize > 2048) {
                document.getElementById(id).value="";
                alert('file more than 2mb!');
            }
        }
	}
};

function validateNumber(evt,id){
   	document.getElementById(id+"Alert").innerHTML = "";
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   		document.getElementById(id+"Alert").innerHTML = "enter only numeric value";
        return false;
    }
    return true;
}

function disableFutureDate(id){
	var dtToday = new Date();
    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();
    if (month < 10)
        month = '0' + month.toString();
    if (day < 10)
        day = '0' + day.toString();
    var maxDate = year + '-' + month + '-' + day;
    $('#'+id).attr('max', maxDate);
}