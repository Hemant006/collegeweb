$(document).ready(function(){
	detail();
});
disableFutureDate('eventDate');
function detail(){
	if ($.fn.dataTable.isDataTable('#detailTbl')) {
		$('#detailTbl').DataTable().destroy();
		$('#detailTbl').empty();
	}	
	$.ajax({type:"get", 
		url:"/admin/academicCalenderByInst", 
			dataType:"json",
				success : function(data, textStatus, jqXHR) {
					console.dir(data);
					$('#detailTbl').DataTable({
						"columnDefs" : [{
							"targets" : 5,
							'orderable': false,
							render : function(data, type, row, meta) {
								var action = `<span class='text-center'><i id="deleteDetail" class="fa fa-trash-o fatbl text-danger btn btn-link" data-toggle="tooltip" title="Delete Detail" style="font-size:20px;" onclick="commonDelete('Id',`+row.id+`,'admin/academicCalender', detail)"></i></span>`;
									return action;
								}
									
						}],
						"lengthMenu": [[10, 25, 50,100, -1], [10, 25, 50,100,"All"]],
						"searching" : true,
						"retrieve":true,
	                    "data": data,
	                    "createdRow": function(row, data, dataIndex){
	                    	this.api().cell($('td:eq(0)', row)).data(dataIndex+1);
	                    },
	                    "columns" : [ {
	                    	"title":"Sr.No",
							"data" : "place",
							"sDefaultContent" : "-"
						}, {
							"title":"Event Name",
							"data" : "eventName",
							"sDefaultContent" : "-"
						}, {
							"title":"Description",
							"data" : "description",
							"sDefaultContent" : "-"
						}, {
							"title":"Event Date",
							"data" : "eventDate",
							"sDefaultContent" : "-"
						}, {
							"title":"Calender Active",
							"data" : "calederActive",
							"sDefaultContent" : "-"
						}, {
							"title":"Action",
							"data" : null,
							"sDefaultContent" : "-"
						}]
	                });
				},
				error : function(jqXHR, textStatus, errorThrown) {
					console.log("Something really bad happened "
							+ textStatus);
					$("#ajaxResponse").html(jqXHR.responseText);
				}
			});
	}