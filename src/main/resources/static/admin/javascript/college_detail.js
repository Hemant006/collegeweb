$(document).ready(function(){
	detail();
	collegeData();
});

function detail(){
	$.ajax({type:"get", 
		url:"/admin/clgdetailByInst", 
			dataType:"json",
				success : function(data, textStatus, jqXHR) {
					if(data.length>0){
						var value=data[0];
						$("#hiddenId").val(value.id);
						$("#course").val(value.course);
						$("#googleMapURL").val(value.googleMapURL);
						$("#principalTitle").val(value.principalTitle);
						CKEDITOR.instances['about'].setData(value.about);
						CKEDITOR.instances['principalMsg'].setData(value.principalMsg);
						document.getElementById("btnsubmit").innerHTML="UPDATE";
						$("#principalImgl").hide();
						$("#requiredClgImg").hide()
						document.getElementById("principalImglAlert").innerHTML="File upload not required.";
						document.getElementById("clgImgAlert").innerHTML="File upload not required.";
						$("#clgImg").attr("required",false);
						$("#princiaplImg").attr("required",false);
					}else{
						CKEDITOR.instances['about'].setData('Enter college about details.');
						CKEDITOR.instances['principalMsg'].setData('Enter principal message.');
						document.getElementById("btnsubmit").innerHTML="Save";
						$("#hiddenId").val(0);
						$("#principalImgl").show();
						$("#requiredClgImg").show();
						document.getElementById("principalImglAlert").innerHTML="";
						document.getElementById("clgImgAlert").innerHTML="";
						$("clgImg").attr("required",true);
						$("princiaplImg").attr("required",true);
					}
				},
				error : function(jqXHR, textStatus, errorThrown) {
					console.log("Something really bad happened "
							+ textStatus);
					$("#ajaxResponse").html(jqXHR.responseText);
				}
			});
}

function collegeData(){
	$.ajax({type:"get", 
		url:"/common/instituteData", 
			dataType:"json",
				success : function(data, textStatus, jqXHR) {
					console.dir(data);
					if(data.length>0){
						var value=data[0];
						$("#collegeName").val(value.name);
						$("#address").val(value.address);
						
						if(value.phone=="" || value.phone==null){
							$("#contactNo").val('Not available.');
						}else{
							$("#contactNo").val(value.phone);
						}
						if(value.instemail==null || value.instemail==""){
							$("#email").val(value.othemail);
						}else{
							$("#email").val(value.instemail);
						}
						$("#website").val(value.link);
					}
				},
				error : function(jqXHR, textStatus, errorThrown) {
					console.log("Something really bad happened "
							+ textStatus);
					$("#ajaxResponse").html(jqXHR.responseText);
				}
			});
}