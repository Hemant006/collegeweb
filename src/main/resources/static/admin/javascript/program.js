$(document).ready(function(){
	detail();
});

function detail(){
	if ($.fn.dataTable.isDataTable('#detailTbl')) {
		$('#detailTbl').DataTable().destroy();
		$('#detailTbl').empty();
	}	
	$.ajax({type:"get", 
		url:"/admin/programByInst", 
			dataType:"json",
				success : function(data, textStatus, jqXHR) {
					$('#detailTbl').DataTable({
						"columnDefs" : [{
							"targets" : 5,
							'orderable': false,
							render : function(data, type, row, meta) {
								let testdata = JSON.stringify(row).replace(/"/g, '\"');
								var action = '<i class="fa fa-edit fatbl text-info" data-toggle="tooltip" title="Edit Department" style="font-size:20px;" onclick=\'programEdit("' + testdata + '")\'>' +
		                            '</i><i class="fa fa-trash fatbl text-danger" data-toggle="tooltip" title="Edit Department" style="font-size:20px;" onclick=\'commonDelete("' + "Id" + '","' + row.id + '","' + "admin/program" + '",detail)\'></i>';
		                        return action;
							}
									
						}],
						"lengthMenu": [[10, 25, 50,100, -1], [10, 25, 50,100,"All"]],
						"searching" : true,
						"retrieve":true,
	                    "data": data,
	                    "createdRow": function(row, data, dataIndex){
	                    	this.api().cell($('td:eq(0)', row)).data(dataIndex+1);
	                    },
	                    "columns" : [ {
	                    	"title":"Sr.No",
							"data" : "programId",
							"sDefaultContent" : "-"
						}, {
							"title":"Name",
							"data" : "programId",
							"sDefaultContent" : "-"
						}, {
							"title":"Title",
							"data" : "programTitle",
							"sDefaultContent" : "-"
						}, {
							"title":"Detail Title",
							"data" : "detailTitle",
							"sDefaultContent" : "-"
						},{
							"title":"Program Detail",
							"data" : "programDetail",
							"sDefaultContent" : "-"
						},{
							"title":"Action",
							"data" : "programDetail",
							"sDefaultContent" : "-"
						}]
	                });
				},
				error : function(jqXHR, textStatus, errorThrown) {
					console.log("Something really bad happened "
							+ textStatus);
					$("#ajaxResponse").html(jqXHR.responseText);
				}
			});
	}
	
function programEdit(objects){
	console.log(JSON.parse(objects));
	$("#programId").val(objects.programId);
	$("#programTitle").val(objects.programTitle);
	$("#detailTitle").val(objects.detailTitle);
	$("#programDetail").val(objects.programDetail);
}