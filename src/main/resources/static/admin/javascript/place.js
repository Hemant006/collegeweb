$(document).ready(function(){
	placeDetail();
});

function placeDetail(){
	if ($.fn.dataTable.isDataTable('#placeDetailTbl')) {
		$('#placeDetailTbl').DataTable().destroy();
		$('#placeDetailTbl').empty();
	}	
	$.ajax({type:"get", 
		url:"/admin/placeByInst", 
			dataType:"json",
				success : function(data, textStatus, jqXHR) {
					$('#placeDetailTbl').DataTable({
						"columnDefs" : [{
							"targets" : 2,
							'orderable': false,
							render : function(data, type, row, meta) {
								var downloadDoc = `<span class='text-center'><i id="downloadFile" class="fa fa-download fatbl text-primary" data-toggle="tooltip" title="Download File" style="font-size:20px;" onclick="downloadDoc('`+row.docPath+`','Place')"></i></span>`;
									return downloadDoc;
								}
									
						},{
							"targets" : 3,
							'orderable': false,
							render : function(data, type, row, meta) {
								var action = `<span class='text-center'><i id="deleteDetail" class="fa fa-trash-o fatbl text-danger btn btn-link" data-toggle="tooltip" title="Delete Detail" style="font-size:20px;" onclick="commonDelete('Id',`+row.id+`,'admin/place', placeDetail)"></i></span>`;
									return action;
								}
									
						}],
						"lengthMenu": [[10, 25, 50,100, -1], [10, 25, 50,100,"All"]],
						"searching" : true,
						"retrieve":true,
	                    "data": data,
	                    "createdRow": function(row, data, dataIndex){
	                    	this.api().cell($('td:eq(0)', row)).data(dataIndex+1);
	                    },
	                    "columns" : [ {
	                    	"title":"Sr.No",
							"data" : "name",
							"sDefaultContent" : "-"
						}, {
							"title":"Place",
							"data" : "name",
							"sDefaultContent" : "-"
						}, {
							"title":"Download",
							"data" : null,
							"sDefaultContent" : "-"
						}, {
							"title":"Action",
							"data" : null,
							"sDefaultContent" : "-"
						}]
	                });
				},
				error : function(jqXHR, textStatus, errorThrown) {
					console.log("Something really bad happened "
							+ textStatus);
					$("#ajaxResponse").html(jqXHR.responseText);
				}
			});
	}