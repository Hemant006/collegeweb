function commonDelete(id,value, endpoint, resultset) {
    var mapObject = {};
 	mapObject[id] = value;
    Swal.fire({
        title: 'Are you sure?',
        html: "You want to Delete Details",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: "YES",
        cancelButtonColor: '#d33',
        cancelButtonText: "Cancel",
        closeOnConfirm: false,
        closeOnCancel: false,
        allowOutsideClick: false
    }).then((result) => {
        if (result.value) {
            if (value!= 0) {
                $.ajax({
                    type: "DELETE",
                    url: "/" + endpoint + "/",
                    data: mapObject,
                    success: function (data) {
                        if (data.success == 1) {
                            resultset();
                            showAlerts(data.message, "success", "alertMessage", false, data.serviceResult, 5000);
                        } else {
                            Swal.fire({
                                title: 'ERROR',
                                html: data.message,
                                icon: 'error',
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: "OK",
                                closeOnConfirm: false,
                                allowOutsideClick: false,
                            }).then((result) => {
                                if (result.value) {

                                }
                            })

                        }
                    },
                    error: function (_) {
                        Swal.fire({
                            title: 'ERROR',
                            html: "Something Went Wrong, Please Try Again",
                            icon: 'error',
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "OK",
                            closeOnConfirm: false,
                            allowOutsideClick: false,
                        }).then((result) => {
                            if (result.value) {

                            }
                        })

                    }
                });

            }
        }

    })
}