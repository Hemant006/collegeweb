var blockdetail = [];
var yearDetail = [];
var nextYears = [];
var yearMatchList = [];
var adminBlockDetailDTOList = [];
function FetchBlockDetail() {
	$.ajax({
		type: "GET",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		url: "/admin/",
		headers: {
			'API-VERSION': 1
		},
		success: function (data) {
			console.log(data);
			console.log(JSON.stringify(data));
			$('#year1DD').append(new Option("Select", 0));
			if (data.success == 1) {

				var yearMasterDTOList = data.serviceResult.yearMasterDTOList;
				var yearMasterDTOListAdded = data.serviceResult.adminBlockDetailDTOList;
				editYeatList = data.serviceResult.adminBlockDetailDTOList;
				adminBlockDetailDTOList = data.serviceResult.adminBlockDetailDTOList
				createdataTables(data.serviceResult.adminBlockDetailDTOList);
				if (yearMasterDTOList.length > 0) {
					for (var i = 0; i < yearMasterDTOList.length; i++) {
						$('#year1DD').append(new Option(yearMasterDTOList[i].displayYear, yearMasterDTOList[i].id));

					}

				}
				for (var i = 0; i < yearMasterDTOListAdded.length; i++) {

					yearMatchList.push(yearMasterDTOListAdded[i].yearMasterDTO.id + "");
				}

			}
			else {
				Swal.fire({
					title: 'ERROR',
					html: data.message,
					icon: 'error',
					confirmButtonColor: '#3085d6',
					confirmButtonText: "OK",
					closeOnConfirm: false,
					allowOutsideClick: false,
				}).then((result) => {
					if (result.value) {

					}
				})

			}
		},
		error: function (data, status, error) {
			console.log("error " + error);
			console.log("messageList " + JSON.stringify(data.responseJSON.messageList));
			console.log("data " + JSON.stringify(data));
			console.log("status " + status);
			showAlerts(data.responseJSON.message, data.statusText, "alertMessage", false, data.responseJSON.serviceResult, 20000);
			if (data.status == "401") {
				showAlerts(data.responseJSON.message, data.statusText, "alertMessage", false, data.responseJSON.serviceResult, 20000);
			}
			else if (data.status == "500") {
				showAlerts(data.responseJSON.message, data.statusText, "alertMessage", false, data.responseJSON.serviceResult, 20000);
			}
		}
	});
}

//$("#year1DD").change(function () {
//	
//
//});


function saveBlockDetail() {
	if ($("#year1DD").val() == 0) {
		$("#yearDDError").text("Please Select Year");
		return;
	}
	var isYearAvailable = yearMatchList.includes($("#year1DD").val() + "");
	if (isYearAvailable) {
		$("#yearDDError").text("Year is already Exists");
		return;
	}
	var yearMasterList = [];

	var data = {
		id: document.getElementById("hiddenId").value,
		yearMasterDTO: {
			id: $("#year1DD").val()
		}

	};
	console.log("before save" + JSON.stringify(data));
	var verfied = true;
	if (verfied) {
		$.ajax({
			type: "POST",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			url: "/admin/",
			headers: {
				'API-VERSION': 1
			},
			data: JSON.stringify(data),
			success: function (data) {
				console.log("DATA " + JSON.stringify(data));
				$("#year1DD").val(0);
				if (data.success == 1) {
					showAlerts(data.message, data.messageList.NoError, "alertMessage", false, data.serviceResult, 2000);
					FetchBlockDetail();
				}
				else {
					Swal.fire({
						title: 'ERROR',
						html: data.message,
						icon: 'error',
						confirmButtonColor: '#3085d6',
						confirmButtonText: "OK",
						closeOnConfirm: false,
						allowOutsideClick: false,
					}).then((result) => {
						if (result.value) {

						}
					})

				}
			},
			error: function (data, status, error) {
				console.log("error " + error);
				console.log("messageList " + JSON.stringify(data.responseJSON.messageList));
				console.log("data " + JSON.stringify(data));
				console.log("status " + status);

				var messageList = data.responseJSON.messageList;
				let myMap = new Map();
				myMap = messageList;
				console.log("myMap" + JSON.stringify(myMap));
				for (const [key, value] of Object.entries(myMap)) {
					// console.log(`${key}: ${value}`);
					// console.log("Key "+key );
					// console.log("value "+value );
					document.getElementById("" + key).innerHTML = value;
				}
				showAlerts(data.responseJSON.serviceResult, data.statusText, "alertMessage", false, data.responseJSON.message, 2000);
			}
		});
	}

}
var editYeatList = [];
function selectYear(datas) {

	editYeatList = editYeatList.filter((x) => x.id == datas);
	console.log("editYeatList" + JSON.stringify(editYeatList));
	$("#year1DD").val(editYeatList[0].yearMasterDTO.id);
	$("#hiddenId").val(editYeatList[0].id);
}
function deleteData(datas) {
	alert(datas);
}
function createdataTables(datas) {
	$('#inst_additional_data_table').DataTable({
		"columnDefs": [{
			"targets": [8, 9, 10],
			'orderable': false,
			render: function (data, type, row, meta) {
				if (meta.col == 8) {
					var active = row.active;
					if (active != true) {
						var action = '<span class=""><i class="fa fa-edit text-info btn btn-link" id="editDetail" data-toggle="tooltip" title="Edit Detail" style="" onclick="selectYear(' + row.id + ')"></i>' +
							'<i id="deleteDetail" class="fa fa-trash-o text-danger btn btn-link" data-toggle="tooltip" title="Delete Detail" style="" onclick="deleteTrustData(' + row.id + ')"></i></span>';
						// var action = '<i class="fa fa-edit fatbl text-info" data-toggle="tooltip" title="Edit Department" style="font-size:20px;"onclick="selectYear('+row.yearMasterDTO.id+')"></i>';
						return action;
					} else {
						return '-';
					}

				} else if (meta.col == 9) {

					if (row.active == false) {
						var lock = '<span class=""><i class="fa fa-unlock text-info btn btn-link" id="lockDetail" data-toggle="tooltip" title="lock Detail" style="" onclick="lockBlock(' + row.id + ')"></i></span>';
						return lock;
					} else {
						var lock = '<span class=""><i class="fa fa-lock text-info btn btn-link" id="lockDetail" data-toggle="tooltip" title="lock Detail" style="" "></i></span>';
						return lock;
					}

				} else if (meta.col == 10) {

					if (row.active == true) {
						var unlock = '<span class=""><i class="fa fa-unlock text-info btn btn-link" id="unlock Detail" data-toggle="tooltip" title="lock Detail" style="" onclick="unlockBlock(' + row.id + ')"></i></span>';
						return unlock;
					} else {
						var unlock = '-';
						return unlock;
					}
				}

			}
		}],
		"lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
		"retrieve": true,
		"destroy": true,
		"data": datas,
		"createdRow": function (row, data, dataIndex) {
			this.api().cell($('td:eq(0)', row)).data(dataIndex + 1);
		},
		"columns": [{
			"title": "Sr.No",
			"data": "activeYear",
			"sDefaultContent": "-"
		},
		{
			"title": "SelectedYear",
			"data": "displayActiveYear",
			"sDefaultContent": "-"
		}, {
			"title": "FirstPastYear",
			"data": "firstPastYear",
			"sDefaultContent": "-"
		}, {
			"title": "secondPastYear",
			"data": "secondPastYear",
			"sDefaultContent": "-"
		},
		{
			"title": "ThirdPastYear",
			"data": "thirdPastYear",
			"sDefaultContent": "-"
		},
		{
			"title": "FirstFutureYear",
			"data": "firstfutureYear",
			"sDefaultContent": "-"
		},
		{
			"title": "SecondFutureYear",
			"data": "secondfutureYear",
			"sDefaultContent": "-"
		},
		{
			"title": "ThirdFutureYear",
			"data": "thirdfutureYear",
			"sDefaultContent": "-"
		},
		{
			"title": "Action",
			"data": "secondPastYear",
			"sDefaultContent": "-"
		}, {
			"title": "lock",
			"data": "secondPastYear",
			"sDefaultContent": "-"
		}, {
			"title": "Unlock",
			"data": "secondPastYear",
			"sDefaultContent": "-"
		}]
	});
}
function deleteTrustData(id, endpoint, result) {
	Swal.fire({
		title: 'Are you sure?',
		html: "You want to Delete Trust Details",
		icon: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		confirmButtonText: "YES",
		cancelButtonColor: '#d33',
		cancelButtonText: "Cancel",
		closeOnConfirm: false,
		closeOnCancel: false,
		allowOutsideClick: false
	}).then((result) => {
		if (result.value) {
			if (id != 0) {
				$.ajax({
					type: "DELETE",
					url: "/admin/",
					headers: {
						'API-VERSION': 1
					},
					data: { adminBlockId: id },
					success: function (data) {
						if (data.success == 1) {
							showAlerts(data.message, data.messageList, "alertMessage", false, data.serviceResult, 2000);
						} else {
							Swal.fire({
								title: 'ERROR',
								html: data.message,
								icon: 'error',
								confirmButtonColor: '#3085d6',
								confirmButtonText: "OK",
								closeOnConfirm: false,
								allowOutsideClick: false,
							}).then((result) => {
								if (result.value) {

								}
							})

						}
					},
					error: function (error) {
						Swal.fire({
							title: 'ERROR',
							html: "Something Went Wrong, Please Try Again",
							icon: 'error',
							confirmButtonColor: '#3085d6',
							confirmButtonText: "OK",
							closeOnConfirm: false,
							allowOutsideClick: false,
						}).then((result) => {
							if (result.value) {

							}
						})

					}
				});

			}
		}

	})
}

function lockBlock(block_id) {
	adminBlockDetailDTOList = adminBlockDetailDTOList.filter((x) => x.active == true);

	console.log("adminBlockDetailDTOList " + adminBlockDetailDTOList.length);

	if (adminBlockDetailDTOList.length > 0) {
		Swal.fire({
			title: 'Already Block is active',
			html: "You Can Not Active This Block",
			icon: 'error',
			showCancelButton: false,
			confirmButtonColor: '#3085d6',
			confirmButtonText: "OK",
			cancelButtonColor: '#d33',
			cancelButtonText: "Cancel",
			closeOnConfirm: false,
			closeOnCancel: false,
			allowOutsideClick: false
		})
	} else {
		Swal.fire({
			title: 'Are you sure?',
			html: "You want to Lock This Block",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			confirmButtonText: "YES",
			cancelButtonColor: '#d33',
			cancelButtonText: "Cancel",
			closeOnConfirm: false,
			closeOnCancel: false,
			allowOutsideClick: false
		}).then((result) => {
			if (result.value) {
				if (block_id != 0) {
					$.ajax({
						type: "PUT",
						url: "/admin/",
						headers: {
							'API-VERSION': 1
						},
						data: { adminBlockId: block_id },
						success: function (data) {
							if (data.success == 1) {
								showAlerts(data.message, data.messageList, "alertMessage", false, data.serviceResult, 2000);
								FetchBlockDetail();
							} else {
								Swal.fire({
									title: 'ERROR',
									html: data.message,
									icon: 'error',
									confirmButtonColor: '#3085d6',
									confirmButtonText: "OK",
									closeOnConfirm: false,
									allowOutsideClick: false,
								}).then((result) => {
									if (result.value) {

									}
								})

							}
						},
						error: function (error) {
							Swal.fire({
								title: 'ERROR',
								html: "Something Went Wrong, Please Try Again",
								icon: 'error',
								confirmButtonColor: '#3085d6',
								confirmButtonText: "OK",
								closeOnConfirm: false,
								allowOutsideClick: false,
							}).then((result) => {
								if (result.value) {

								}
							})

						}
					});

				}
			}

		})
	}

}

function unlockBlock(block_id) {
	Swal.fire({
		title: 'Are you sure?',
		html: "You want to Unlock This Block",
		icon: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		confirmButtonText: "YES",
		cancelButtonColor: '#d33',
		cancelButtonText: "Cancel",
		closeOnConfirm: false,
		closeOnCancel: false,
		allowOutsideClick: false
	}).then((result) => {
		if (result.value) {
			if (block_id != 0) {
				$.ajax({
					type: "PUT",
					url: "/admin/unlock/",
					headers: {
						'API-VERSION': 1
					},
					data: { adminBlockId: block_id },
					success: function (data) {
						if (data.success == 1) {
							showAlerts(data.message, data.messageList, "alertMessage", false, data.serviceResult, 2000);
							FetchBlockDetail();
						} else {
							Swal.fire({
								title: 'ERROR',
								html: data.message,
								icon: 'error',
								confirmButtonColor: '#3085d6',
								confirmButtonText: "OK",
								closeOnConfirm: false,
								allowOutsideClick: false,
							}).then((result) => {
								if (result.value) {

								}
							})

						}
					},
					error: function (error) {
						Swal.fire({
							title: 'ERROR',
							html: "Something Went Wrong, Please Try Again",
							icon: 'error',
							confirmButtonColor: '#3085d6',
							confirmButtonText: "OK",
							closeOnConfirm: false,
							allowOutsideClick: false,
						}).then((result) => {
							if (result.value) {

							}
						})

					}
				});

			}
		}

	})
}