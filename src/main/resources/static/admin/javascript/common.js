$(document).ready(function(){
	pages();
});

function deptByInst(){
	$("#departmentId").empty();
	$.getJSON("/common/deptByInst",{
		ajax: 'true',
		cache: 'false'
	}, function (data) {
		console.log(data);
		$("#departmentId").append('<option value=""  selected="" disabled="">'+ "Select" + '</option>');
		$.each(data, function (index, value) {
			$('#departmentId').append('<option value="' + value.id + '">'+ value.name + '</option>').trigger('chosen:updated');
		});
	});
}

function pages(){
	$("#pages").empty();
	$.getJSON("/common/page",{
		ajax: 'true',
		cache: 'false'
	}, function (data) {
		$("#pages").append('<option value=""  selected="" disabled="">'+ "Select" + '</option>');
		$.each(data, function (index, value) {
			$('#pages').append('<option value="' + value.id + '">'+ value.name + '</option>').trigger('chosen:updated');
		});
	});
}

function tblexport(id,index,name){
	if(index!='NA'){
		myDataTable = $("#"+id).DataTable()
  		myDataTable.column([index]).visible(false);
	}
	$("#"+id).table2excel({
	   exclude : ".noExl",
	   name : name,
	   filename : name,
	   fileext : ".xls"
   });
   if(index!='NA'){
   		myDataTable = $("#"+id).DataTable()
  		myDataTable.column([index]).visible(true);
   }
	
}


function downloadDoc(docPath, filename){
	location.href="/downloadDoc?docPath="+docPath+"&fileName="+filename;
}


    
