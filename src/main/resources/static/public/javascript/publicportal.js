$(document).ready(function() {	
	getPublicPortalData();
});
var json;
function getPublicPortalData(){
	$.ajax({
		type: "get",
		url: "/getPublicPortalData",
		dataType: "json",
		success: function (data, textStatus, jqXHR) {	
				console.dir(data);	
				// Setting Up Slider..
				for(var i=0;i<data.sliderDetaillist.length;i++){
					var slider = "#slider".concat(i+1);
					$(slider).attr("src","/"+data.sliderDetaillist[i].docPath);
				}	
				// Setting Up Departments.
				for(var i=0;i<data.departmentByInstitute.length;i++){
					console.log(data.departmentByInstitute[i].name);			
					 $("#allDeptDiv").append('<div class="col-lg-4 col-md-6 col-12"> '
					 		+ ' <div class="single-item">'
					 		+ '<div class="single-item-image overlay-effect"> '
                            + '<a href="#"><img src="https://www.w3schools.com/images/picture.jpg" alt="No Image" height="100px" width="100px"></a>'
                            +  '</div>'
                            + ' <div class="button-bottom">'
                            + ' <a href="#" class="button-default">'+data.departmentByInstitute[i].name+'</a>'
                             + ' </div>'
					 		+ '</div>'
               			+'</div>');
				}					
		},
		error: function (jqXHR, textStatus, errorThrown) {
			console.log("Something really bad happened "
				+ textStatus);
			$("#ajaxResponse").html(jqXHR.responseText);
		}
	});
}