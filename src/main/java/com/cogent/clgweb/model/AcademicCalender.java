package com.cogent.clgweb.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "academic_calender")
public class AcademicCalender extends BaseModel {

	@Column(name = "institute_id")
	private Long instituteId;

	@Column(name = "event_name")
	private String eventName;

	@Column(name = "description", length = 1000)
	private String description;

	@Column(name = "event_date")
	private String eventDate;

	@Column(name = "calender_active")
	private Boolean calederActive;

	public Long getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Long instituteId) {
		this.instituteId = instituteId;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEventDate() {
		return eventDate;
	}

	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}

	public Boolean getCalederActive() {
		return calederActive;
	}

	public void setCalederActive(Boolean calederActive) {
		this.calederActive = calederActive;
	}

	@Override
	public String toString() {
		return "AcademicCalender [instituteId=" + instituteId + ", eventName=" + eventName + ", description="
				+ description + ", eventDate=" + eventDate + ", calederActive=" + calederActive + "]";
	}
}
