package com.cogent.clgweb.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "place")
public class Place extends BaseModel {

	public Place() {
		super();
	}

	@Column(name = "institute_id")
	private Long instituteId;

	@Column(name = "name")
	private String name;

	@Column(name = "doc_path")
	private String docPath;

	public Long getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Long instituteId) {
		this.instituteId = instituteId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDocPath() {
		return docPath;
	}

	public void setDocPath(String docPath) {
		this.docPath = docPath;
	}

}
