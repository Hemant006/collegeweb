package com.cogent.clgweb.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "menu_institute_type_mpg")
public class MenuInstituteTypeMpg extends BaseModel {

	@Column(name = "menu_id")
	private Long menuId;

	@Column(name = "institute_type_id")
	private Long instituteTypeId;

	@Column(name = "role_id")
	private Long roleId;

	@Column(name = "portal_id")
	private Long portalId;

	@Column(name = "parent_id")
	private Long parentId;

	@Column(name = "url")
	private String url;

	public Long getMenuId() {
		return menuId;
	}

	public void setMenuId(Long menuId) {
		this.menuId = menuId;
	}

	public Long getInstituteTypeId() {
		return instituteTypeId;
	}

	public void setInstituteTypeId(Long instituteTypeId) {
		this.instituteTypeId = instituteTypeId;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Long getPortalId() {
		return portalId;
	}

	public void setPortalId(Long portalId) {
		this.portalId = portalId;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
