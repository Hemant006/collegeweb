package com.cogent.clgweb.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "cp_document_mpg")
public class CPDocumentMpg extends BaseModel {

	@Column(name = "page_inst_mpg_id")
	private Long pageInstMpgId;

	@Column(name = "doc_path")
	private String docPath;

	@Column(name = "doc_title")
	private String docTitle;

	public Long getPageInstMpgId() {
		return pageInstMpgId;
	}

	public void setPageInstMpgId(Long pageInstMpgId) {
		this.pageInstMpgId = pageInstMpgId;
	}

	public String getDocPath() {
		return docPath;
	}

	public void setDocPath(String docPath) {
		this.docPath = docPath;
	}

	public String getDocTitle() {
		return docTitle;
	}

	public void setDocTitle(String docTitle) {
		this.docTitle = docTitle;
	}

	@Override
	public String toString() {
		return "CPDocumentMpg [pageInstMpgId=" + pageInstMpgId + ", docPath=" + docPath + ", docTitle=" + docTitle
				+ "]";
	}
}
