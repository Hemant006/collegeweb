package com.cogent.clgweb.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "page_inst_mpg")
public class PageInstMpg extends BaseModel {

	@Column(name = "institute_id")
	private Long instituteId;

	@Column(name = "page_id")
	private Long pageId;

	@Column(name = "description", length = 1000)
	private String description;

	public Long getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Long instituteId) {
		this.instituteId = instituteId;
	}

	public Long getPageId() {
		return pageId;
	}

	public void setPageId(Long pageId) {
		this.pageId = pageId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "PageInstMpg [instituteId=" + instituteId + ", pageId=" + pageId + ", description=" + description + "]";
	}
	
}
