package com.cogent.clgweb.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "college_detail")
public class CollegeDetail extends BaseModel{

	@Column(name = "institute_id")
	private Long instituteId;

	@Column(name = "college_image")
	private String collegeImage;

	@Column(name = "principal_image")
	private String principalImage;

	@Column(name = "course")
	private String course;

	@Column(name = "google_map_url", length = 1000)
	private String googleMapURL;

	@Column(name = "about", length = 1000)
	private String about;

	@Column(name = "principal_title")
	private String principalTitle;

	@Column(name = "principal_msg", length = 1000)
	private String principalMsg;

	public Long getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Long instituteId) {
		this.instituteId = instituteId;
	}

	public String getCollegeImage() {
		return collegeImage;
	}

	public void setCollegeImage(String collegeImage) {
		this.collegeImage = collegeImage;
	}

	public String getPrincipalImage() {
		return principalImage;
	}

	public void setPrincipalImage(String principalImage) {
		this.principalImage = principalImage;
	}

	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}

	public String getGoogleMapURL() {
		return googleMapURL;
	}

	public void setGoogleMapURL(String googleMapURL) {
		this.googleMapURL = googleMapURL;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public String getPrincipalTitle() {
		return principalTitle;
	}

	public void setPrincipalTitle(String principalTitle) {
		this.principalTitle = principalTitle;
	}

	public String getPrincipalMsg() {
		return principalMsg;
	}

	public void setPrincipalMsg(String principalMsg) {
		this.principalMsg = principalMsg;
	}
}
