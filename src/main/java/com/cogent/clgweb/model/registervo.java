package com.cogent.clgweb.model;

public class registervo {

	private int regid;
	private String clgname;
	private String clg_id;
	private String salutation;
	private String surname;
	private String personname;
	private String lastname;
	private String gender;
	private String dsgname;
	private String dsg_id;
	private String dept;
	private String dept_id;
	private String stafftype;
	private String mobile;
	private String email;
	private String qualification;
	private String dob;
	private String gradepay;
	private String institutetype;
	private String insttype_id;
	private int role_id;
	private String aadharno;
	private String photopath;
	private int class_type;
	private String work_type;
	private String govtEmailId;
	private Long university_id;
	private Integer verified;
	private Long district_id;
	private String rolename;
	private String shortClgName;

	// private Set<RoleIdMapping> maprole;
	public int getRegid() {
		return regid;
	}

	public void setRegid(int regid) {
		this.regid = regid;
	}

	public String getClgname() {
		return clgname;
	}

	public void setClgname(String clgname) {
		this.clgname = clgname;
	}

	public String getClg_id() {
		return clg_id;
	}

	public void setClg_id(String clg_id) {
		this.clg_id = clg_id;
	}

	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getPersonname() {
		return personname;
	}

	public void setPersonname(String personname) {
		this.personname = personname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDsgname() {
		return dsgname;
	}

	public void setDsgname(String dsgname) {
		this.dsgname = dsgname;
	}

	public String getDsg_id() {
		return dsg_id;
	}

	public void setDsg_id(String dsg_id) {
		this.dsg_id = dsg_id;
	}

	public String getDept() {
		return dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}

	public String getDept_id() {
		return dept_id;
	}

	public void setDept_id(String dept_id) {
		this.dept_id = dept_id;
	}

	public String getStafftype() {
		return stafftype;
	}

	public void setStafftype(String stafftype) {
		this.stafftype = stafftype;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getGradepay() {
		return gradepay;
	}

	public void setGradepay(String gradepay) {
		this.gradepay = gradepay;
	}

	public String getInstitutetype() {
		return institutetype;
	}

	public void setInstitutetype(String institutetype) {
		this.institutetype = institutetype;
	}

	public String getInsttype_id() {
		return insttype_id;
	}

	public void setInsttype_id(String insttype_id) {
		this.insttype_id = insttype_id;
	}

	public int getRole_id() {
		return role_id;
	}

	public void setRole_id(int role_id) {
		this.role_id = role_id;
	}

	public String getAadharno() {
		return aadharno;
	}

	public void setAadharno(String aadharno) {
		this.aadharno = aadharno;
	}

	public String getPhotopath() {
		return photopath;
	}

	public void setPhotopath(String photopath) {
		this.photopath = photopath;
	}

	public int getClass_type() {
		return class_type;
	}

	public void setClass_type(int class_type) {
		this.class_type = class_type;
	}

	public String getWork_type() {
		return work_type;
	}

	public void setWork_type(String work_type) {
		this.work_type = work_type;
	}

	public String getGovtEmailId() {
		return govtEmailId;
	}

	public void setGovtEmailId(String govtEmailId) {
		this.govtEmailId = govtEmailId;
	}

	public Long getDistrict_id() {
		return district_id;
	}

	public void setDistrict_id(Long district_id) {
		this.district_id = district_id;
	}

	public Long getUniversity_id() {
		return university_id;
	}

	public void setUniversity_id(Long university_id) {
		this.university_id = university_id;
	}

	public Integer getVerified() {
		return verified;
	}

	public void setVerified(Integer verified) {
		this.verified = verified;
	}

	public String getRolename() {
		return rolename;
	}

	public void setRolename(String rolename) {
		this.rolename = rolename;
	}

	public String getShortClgName() {
		return shortClgName;
	}

	public void setShortClgName(String shortClgName) {
		this.shortClgName = shortClgName;
	}

	@Override
	public String toString() {
		return "registervo{" + "regid=" + regid + ", clgname=" + clgname + ", clg_id=" + clg_id + ", salutation="
				+ salutation + ", surname=" + surname + ", personname=" + personname + ", lastname=" + lastname
				+ ", gender=" + gender + ", dsgname=" + dsgname + ", dsg_id=" + dsg_id + ", dept=" + dept + ", dept_id="
				+ dept_id + ", stafftype=" + stafftype + ", mobile=" + mobile + ", email=" + email + ", qualification="
				+ qualification + ", dob=" + dob + ", gradepay=" + gradepay + ", institutetype=" + institutetype
				+ ", insttype_id=" + insttype_id + ", role_id=" + role_id + ", aadharno=" + aadharno + ", photopath="
				+ photopath + ", class_type=" + class_type + ", work_type=" + work_type + ", govtEmailId=" + govtEmailId
				+ ", university_id=" + university_id + ", verified=" + verified + ", district_id=" + district_id
				+ ", rolename=" + rolename + ", shortClgName=" + shortClgName + '}';
	}

	public registervo(int regid, String clgname, String clg_id, String salutation, String surname, String personname,
			String lastname, String gender, String dsgname, String dsg_id, String dept, String dept_id,
			String stafftype, String mobile, String email, String qualification, String dob, String gradepay,
			String institutetype, String insttype_id, int role_id, String aadharno, String photopath, int class_type,
			String work_type, String govtEmailId, Long university_id, Integer verified, Long district_id,
			String rolename, String shortClgName) {
		super();
		this.regid = regid;
		this.clgname = clgname;
		this.clg_id = clg_id;
		this.salutation = salutation;
		this.surname = surname;
		this.personname = personname;
		this.lastname = lastname;
		this.gender = gender;
		this.dsgname = dsgname;
		this.dsg_id = dsg_id;
		this.dept = dept;
		this.dept_id = dept_id;
		this.stafftype = stafftype;
		this.mobile = mobile;
		this.email = email;
		this.qualification = qualification;
		this.dob = dob;
		this.gradepay = gradepay;
		this.institutetype = institutetype;
		this.insttype_id = insttype_id;
		this.role_id = role_id;
		this.aadharno = aadharno;
		this.photopath = photopath;
		this.class_type = class_type;
		this.work_type = work_type;
		this.govtEmailId = govtEmailId;
		this.university_id = university_id;
		this.verified = verified;
		this.district_id = district_id;
		this.rolename = rolename;
		this.shortClgName = shortClgName;
	}

	public registervo() {
		super();
	}
	
}