package com.cogent.clgweb.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "slider_detail")
public class SliderDetail extends BaseModel {

	@Column(name = "institute_id")
	private Long instituteId;

	@Column(name = "doc_path")
	private String docPath;
	
	@Column(name = "sequence")
	private Integer seqenceNo=0;

	public Long getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Long instituteId) {
		this.instituteId = instituteId;
	}

	public String getDocPath() {
		return docPath;
	}

	public void setDocPath(String docPath) {
		this.docPath = docPath;
	}

	public Integer getSeqenceNo() {
		return seqenceNo;
	}

	public void setSeqenceNo(Integer seqenceNo) {
		this.seqenceNo = seqenceNo;
	}
	
}
