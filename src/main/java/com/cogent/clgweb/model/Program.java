package com.cogent.clgweb.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "program")
public class Program extends BaseModel{

	public Program() {
		super();
	}

	@Column(name = "institute_id")
	private Long instituteId;
	
	@Column(name = "program_id")
	private Integer programId;
	
	@Column(name = "program_title")
	private String programTitle;
	
	@Column(name = "detail_title")
	private String detailTitle;
	
	@Column(name = "program_detail", length = 1000)
	private String programDetail;

	public Long getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Long instituteId) {
		this.instituteId = instituteId;
	}

	public Integer getProgramId() {
		return programId;
	}

	public void setProgramId(Integer programId) {
		this.programId = programId;
	}

	public String getProgramTitle() {
		return programTitle;
	}

	public void setProgramTitle(String programTitle) {
		this.programTitle = programTitle;
	}

	public String getDetailTitle() {
		return detailTitle;
	}

	public void setDetailTitle(String detailTitle) {
		this.detailTitle = detailTitle;
	}

	public String getProgramDetail() {
		return programDetail;
	}

	public void setProgramDetail(String programDetail) {
		this.programDetail = programDetail;
	}
}
