package com.cogent.clgweb.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "upload_document")
public class UploadDocument extends BaseModel {

	@Column(name = "institute_id")
	private Long instituteId;

	@Column(name = "title")
	private String title;

	@Column(name = "doc_path")
	private String docPath;

	public Long getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Long instituteId) {
		this.instituteId = instituteId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDocPath() {
		return docPath;
	}

	public void setDocPath(String docPath) {
		this.docPath = docPath;
	}

}
