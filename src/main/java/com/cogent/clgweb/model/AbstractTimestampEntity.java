package com.cogent.clgweb.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import org.springframework.boot.context.properties.bind.DefaultValue;

import com.cogent.clgweb.utils.Functions;
import com.sun.el.lang.FunctionMapperImpl.Function;

@MappedSuperclass
public class AbstractTimestampEntity extends Object implements Serializable {

	@Column(name = "created_at", nullable = true)
	private String createdAt=Functions.getCurrentTimeStamp();

	@Column(name = "updated_at", nullable = true)
	private String updatedAt;

	@Column(name = "created_by", nullable = true)
	private Long createdBy;

	@Column(name = "updated_by", nullable = true)
	private Long updatedBy;

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Long getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

}