package com.cogent.clgweb.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="page_mst")
public class PageMaster extends BaseModel{

	public PageMaster() {
		super();
	}
	
	@Column(name = "name")
	private String name;

	@Column(name = "is_document_provided")
	private Integer isDocumentProvided=0;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getIsDocumentProvided() {
		return isDocumentProvided;
	}

	public void setIsDocumentProvided(Integer isDocumentProvided) {
		this.isDocumentProvided = isDocumentProvided;
	}

	@Override
	public String toString() {
		return "PageMaster [name=" + name + ", isDocumentProvided=" + isDocumentProvided + "]";
	}
	
}
