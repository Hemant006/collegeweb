package com.cogent.clgweb.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "facilitie")
public class Facilitie extends BaseModel {

	@Column(name = "institute_id")
	private Long instituteId;
	
	@Column(name = "name")
	private String name;

	@Column(name = "department_id")
	private Long departmentId;

	@Column(name = "description", length = 1000)
	private String description;

	@Column(name = "sequence")
	private Integer sequence;

	// 0 for lab and 1 for workshop
	@Column(name = "labWorkshop")
	private Boolean labWorkshop;

	@Column(name = "doc_path")
	private String docPath;

	public Long getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Long instituteId) {
		this.instituteId = instituteId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(Long departmentId) {
		this.departmentId = departmentId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public Boolean getLabWorkshop() {
		return labWorkshop;
	}

	public void setLabWorkshop(Boolean labWorkshop) {
		this.labWorkshop = labWorkshop;
	}

	public String getDocPath() {
		return docPath;
	}

	public void setDocPath(String docPath) {
		this.docPath = docPath;
	}

}
