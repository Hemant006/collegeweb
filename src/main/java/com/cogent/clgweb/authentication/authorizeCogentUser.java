package com.cogent.clgweb.authentication;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.cogent.clgweb.dto.MenuMpgDTO;
import com.cogent.clgweb.model.registervo;
import com.cogent.clgweb.request.InstituteDetail;
import com.cogent.clgweb.service.MenuService;
import com.cogent.clgweb.utils.Constant;
import com.cogent.clgweb.utils.ConstantView;
import com.cogent.clgweb.utils.Functions;

@Controller
@CrossOrigin(origins = "*")
public class authorizeCogentUser {

	@Autowired
	MenuService menuService;

	@GetMapping("/admin")
	public ModelAndView test(String token, HttpSession session, Model model) {
		ModelAndView mav = new ModelAndView();
		try {
			mav.setViewName(ConstantView.UPLOAD_DOCUMENT);
			registervo rvo = Functions.decodeJWT(token);
			session.setAttribute(Constant.SESSION_USER, rvo);
			session.setMaxInactiveInterval(300000000);
			//menu
			List<MenuMpgDTO> list = menuService.menuList(Long.valueOf(rvo.getInsttype_id()), Long.valueOf(rvo.getRole_id()));
			session.setAttribute("menulist", list);
			//title
			model.addAttribute("title", "Dashboard");
			model.addAttribute("collegeName", rvo.getClgname());

			//Dashboard data from ctehostel
			List<InstituteDetail> instData=menuService.instituteDetail(Long.valueOf(rvo.getClg_id()));
			model.addAttribute("instDetail", instData);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}

}
