package com.cogent.clgweb.utils;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.lang.StringEscapeUtils;

import com.cogent.clgweb.model.registervo;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

public class Functions {

	public static String getNormalDateFormat(String date) {
		if (date.length() > 9) {
			date = date.substring(0, 10);
			String year = date.substring(0, 4);
			String month = date.substring(5, 7);
			String dt = date.substring(8, 10);
			date = dt + "-" + month + "-" + year;

		}
		return date;
	}

	public static String getCurrentTimeStamp() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		String currentDate = sdf.format(cal.getTime());
		return currentDate;
	}

	public static String escapeHtml4(HttpServletRequest request, String field) {
	        return StringEscapeUtils.escapeHtml(request.getParameter(field));
    }
	
	public static String extractFileName(Part part) {
		String disposition = part.getHeader("Content-Disposition");
		String fileName = disposition.replaceFirst("(?i)^.*filename=\"?([^\"]+)\"?.*$", "$1");
        fileName = fileName.replaceAll(" ","_");
		return fileName;
	}
	
	public static String docPathFolder(String foldername, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		String rootPath = System.getProperty("catalina.home");
		Path aPath = Paths.get(rootPath);
		Path parentsParentpath = aPath.getParent();
		String appPath = String.valueOf(parentsParentpath) + File.separator + foldername;
		System.out.println("DocumentRootPath " +appPath);
		File fileSaveDir = new File(appPath);
		if (!fileSaveDir.exists()) {
			fileSaveDir.mkdirs();
		}
		return appPath;
	}
	
	public static registervo decodeJWT(String jwt) {
        //This line will throw an exception if it is not a signed JWS (as expected)
		registervo jwtRequest = null;
		try {
			Claims claims = Jwts.parser()
	                .setSigningKey(DatatypeConverter.parseBase64Binary(Constant.SECRET_KEY))
	                .parseClaimsJws(jwt).getBody();

	        final ObjectMapper mapper = new ObjectMapper();
	        jwtRequest = mapper.convertValue(claims.get("user"), registervo.class);
		}catch (Exception e) {
			e.printStackTrace();
		}
        return jwtRequest;
    }  
	 
}
