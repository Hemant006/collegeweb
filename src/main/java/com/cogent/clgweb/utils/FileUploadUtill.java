package com.cogent.clgweb.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

import com.cogent.clgweb.exception.MyFileNotFoundException;


public class FileUploadUtill {

	public static String uploadFile(MultipartFile file, String subDir, Integer fileType)
			throws IOException {
		String rootPath = System.getProperty("catalina.home");
		Path aPath = Paths.get(rootPath);
		Path parentsParentpath = aPath.getParent();
		String appPath = String.valueOf(parentsParentpath) + File.separator + Constant.DOC_BASE_PATH + File.separator+ subDir;
		File fileSaveDir = new File(appPath);
		if (!fileSaveDir.exists()) {
			fileSaveDir.mkdirs();
		}
		String filePathDB = "";
		try {
			Boolean fileSize = false;
			Boolean fileFormate = false;
			String fileName = file.getOriginalFilename();
			Long actualFileSize = file.getSize() / 1024;
			Long documentSizeInKb = (long) (1 * 1024);
			String contentType = file.getContentType();

			if(fileType==Constant.PDF_FORMAT) {
				if (contentType.equalsIgnoreCase("application/pdf")) {
					System.out.println("File Formate is Valid ");
					fileFormate = true;
				} else {
					System.out.println("File Formate is not Valid ");
					throw new MyFileNotFoundException("Error while Uploading Document ! Please Upload Valid File Formate");
				}
			}else if(fileType==Constant.IMAGE_FORMAT) {
				if (contentType.equalsIgnoreCase("image/png") || contentType.equalsIgnoreCase("image/jpeg")
						|| contentType.equalsIgnoreCase("image/jpg")) {
					System.out.println("File Formate is Valid ");
					fileFormate = true;
				} else {
					System.out.println("File Formate is not Valid ");
					throw new MyFileNotFoundException("Error while Uploading Document ! Please Upload Valid File Formate");
				}
			}else if(fileType==Constant.PDF_AND_IMAGE_FORMAT) {
				if (contentType.equalsIgnoreCase("application/pdf") ||contentType.equalsIgnoreCase("image/png") || contentType.equalsIgnoreCase("image/jpeg")
						|| contentType.equalsIgnoreCase("image/jpg")) {
					System.out.println("File Formate is Valid ");
					fileFormate = true;
				} else {
					System.out.println("File Formate is not Valid ");
					throw new MyFileNotFoundException("Error while Uploading Document ! Please Upload Valid File Formate");
				}
			}

			if (actualFileSize < documentSizeInKb) {
				System.out.println("File Size is proper");
				fileSize = true;
			} else {
				System.out.println("File Formate is not proper ");
				throw new MyFileNotFoundException("Error while Uploading Document ! File Size Exceed than Required");
			}

			if ((fileSize == true) && (fileFormate == true)) {
				SimpleDateFormat trnformat = new SimpleDateFormat("yyMMddhhmmssMs");
				fileName = subDir + "_" + trnformat.format(new Date()) + "_" + fileName;
				fileName = fileName.replaceAll(" ", "");
				filePathDB = Constant.DOC_BASE_PATH + "/" + subDir + "/" + fileName;
				Path fileNameAndPath = Paths.get(appPath, fileName);
				System.out.println("DocumentPath:- " + fileNameAndPath);
				Files.write(fileNameAndPath, file.getBytes());
			}

		} catch (MyFileNotFoundException e) {
			e.printStackTrace();
			System.out.println("Exception Throw");
			throw e;
		} catch (IllegalStateException e) {
			e.printStackTrace();
			System.out.println("Error in Data insert! Check your file formats and Try again");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error in Data insert! Check your file formats and Try again");
		}
		return filePathDB;
	}

}
