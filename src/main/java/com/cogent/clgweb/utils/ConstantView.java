package com.cogent.clgweb.utils;

public class ConstantView {

	//ADMIN
	public static final String PLACE="admin/place";
	public static final String GALLERY="admin/gallery";
	public static final String INFO="admin/info";
	public static final String PROGRAM="admin/program";
	public static final String COLLEGE_DASHBOARD="admin/college_detail";
	public static final String ACADEMIC_CALENDER="admin/academic_calender";
	public static final String FACILITIES="admin/facilitie";
	public static final String UPLOAD_DOCUMENT="admin/upload_document";
	public static final String SLIDER="admin/slider";
	
	// PUBLIC
	public static final String PUBLIC_PORTAL="public/index";
	
}
