package com.cogent.clgweb.utils;

public class Constant {

	static Long instituteId=99L;
	
	//DOCUMENT
	public static final String DOC_BASE_PATH = "CollegeWeb/"+instituteId;
	public static final Integer PDF_FORMAT = 1;
	public static final Integer IMAGE_FORMAT = 2;
	public static final Integer PDF_AND_IMAGE_FORMAT = 3;
	
	public static final String API_HEADER_V_1 = "API-VERSION=1";
	
	// Constant For Response DTO
	public static final Integer SUCCESS_CODE = 1;
	public static final Integer ERROR_CODE = 0;
	
	public static final String ADD_SUCCESS_MESSAGE = "Details are added successfully.";
	
	public static final String INTERNAL_SERVER_SERVICERESULT = "Internal Server Error";
	public static final String INTERNAL_SERVER_MESSAGE = "Something Went Wrong, Please Try Again";
	
	public static final String SESSION_USER = "user";
	public static final String SECRET_KEY = "oeRaYY7Wo24sDqKSX3IM9ASGmdGPmkTd9jo1QTy4b7P9Ze5_9hKolVX8xNrQDcNRfVEdTZNOuOyqEGhXEbdJI-ZQ19k_o9MI0y3eZN2lp9jow55FfXMiINEdt1XR85VipRLSOkT6kSpzs2x-jbLDiz9iFVzkd81YKxMgPA7VfZeQUm4n-mOmnWMaVX30zGFU4L3oPBctYKkl4dYfqYWqRNfrgPJVi5DGFjywgxx0ASEiJHtV72paI3fDR2XwlSkyhhmY-ICjCRmsJN4fX1pdoL8a18-aQrvyu4j0Os6dVPYIoPvvY0SAZtWYKHfM15g7A3HD4cVREf9cUsprCRK93w";


}
