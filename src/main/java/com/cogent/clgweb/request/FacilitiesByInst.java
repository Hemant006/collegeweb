package com.cogent.clgweb.request;

public interface FacilitiesByInst {

	Long getId();
	String getDescription();
	String getDoc_path();
	Boolean getLab_workshop();
	String getName();
	Long getSequence();
	Long getInstituteId();
	String getDepartment();
}
