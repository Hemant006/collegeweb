package com.cogent.clgweb.request;

public interface DepartmentByInstitute {

	Long getId();
	
	String getName();
}
