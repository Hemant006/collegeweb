package com.cogent.clgweb.request;

public interface InstituteDetail {

	Long getId();
	String getName();
	String getLink();
	Long getUniversityId();
	Long getCode();
	Long getInstituteTypeId();
	String getShortName();
	String getAddress();
	String getCity();
	String getDistrict();
	String getInstemail();
	String getOthemail();
	String getInstphone();
	Long getPincode();
	String getInstlandline();
	Long getDistrict_code();
	Long getTaluka_code();
	String getOtherinfo();
	Long getZoneId();
	Integer getProfile_completed();
	Integer getEducation_type();
	Integer getLocality();
	String getStart_Date();
	String getApproval_order_no();
	Integer getSirf_rank();
	String getChecode();

}