package com.cogent.clgweb.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cogent.clgweb.model.CPDocumentMpg;

public interface CPDocumentMpgRepo extends JpaRepository<CPDocumentMpg, Long>{

	CPDocumentMpg findAllByPageInstMpgId(Long id);

	List<CPDocumentMpg> findByPageInstMpgIdAndIsDeleted(Long Id, Integer isDeleted);

}	
