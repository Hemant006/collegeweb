package com.cogent.clgweb.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.cogent.clgweb.model.Facilitie;
import com.cogent.clgweb.request.FacilitiesByInst;

public interface FacilitieRepo extends JpaRepository<Facilitie, Long>{

	@Query(nativeQuery=true, value="SELECT fc.id as id, fc.description as description, fc.doc_path as doc_path, fc.lab_workshop as lab_workshop,\r\n"
			+ "	fc.name as name,fc.sequence as sequence,fc.institute_id as institute_id, dm.name as department \r\n"
			+ "    FROM college_web.facilitie fc \r\n"
			+ "		left join ctehostel.department_master dm on dm.id=fc.department_id\r\n"
			+ "			where fc.is_deleted=0 and dm.is_deleted=0 and fc.institute_id=:instituteId\r\n"
			+ "				order by fc.name")
	List<FacilitiesByInst> facilitiesByInst(Long instituteId);

}
