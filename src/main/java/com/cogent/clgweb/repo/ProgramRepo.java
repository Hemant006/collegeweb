package com.cogent.clgweb.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cogent.clgweb.model.Program;

public interface ProgramRepo extends JpaRepository<Program, Long>{

	List<Program> findByinstituteIdAndIsDeleted(Long instituteId, Integer isDeleted);
	
	Optional<Program> findById(Long Id);
}
