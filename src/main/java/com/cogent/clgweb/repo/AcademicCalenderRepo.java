package com.cogent.clgweb.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cogent.clgweb.model.AcademicCalender;

public interface AcademicCalenderRepo extends JpaRepository<AcademicCalender, Long>{

	List<AcademicCalender> findByInstituteIdAndIsDeleted(Long instituteId, Integer i);

}
