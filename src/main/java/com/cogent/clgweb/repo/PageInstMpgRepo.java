package com.cogent.clgweb.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cogent.clgweb.model.PageInstMpg;

public interface PageInstMpgRepo extends JpaRepository<PageInstMpg, Long>{

	List<PageInstMpg> findAllByPageIdAndIsDeleted(Long pageId, Integer isDeleted);
	
	/*
	 * @Query(nativeQuery = true,
	 * value="SELECT cdm.doc_path as doc_path, cdm.doc_title as doc_title \r\n" +
	 * "	FROM college_web.page_inst_mpg pim\r\n" +
	 * "	left join cp_document_mpg cdm on cdm.page_inst_mpg_id=pim.id\r\n" +
	 * "		where pim.is_deleted=0 and cdm.is_deleted=0 and pim.institute_id=:instituteId"
	 * ) List infoDetaiList(@Param("instituteId") Long instituteId);
	 */

	List<PageInstMpg> findByInstituteId(Long instituteId);

	List<PageInstMpg> findByInstituteIdAndIsDeleted(Long instituteId, Integer isDeleted);
	
}
