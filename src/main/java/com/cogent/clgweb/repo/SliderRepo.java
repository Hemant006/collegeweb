package com.cogent.clgweb.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cogent.clgweb.dto.SliderDetailDTO;
import com.cogent.clgweb.model.SliderDetail;

public interface SliderRepo extends JpaRepository<SliderDetail, Long>{

	List<SliderDetail> findByinstituteIdAndIsDeleted(Long instituteId, Integer isDeleted);
	
	Optional<SliderDetail> findById(Long Id);
	
}
