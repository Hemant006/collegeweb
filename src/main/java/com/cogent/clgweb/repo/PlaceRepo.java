package com.cogent.clgweb.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cogent.clgweb.model.Place;

public interface PlaceRepo extends JpaRepository<Place, Long>{

	List<Place> findByinstituteIdAndIsDeleted(Long instituteId, Integer isDeleted);
	
	Optional<Place> findById(Long Id);
	
}
