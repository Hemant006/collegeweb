package com.cogent.clgweb.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cogent.clgweb.model.CategoryMaster;

public interface CategoryMasterlRepo extends JpaRepository<CategoryMaster, Long> {

	List<CategoryMaster> findByinstituteIdAndIsDeleted(Long instituteId, Integer isDeleted);

	Optional<CategoryMaster> findById(Long Id);

}
