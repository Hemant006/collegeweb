package com.cogent.clgweb.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cogent.clgweb.model.UploadDocument;

public interface UploadDocumentRepo extends JpaRepository<UploadDocument, Long>{

	List<UploadDocument> findByInstituteIdAndIsDeleted(Long instituteId, int i);

}
