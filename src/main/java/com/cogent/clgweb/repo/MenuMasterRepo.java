package com.cogent.clgweb.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.cogent.clgweb.model.MenuMaster;
import com.cogent.clgweb.request.InstituteDetail;

public interface MenuMasterRepo extends JpaRepository<MenuMaster, Long> {

	@Query(nativeQuery = true, value = "select * from ctehostel.inst_mst where is_deleted=0 and is_active=1 and id=:instituteId")
	List<InstituteDetail> instDetail(Long instituteId);

}
