package com.cogent.clgweb.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cogent.clgweb.model.MenuInstituteTypeMpg;

public interface MenuInstituteTypeMpgRepo extends JpaRepository<MenuInstituteTypeMpg, Long>{

	List<MenuInstituteTypeMpg> findByInstituteTypeIdAndRoleIdAndIsDeleted(Long instituteTypeId, Long roleId, Integer isDeleted);

}
