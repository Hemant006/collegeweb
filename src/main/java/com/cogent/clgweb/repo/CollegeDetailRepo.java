package com.cogent.clgweb.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cogent.clgweb.model.CollegeDetail;

public interface CollegeDetailRepo extends JpaRepository<CollegeDetail, Long>{

	List<CollegeDetail> findByInstituteIdAndIsDeleted(Long instituteId, Integer isDeleted);

}
