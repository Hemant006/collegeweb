package com.cogent.clgweb.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.cogent.clgweb.dto.CustomDTO;
import com.cogent.clgweb.model.PageMaster;
import com.cogent.clgweb.request.DepartmentByInstitute;

public interface PageMasterRepo extends JpaRepository<PageMaster, Long> {

	Optional<PageMaster> findById(Long Id);

	List<PageMaster> findByisDeleted(Integer isDeleted);
	
	@Query(nativeQuery=true, value="SELECT dm.id as id, dm.name as name \r\n"
			+ "	FROM ctehostel.institute_department_mpg idm\r\n"
			+ "	left join ctehostel.department_master dm on dm.id=idm.department_id\r\n"
			+ "		where idm.is_deleted=0 and dm.is_deleted=0 and idm.institute_id=:instituteId \r\n"
			+ "			order by name")
    List<DepartmentByInstitute> departmentByInstitute(Long instituteId);
	
}
