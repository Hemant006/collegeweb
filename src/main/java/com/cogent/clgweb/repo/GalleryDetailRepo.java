package com.cogent.clgweb.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cogent.clgweb.model.GalleryDetail;

public interface GalleryDetailRepo extends JpaRepository<GalleryDetail, Long> {

	List<GalleryDetail> findByinstituteIdAndIsDeleted(Long instituteId, Integer isDeleted);

	Optional<GalleryDetail> findById(Long Id);

}
