package com.cogent.clgweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CogentCollegeWebApplication {

	public static void main(String[] args) {
		
		SpringApplication.run(CogentCollegeWebApplication.class, args);
	}

}
