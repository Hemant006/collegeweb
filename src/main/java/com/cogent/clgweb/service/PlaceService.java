package com.cogent.clgweb.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.cogent.clgweb.model.Place;
import com.cogent.clgweb.model.registervo;

public interface PlaceService {

	void addDetail(Place pd, MultipartFile uploadedDocument, registervo rvo);
	
	void deleteDetail(Long Id, registervo rvo);
	
	List<Place> placeByInst(Long instituteId);
	
}
