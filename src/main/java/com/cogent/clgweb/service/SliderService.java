package com.cogent.clgweb.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.cogent.clgweb.dto.PublicPortalDataDTO;
import com.cogent.clgweb.dto.SliderDetailDTO;
import com.cogent.clgweb.model.Place;
import com.cogent.clgweb.model.SliderDetail;

public interface SliderService {

	void addSliderDetail(MultipartFile uploadedDocument);
	
	void deleteSliderDetail(Long Id);
	
	List<SliderDetail> sliderByInst(Long instituteId);
	
	PublicPortalDataDTO sliderDTOByInst(Long instituteId);
	
}
