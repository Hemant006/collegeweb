package com.cogent.clgweb.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.cogent.clgweb.model.UploadDocument;
import com.cogent.clgweb.model.registervo;

public interface UploadDocumentService {

	void addDetail(UploadDocument uploadDoc, MultipartFile file, registervo rvo);

	List<UploadDocument> documentByInstitute(Long instituteId);

	void deleteDetail(Long id, registervo rvo);

}
