package com.cogent.clgweb.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.cogent.clgweb.model.CategoryMaster;
import com.cogent.clgweb.model.GalleryDetail;


public interface GallaryDetailService {	
	
		void addCategoryDetail(CategoryMaster pd);
		
		void deleteCategoryDetail(Long Id);
		
		List<CategoryMaster> CategoryDetailByInst(Long instituteId);
		
		void addGalleryDetail(GalleryDetail gd, MultipartFile uploadedDocument);
		
		List<GalleryDetail> galleryDetailByInst(Long instituteId);
		
		void deleteGalleryDetail(Long Id);
}
