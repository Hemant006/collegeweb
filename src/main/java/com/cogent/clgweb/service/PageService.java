package com.cogent.clgweb.service;

import java.util.List;

import com.cogent.clgweb.model.PageMaster;
import com.cogent.clgweb.request.DepartmentByInstitute;

public interface PageService {

	List<PageMaster> pageDetail();

	List<DepartmentByInstitute> deptByInst(Long instituteId);
}
