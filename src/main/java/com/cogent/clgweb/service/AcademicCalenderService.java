package com.cogent.clgweb.service;

import java.util.List;

import com.cogent.clgweb.model.AcademicCalender;
import com.cogent.clgweb.model.registervo;

public interface AcademicCalenderService {

	void addDetail(AcademicCalender academicCalender, registervo rvo);

	List<AcademicCalender> academicCalenderByInst(Long instituteId);

	void deleteDetail(Long id, registervo rvo);

}
