package com.cogent.clgweb.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.cogent.clgweb.model.Facilitie;
import com.cogent.clgweb.model.registervo;
import com.cogent.clgweb.request.FacilitiesByInst;

public interface FacilitieService {

	void addDetail(Facilitie facilitie, MultipartFile file, registervo rvo);

	List<FacilitiesByInst> facilitiesByInst(Long instituteId);

	void deleteDetail(Long id, registervo rvo);

}
