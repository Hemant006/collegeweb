package com.cogent.clgweb.service;

import java.util.List;

import com.cogent.clgweb.model.Program;
import com.cogent.clgweb.model.registervo;

public interface ProgramService {

	void addDetail(Program program, registervo rvo);

	void deleteDetail(Long Id, registervo rvo);

	List<Program> ProgramByInst(Long instituteId);
}
