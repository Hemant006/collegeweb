package com.cogent.clgweb.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.cogent.clgweb.dto.InfoDetailDTO;
import com.cogent.clgweb.dto.PageInstMpgDTO;
import com.cogent.clgweb.model.CPDocumentMpg;
import com.cogent.clgweb.model.PageInstMpg;
import com.cogent.clgweb.model.registervo;

public interface InfoService {

	void addDetail(PageInstMpgDTO pageInstMpgDTO, registervo rvo, Long Id);
	
	void addInfoDetail(CPDocumentMpg pd, registervo rvo,MultipartFile file);
	
	List<PageInstMpg> pageInstMpgList(Long pageId);
	
	List<InfoDetailDTO> infoDetaiList(registervo rvo);

	void deleteDetail(Long id, registervo rvo);
}
