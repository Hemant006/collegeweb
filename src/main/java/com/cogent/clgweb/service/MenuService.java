package com.cogent.clgweb.service;

import java.util.List;

import com.cogent.clgweb.dto.MenuMpgDTO;
import com.cogent.clgweb.request.InstituteDetail;

public interface MenuService {

	List<MenuMpgDTO> menuList(Long instituteTypeId, Long roleId);

	List<InstituteDetail> instituteDetail(Long instituteId);


}
