package com.cogent.clgweb.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.cogent.clgweb.dto.CollegeDetailDTO;
import com.cogent.clgweb.model.CollegeDetail;
import com.cogent.clgweb.model.registervo;

public interface CollegeDetailService {

	void addDetail(CollegeDetailDTO collegeDetailDTO, MultipartFile clgImg, MultipartFile princiaplImg, registervo rvo, Long Id);

	List<CollegeDetail> clgDetailByInst(Long instituteId);

}
