package com.cogent.clgweb.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.cogent.clgweb.model.PageMaster;
import com.cogent.clgweb.model.registervo;
import com.cogent.clgweb.request.DepartmentByInstitute;
import com.cogent.clgweb.request.InstituteDetail;
import com.cogent.clgweb.service.MenuService;
import com.cogent.clgweb.service.PageService;
import com.cogent.clgweb.utils.Constant;
import com.cogent.clgweb.utils.ConstantView;

@RestController
@Controller
@RequestMapping("/common")
public class CommonController {

	@Autowired
	PageService pageService;
	
	@Autowired
	MenuService menuService;
	
	@GetMapping("")
	public ModelAndView test(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName(ConstantView.COLLEGE_DASHBOARD);
		return mav;
	}
	
	@GetMapping("/page")
	public List<PageMaster> pageDetail(HttpServletRequest request, HttpServletResponse response) {
		List<PageMaster> list=pageService.pageDetail();
		return list;
	}
	
	@GetMapping("/deptByInst")
	public List<DepartmentByInstitute> deptByInst() {
		List<DepartmentByInstitute> list=pageService.deptByInst(5L);
		return list;
	}
	
	@GetMapping("/instituteData")
	public List<InstituteDetail> clgDetailByInst(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session=request.getSession();
		registervo rvo = (registervo) session.getAttribute(Constant.SESSION_USER);
		List<InstituteDetail> list=menuService.instituteDetail(Long.valueOf(rvo.getClg_id()));
		return list;
	}
}
