package com.cogent.clgweb.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.cogent.clgweb.service.SliderService;
import com.cogent.clgweb.utils.ConstantView;
import com.cogent.clgweb.dto.PublicPortalDataDTO;
import com.cogent.clgweb.dto.SliderDetailDTO;

@RestController
@Controller
public class PublicPortalController {
	
	@Autowired
	SliderService sliderService;
	
	@GetMapping("")
	public ModelAndView loadPublicPortal(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName(ConstantView.PUBLIC_PORTAL);
		return mav;
	}
	
	@GetMapping("/getPublicPortalData")
	public PublicPortalDataDTO get(HttpServletRequest request, HttpServletResponse response) {
		PublicPortalDataDTO publicPortalDTO= sliderService.sliderDTOByInst(99L);
		return publicPortalDTO;
	}

}
