package com.cogent.clgweb.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.cogent.clgweb.dto.ResponseDTO;
import com.cogent.clgweb.model.AcademicCalender;
import com.cogent.clgweb.model.registervo;
import com.cogent.clgweb.service.AcademicCalenderService;
import com.cogent.clgweb.utils.Constant;
import com.cogent.clgweb.utils.ConstantView;

@Controller
@RestController
@RequestMapping("/admin")
public class AcademicCalenderController {

	@Autowired
	AcademicCalenderService academicCalenderService;
	
	@GetMapping("/academicCalender")
	public ModelAndView test(HttpServletRequest request, HttpServletResponse response, Model model) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName(ConstantView.ACADEMIC_CALENDER);
		model.addAttribute("title", "Academic Calender");
		return mav;
	}
	
	@PostMapping("/academicCalender")
	public void addPlace(@ModelAttribute AcademicCalender academicCalender, HttpServletRequest request , 
			HttpServletResponse response) throws IOException {
		try {
			HttpSession session=request.getSession();
			registervo rvo = (registervo) session.getAttribute(Constant.SESSION_USER);
			academicCalenderService.addDetail(academicCalender, rvo);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			response.sendRedirect("academicCalender");
		}
	}
	
	@GetMapping("/academicCalenderByInst")
	public List<AcademicCalender> placeByInst(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session=request.getSession();
		registervo rvo = (registervo) session.getAttribute(Constant.SESSION_USER);
		List<AcademicCalender> list=academicCalenderService.academicCalenderByInst(Long.valueOf(rvo.getClg_id()));
		return list;
	}
	
	@DeleteMapping(value="/academicCalender")
	public @ResponseBody ResponseEntity<ResponseDTO> deletePlace(@RequestParam Long Id, 
			HttpServletRequest request , HttpServletResponse response) throws IOException {
		ResponseDTO responseDTO = new ResponseDTO();
		try {
			HttpSession session=request.getSession();
			registervo rvo = (registervo) session.getAttribute(Constant.SESSION_USER);
			responseDTO.setSuccess(Constant.SUCCESS_CODE);
			responseDTO.setServiceResult("Deleted");
			responseDTO.setMessage("Deleted successfully");
			academicCalenderService.deleteDetail(Id, rvo);
			return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.OK);
		}catch (Exception e) {
			e.printStackTrace();
			responseDTO.setSuccess(Constant.ERROR_CODE);
			responseDTO.setServiceResult(Constant.INTERNAL_SERVER_SERVICERESULT);
			responseDTO.setMessage(Constant.INTERNAL_SERVER_MESSAGE);
			return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
