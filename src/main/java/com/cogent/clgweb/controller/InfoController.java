package com.cogent.clgweb.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.cogent.clgweb.dto.InfoDetailDTO;
import com.cogent.clgweb.dto.PageInstMpgDTO;
import com.cogent.clgweb.dto.ResponseDTO;
import com.cogent.clgweb.model.CPDocumentMpg;
import com.cogent.clgweb.model.PageInstMpg;
import com.cogent.clgweb.model.registervo;
import com.cogent.clgweb.service.InfoService;
import com.cogent.clgweb.utils.Constant;
import com.cogent.clgweb.utils.ConstantView;

@MultipartConfig
@Controller
@RestController
@RequestMapping("/admin")
public class InfoController {

	@Autowired
	InfoService infoService;
	
	@GetMapping("/info")
	public ModelAndView info(HttpServletRequest request, HttpServletResponse response, Model model) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName(ConstantView.INFO);
		model.addAttribute("title", "Upload Information");
		return mav;
	}
	
	@PostMapping("/info")
	public void addInfo(@ModelAttribute PageInstMpg pageInstMpg, HttpServletRequest request,HttpServletResponse response, 
			@RequestParam("pageInstMpgId") Long Id) 
			throws IOException {
		try {
			HttpSession session=request.getSession();
			registervo rvo = (registervo) session.getAttribute(Constant.SESSION_USER);
			PageInstMpgDTO pageInstMpgDTO=new PageInstMpgDTO();
			BeanUtils.copyProperties(pageInstMpg, pageInstMpgDTO);
			infoService.addDetail(pageInstMpgDTO, rvo,Id);
		}catch (Exception e) {
			e.printStackTrace();
		}
		response.sendRedirect("info");
	}
	
	@PostMapping("/infoDetail")
	public void addInfoDetail(@ModelAttribute CPDocumentMpg cpdm, @RequestParam("infoDoc") MultipartFile file, 
			HttpServletRequest request ,HttpServletResponse response) throws IOException {
		try {
			HttpSession session=request.getSession();
			registervo rvo = (registervo) session.getAttribute(Constant.SESSION_USER);
			infoService.addInfoDetail(cpdm, rvo,file);
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			 response.sendRedirect("info");
		}
	}
	
	@GetMapping("/infoDetai")
	public List<PageInstMpg> pageInstMpgList(@RequestParam("pageId") Long PageId) {
		List<PageInstMpg> list=infoService.pageInstMpgList(PageId);
		return list;
	}
	
	@GetMapping("/infoDetaiList")
	public List<InfoDetailDTO> infoDetaiList(HttpServletRequest request) {
		HttpSession session=request.getSession();
		registervo rvo = (registervo) session.getAttribute(Constant.SESSION_USER);
		List<InfoDetailDTO> list=infoService.infoDetaiList(rvo);
		return list;
	}
	
	@DeleteMapping(value="/info")
	public @ResponseBody ResponseEntity<ResponseDTO> deletePlace(@RequestParam Long Id, 
			HttpServletRequest request,HttpServletResponse response) throws IOException {
		ResponseDTO responseDTO = new ResponseDTO();
		try {
			HttpSession session=request.getSession();
			registervo rvo = (registervo) session.getAttribute(Constant.SESSION_USER);
			responseDTO.setSuccess(Constant.SUCCESS_CODE);
			responseDTO.setServiceResult("Deleted");
			responseDTO.setMessage("Deleted successfully");
			infoService.deleteDetail(Id, rvo);
			return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.OK);
		}catch (Exception e) {
			e.printStackTrace();
			responseDTO.setSuccess(Constant.ERROR_CODE);
			responseDTO.setServiceResult(Constant.INTERNAL_SERVER_SERVICERESULT);
			responseDTO.setMessage(Constant.INTERNAL_SERVER_MESSAGE);
			return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
