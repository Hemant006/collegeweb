package com.cogent.clgweb.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.cogent.clgweb.dto.ResponseDTO;
import com.cogent.clgweb.model.Place;
import com.cogent.clgweb.model.SliderDetail;
import com.cogent.clgweb.service.PlaceService;
import com.cogent.clgweb.service.SliderService;
import com.cogent.clgweb.utils.Constant;
import com.cogent.clgweb.utils.ConstantView;


@RestController
@Controller
@RequestMapping("/admin")
public class SliderController {
	
	Long instituteId=99L;
	Long regId=12345L;	
	
	@Autowired
	SliderService sliderService;
	
	@GetMapping("/slider")
	public ModelAndView test(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName(ConstantView.SLIDER);
		return mav;
	}
	
	@PostMapping("/addslider")
	public void addslider( @RequestParam("fileInput") MultipartFile file, HttpServletResponse response) throws IOException {
		try {
			
				sliderService.addSliderDetail(file);
			
		}catch (Exception e) {
			System.out.println("Exception:- " + e);
		}finally {
			response.sendRedirect("slider");
		}
	}
	
	@GetMapping("/sliderByInst")
	public List<SliderDetail> sliderByInst(HttpServletRequest request, HttpServletResponse response) {
		List<SliderDetail> list=sliderService.sliderByInst(instituteId);
		return list;
	}
	
	@DeleteMapping(value="/slider")
	public @ResponseBody ResponseEntity<ResponseDTO> deleteSlider(@RequestParam Long Id, HttpServletResponse response) throws IOException {
		ResponseDTO responseDTO = new ResponseDTO();
		try {
			responseDTO.setSuccess(Constant.SUCCESS_CODE);
			responseDTO.setServiceResult("Deleted successfully");
			responseDTO.setMessage("Deleted successfully");
			sliderService.deleteSliderDetail(Id);
			return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.OK);
		}catch (Exception e) {
			System.out.println("Exception:- " + e);
			responseDTO.setSuccess(Constant.ERROR_CODE);
			responseDTO.setServiceResult(Constant.INTERNAL_SERVER_SERVICERESULT);
			responseDTO.setMessage(Constant.INTERNAL_SERVER_MESSAGE);
			return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	

}
