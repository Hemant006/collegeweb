package com.cogent.clgweb.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.cogent.clgweb.dto.CollegeDetailDTO;
import com.cogent.clgweb.model.CollegeDetail;
import com.cogent.clgweb.model.registervo;
import com.cogent.clgweb.service.CollegeDetailService;
import com.cogent.clgweb.utils.Constant;
import com.cogent.clgweb.utils.ConstantView;

@Controller
@RestController
@MultipartConfig
@RequestMapping("/admin")
public class CollegeDetailController {

	@Autowired
	CollegeDetailService collegeDetailService;
 	
	@GetMapping("/clgdetail")
	public ModelAndView test(HttpServletRequest request, HttpServletResponse response, 
			Model model) {
		HttpSession session=request.getSession();
		registervo rvo = (registervo) session.getAttribute(Constant.SESSION_USER);
		ModelAndView mav = new ModelAndView();
		mav.setViewName(ConstantView.COLLEGE_DASHBOARD);
		model.addAttribute("title", "Dashboard");
		model.addAttribute("collegeName", rvo.getClgname());
		return mav;
	}
	
	@PostMapping("/clgdetail")
	public void addPlace(@ModelAttribute CollegeDetail collegeDetail, 
			@RequestParam("clgImg") MultipartFile clgImg, @RequestParam("princiaplImg") MultipartFile princiaplImg,
			HttpServletRequest request , HttpServletResponse response, @RequestParam("hiddenId") Long Id) throws IOException {
		try {
			HttpSession session=request.getSession();
			registervo rvo = (registervo) session.getAttribute(Constant.SESSION_USER);
			CollegeDetailDTO collegeDetailDTO=new CollegeDetailDTO();
			BeanUtils.copyProperties(collegeDetail, collegeDetailDTO);
			collegeDetailService.addDetail(collegeDetailDTO, clgImg, princiaplImg, rvo, Id);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			response.sendRedirect("clgdetail");
		}
	}
	
	@GetMapping("/clgdetailByInst")
	public List<CollegeDetail> clgDetailByInst(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session=request.getSession();
		registervo rvo = (registervo) session.getAttribute(Constant.SESSION_USER);
		List<CollegeDetail> list=collegeDetailService.clgDetailByInst(Long.valueOf(rvo.getClg_id()));
		return list;
	}
}
