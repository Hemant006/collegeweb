package com.cogent.clgweb.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Controller
public class DownloadDocumentController {

	@GetMapping("/downloadDoc")
	public void downloadFile(@RequestParam String docPath,@RequestParam String fileName, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		/*
		 * PrintWriter out = response.getWriter(); String rootPath =
		 * System.getProperty("catalina.home"); Path aPath = Paths.get(rootPath); Path
		 * parentsParentpath = aPath.getParent();
		 * response.setContentType("APPLICATION/OCTET-STREAM");
		 * response.setHeader("Content-Disposition", "attachment; filename=\"" + docPath
		 * + "\"");
		 * 
		 * FileInputStream fileInputStream = new FileInputStream(parentsParentpath + "/"
		 * + docPath);
		 * 
		 * int i; while ((i = fileInputStream.read()) != -1) { out.write(i); }
		 * fileInputStream.close(); out.close();
		 */
		
		if(!docPath.equalsIgnoreCase("") && !fileName.equalsIgnoreCase("")) {
			String rootPath = System.getProperty("catalina.home");
			Path aPath = Paths.get(rootPath);
			Path parentsParentpath = aPath.getParent();
			String filepath = String.valueOf(parentsParentpath) + File.separator + docPath;
			filepath = filepath.replace("\\", "/");
			File file = new File(filepath);
			InputStream in = null;
			OutputStream outstream = null;
			int extPosition=filepath.lastIndexOf(".");
			int length=filepath.length();
			String fileExent=filepath.substring(extPosition, length);
			try {
				response.reset();
				in = new FileInputStream(file);
				response.addHeader("content-disposition", "attachment; filename=" + fileName + fileExent);                                
				outstream = response.getOutputStream();
				IOUtils.copyLarge(in, outstream);
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				IOUtils.closeQuietly(outstream);
				IOUtils.closeQuietly(in);
			}
		}
	}
}
