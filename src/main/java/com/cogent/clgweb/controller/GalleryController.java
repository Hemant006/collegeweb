package com.cogent.clgweb.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.cogent.clgweb.dto.ResponseDTO;
import com.cogent.clgweb.model.CategoryMaster;
import com.cogent.clgweb.model.GalleryDetail;
import com.cogent.clgweb.service.GallaryDetailService;
import com.cogent.clgweb.utils.Constant;
import com.cogent.clgweb.utils.ConstantView;

@RestController
@Controller
@RequestMapping("/admin")
public class GalleryController {

	Long instituteId = 99L;
	Long regId = 12345L;

	@Autowired
	GallaryDetailService gallaryservice;

	@GetMapping("/gallery")
	public ModelAndView test(HttpServletRequest request, HttpServletResponse response, Model model) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName(ConstantView.GALLERY);
		model.addAttribute("title", "Gallery");
		return mav;
	}

	@PostMapping("/addcategory")
	public void addCategory(@Valid @ModelAttribute CategoryMaster cm, BindingResult result,
			HttpServletResponse response) throws IOException {
		try {
			System.out.println("resultresult:- " + result);
			if (result.hasErrors()) {
				System.out.println("result:- " + result);
			} else {
				gallaryservice.addCategoryDetail(cm);
			}
		} catch (Exception e) {
			System.out.println("Exception:- " + e);
		} finally {
			response.sendRedirect("gallery");
		}
	}

	@GetMapping("/categoryByInst")
	public List<CategoryMaster> categoryByInst(HttpServletRequest request, HttpServletResponse response) {
		List<CategoryMaster> list = gallaryservice.CategoryDetailByInst(instituteId);
		return list;
	}

	@DeleteMapping(value = "/category")
	public @ResponseBody ResponseEntity<ResponseDTO> deletePlace(@RequestParam Long Id, HttpServletResponse response)
			throws IOException {
		ResponseDTO responseDTO = new ResponseDTO();
		try {
			responseDTO.setSuccess(Constant.SUCCESS_CODE);
			responseDTO.setServiceResult("Deleted successfully");
			responseDTO.setMessage("Deleted successfully");
			gallaryservice.deleteCategoryDetail(Id);
			return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println("Exception:- " + e);
			responseDTO.setSuccess(Constant.ERROR_CODE);
			responseDTO.setServiceResult(Constant.INTERNAL_SERVER_SERVICERESULT);
			responseDTO.setMessage(Constant.INTERNAL_SERVER_MESSAGE);
			return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("/addgallery")
	public void addPlace(@Valid @ModelAttribute GalleryDetail gd, BindingResult result,
			@RequestParam("gallery_path") MultipartFile file, HttpServletResponse response) throws IOException {
		try {
			System.out.println("resultresult:- " + result);
			if (result.hasErrors()) {
				System.out.println("result:- " + result);
			} else {
				gallaryservice.addGalleryDetail(gd, file);
			}
		} catch (Exception e) {
			System.out.println("Exception:- " + e);
		} finally {
			response.sendRedirect("gallery");
		}
	}

	@GetMapping("/galleryDetailByInst")
	public List<GalleryDetail> galleryDetailByInst(HttpServletRequest request, HttpServletResponse response) {
		List<GalleryDetail> list = gallaryservice.galleryDetailByInst(instituteId);
		return list;
	}
	
	@DeleteMapping(value = "/gallery")
	public @ResponseBody ResponseEntity<ResponseDTO> deleteGallery(@RequestParam Long Id, HttpServletResponse response)
			throws IOException {
		ResponseDTO responseDTO = new ResponseDTO();
		try {
			responseDTO.setSuccess(Constant.SUCCESS_CODE);
			responseDTO.setServiceResult("Deleted successfully");
			responseDTO.setMessage("Deleted successfully");
			gallaryservice.deleteGalleryDetail(Id);
			return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println("Exception:- " + e);
			responseDTO.setSuccess(Constant.ERROR_CODE);
			responseDTO.setServiceResult(Constant.INTERNAL_SERVER_SERVICERESULT);
			responseDTO.setMessage(Constant.INTERNAL_SERVER_MESSAGE);
			return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
