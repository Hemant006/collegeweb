package com.cogent.clgweb.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.cogent.clgweb.dto.PublicPortalDataDTO;
import com.cogent.clgweb.dto.SliderDetailDTO;
import com.cogent.clgweb.model.SliderDetail;
import com.cogent.clgweb.repo.PageMasterRepo;
import com.cogent.clgweb.repo.SliderRepo;
import com.cogent.clgweb.request.DepartmentByInstitute;
import com.cogent.clgweb.service.PageService;
import com.cogent.clgweb.service.SliderService;
import com.cogent.clgweb.utils.FileUploadUtill;
import com.cogent.clgweb.utils.Functions;

@Service
@Transactional
public class SliderServiceImpl implements SliderService{

	Long instituteId=99L;
	Long regId=12345L;
	
	@Autowired
	SliderRepo serviceRepo;
	
	@Autowired
	PageMasterRepo pageMasterRepo;
	
	@Override
	public void addSliderDetail(MultipartFile file) {
		try {
			String docPath = FileUploadUtill.uploadFile(file, "Slider");
			SliderDetail pd = new SliderDetail();
			pd.setInstituteId(instituteId);
			pd.setCreatedBy(regId);
			pd.setDocPath(docPath);
			serviceRepo.save(pd);
		} catch (Exception e) {
			System.out.println("Exception:- " + e);
			e.printStackTrace();
		}
	}

	@Override
	public void deleteSliderDetail(Long Id) {
		Optional<SliderDetail> deletePlace = serviceRepo.findById(Id);
		deletePlace.get().setIsDeleted(1);
		deletePlace.get().setIsActive(0);
		deletePlace.get().setDeletedBy(regId);
		deletePlace.get().setDeletedAt(Functions.getCurrentTimeStamp());
		serviceRepo.save(deletePlace.get());
	}

	@Override
	public List<SliderDetail> sliderByInst(Long instituteId) {
		List<SliderDetail> list=serviceRepo.findByinstituteIdAndIsDeleted(instituteId, 0);
		return list;
	}

	@Override
	public PublicPortalDataDTO sliderDTOByInst(Long instituteId) {
		// Main Public DTO.
		PublicPortalDataDTO publicPortalDTOObj = new PublicPortalDataDTO();
		// Slider Details.
		List<SliderDetail> list=serviceRepo.findByinstituteIdAndIsDeleted(instituteId, 0);		
		List<SliderDetailDTO> DTOlist=new ArrayList<SliderDetailDTO>();
		for(SliderDetail sd:list) {
			SliderDetailDTO sdto = new SliderDetailDTO();
			BeanUtils.copyProperties(sd, sdto);			
			DTOlist.add(sdto);
		}
		publicPortalDTOObj.setSliderDetaillist(DTOlist);
		// Department List.
		List<DepartmentByInstitute> departmentByInstitute = pageMasterRepo.departmentByInstitute(22L);
		publicPortalDTOObj.setDepartmentByInstitute(departmentByInstitute);
		return publicPortalDTOObj;
	}
}
