package com.cogent.clgweb.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cogent.clgweb.dto.MenuMpgDTO;
import com.cogent.clgweb.model.MenuInstituteTypeMpg;
import com.cogent.clgweb.model.MenuMaster;
import com.cogent.clgweb.repo.MenuInstituteTypeMpgRepo;
import com.cogent.clgweb.repo.MenuMasterRepo;
import com.cogent.clgweb.request.InstituteDetail;
import com.cogent.clgweb.service.MenuService;

@Service
public class MenuServiceImpl implements MenuService{

	@Autowired
	MenuInstituteTypeMpgRepo menuInstTypeMpgRepo;
	
	@Autowired
	MenuMasterRepo menuMasterRepo;
 	
	@Override
	public List<MenuMpgDTO> menuList(Long instituteTypeId, Long roleId) {
		List<MenuMpgDTO> menuMpgDTOList = new ArrayList<>();
		
		List<MenuInstituteTypeMpg> menuInstTypeMpgList = menuInstTypeMpgRepo.findByInstituteTypeIdAndRoleIdAndIsDeleted(instituteTypeId, roleId, 0);
		
		for(MenuInstituteTypeMpg menuInstituteTypeMpg : menuInstTypeMpgList){
			MenuMpgDTO menuMpgDTO = new MenuMpgDTO();
			Optional<MenuMaster> menuMaster=menuMasterRepo.findById(menuInstituteTypeMpg.getMenuId());
			BeanUtils.copyProperties(menuInstituteTypeMpg, menuMpgDTO);
			menuMpgDTO.setName(menuMaster.get().getName());
			menuMpgDTO.setIcon(menuMaster.get().getIcon());
			menuMpgDTOList.add(menuMpgDTO);
		}
		
		return menuMpgDTOList;
	}

	@Override
	public List<InstituteDetail> instituteDetail(Long instituteId) {
		List<InstituteDetail> list=menuMasterRepo.instDetail(instituteId);
		return list;
	}

}
