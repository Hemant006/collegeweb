package com.cogent.clgweb.serviceImpl;

import java.util.List;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.cogent.clgweb.model.CategoryMaster;
import com.cogent.clgweb.model.GalleryDetail;
import com.cogent.clgweb.repo.CategoryMasterlRepo;
import com.cogent.clgweb.repo.GalleryDetailRepo;
import com.cogent.clgweb.service.GallaryDetailService;
import com.cogent.clgweb.utils.Constant;
import com.cogent.clgweb.utils.FileUploadUtill;
import com.cogent.clgweb.utils.Functions;

@Service
public class GallaryDetailServiceImpl implements GallaryDetailService {

	Long instituteId = 99L;
	Long regId = 12345L;

	@Autowired
	CategoryMasterlRepo CategoryReporepo;

	@Autowired
	GalleryDetailRepo Gallerydetailrepo;

	@Override
	public void addCategoryDetail(CategoryMaster cm) {
		try {
			cm.setInstituteId(instituteId);
			cm.setCreatedBy(regId);
			CategoryReporepo.save(cm);
		} catch (Exception e) {
			System.out.println("Exception:- " + e);
			e.printStackTrace();
		}
	}

	@Override
	public void deleteCategoryDetail(Long Id) {
		Optional<CategoryMaster> deleteCategory = CategoryReporepo.findById(Id);
		deleteCategory.get().setIsDeleted(1);
		deleteCategory.get().setIsActive(0);
		deleteCategory.get().setDeletedBy(regId);
		deleteCategory.get().setDeletedAt(Functions.getCurrentTimeStamp());
		CategoryReporepo.save(deleteCategory.get());
	}

	@Override
	public List<CategoryMaster> CategoryDetailByInst(Long instituteId) {
		List<CategoryMaster> list = CategoryReporepo.findByinstituteIdAndIsDeleted(instituteId, 0);
		return list;
	}

	@Override
	public void addGalleryDetail(GalleryDetail gd, MultipartFile file) {
		try {
			String docPath = FileUploadUtill.uploadFile(file, "Gallery", Constant.IMAGE_FORMAT);
			gd.setInstituteId(instituteId);
			gd.setCreatedBy(regId);
			gd.setDocPath(docPath);
			Gallerydetailrepo.save(gd);
		} catch (Exception e) {
			System.out.println("Exception:- " + e);
			e.printStackTrace();
		}
	}

	@Override
	public List<GalleryDetail> galleryDetailByInst(Long instituteId) {
		List<GalleryDetail> list = Gallerydetailrepo.findByinstituteIdAndIsDeleted(instituteId, 0);
		return list;
	}

	@Override
	public void deleteGalleryDetail(Long Id) {
		Optional<GalleryDetail> deleteGalleryDetail = Gallerydetailrepo.findById(Id);
		deleteGalleryDetail.get().setIsDeleted(1);
		deleteGalleryDetail.get().setIsActive(0);
		deleteGalleryDetail.get().setDeletedBy(regId);
		deleteGalleryDetail.get().setDeletedAt(Functions.getCurrentTimeStamp());
		Gallerydetailrepo.save(deleteGalleryDetail.get());
		
	}

}
