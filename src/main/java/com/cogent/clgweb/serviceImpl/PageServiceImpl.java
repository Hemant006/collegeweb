package com.cogent.clgweb.serviceImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cogent.clgweb.model.PageMaster;
import com.cogent.clgweb.repo.PageMasterRepo;
import com.cogent.clgweb.request.DepartmentByInstitute;
import com.cogent.clgweb.service.PageService;

@Service
@Transactional
public class PageServiceImpl implements PageService{

	@Autowired
	PageMasterRepo pageMasterRepo ; 
	
	@Override
	public List<PageMaster> pageDetail() {
		List<PageMaster> list=pageMasterRepo.findByisDeleted(0);
		return list;
	}

	@Override
	public List<DepartmentByInstitute> deptByInst(Long instituteId) {
		List<DepartmentByInstitute> list=pageMasterRepo.departmentByInstitute(instituteId);
		return list;
	}
}
