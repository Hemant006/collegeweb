package com.cogent.clgweb.serviceImpl;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.cogent.clgweb.model.UploadDocument;
import com.cogent.clgweb.model.registervo;
import com.cogent.clgweb.repo.UploadDocumentRepo;
import com.cogent.clgweb.service.UploadDocumentService;
import com.cogent.clgweb.utils.Constant;
import com.cogent.clgweb.utils.FileUploadUtill;
import com.cogent.clgweb.utils.Functions;

@Service
public class UploadDocumentServiceImpl implements UploadDocumentService{

	@Autowired
	UploadDocumentRepo uploadDocumentRepo;
	
	@Override
	public void addDetail(UploadDocument uploadDoc, MultipartFile file, registervo rvo) {
		try {
			String docPath=FileUploadUtill.uploadFile(file, "Document", Constant.PDF_AND_IMAGE_FORMAT);
			uploadDoc.setDocPath(docPath);
			uploadDoc.setInstituteId(Long.valueOf(rvo.getClg_id()));
			uploadDoc.setCreatedBy(Long.valueOf(rvo.getRegid()));
			uploadDocumentRepo.save(uploadDoc);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<UploadDocument> documentByInstitute(Long instituteId) {
		List<UploadDocument> list=uploadDocumentRepo.findByInstituteIdAndIsDeleted(instituteId,0);
		return list;
	}

	@Override
	public void deleteDetail(Long id, registervo rvo) {
		Optional<UploadDocument> data=uploadDocumentRepo.findById(id);
		data.get().setDeletedAt(Functions.getCurrentTimeStamp());
		data.get().setDeletedBy(Long.valueOf(rvo.getRegid()));
		data.get().setIsDeleted(1);
		data.get().setIsActive(0);
		uploadDocumentRepo.save(data.get());
		
	}

}
