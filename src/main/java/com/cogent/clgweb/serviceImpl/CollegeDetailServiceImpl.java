package com.cogent.clgweb.serviceImpl;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.cogent.clgweb.dto.CollegeDetailDTO;
import com.cogent.clgweb.model.CollegeDetail;
import com.cogent.clgweb.model.registervo;
import com.cogent.clgweb.repo.CollegeDetailRepo;
import com.cogent.clgweb.service.CollegeDetailService;
import com.cogent.clgweb.utils.Constant;
import com.cogent.clgweb.utils.FileUploadUtill;
import com.cogent.clgweb.utils.Functions;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Service
public class CollegeDetailServiceImpl implements CollegeDetailService{

	@Autowired
	CollegeDetailRepo collegeDetailRepo;
 	
	@Override
	public void addDetail(CollegeDetailDTO collegeDetailDTO, MultipartFile clgImg, MultipartFile princiaplImg, registervo rvo,
			Long Id) {
		try {
			CollegeDetail clgDetail=null;
			if(Id==0) {
				clgDetail=new CollegeDetail();
				clgDetail.setCreatedBy(Long.valueOf(rvo.getRegid()));
			}else {
				clgDetail=collegeDetailRepo.getOne(Id);
				clgDetail.setUpdatedAt(Functions.getCurrentTimeStamp());
				clgDetail.setUpdatedBy(Long.valueOf(rvo.getRegid()));
			}
			
			BeanUtils.copyProperties(collegeDetailDTO, clgDetail);
			clgDetail.setId(Id);
			String clgImgPath = FileUploadUtill.uploadFile(clgImg, "College_Detail", Constant.IMAGE_FORMAT);
			String princiaplImgPath = FileUploadUtill.uploadFile(princiaplImg, "College_Detail", Constant.IMAGE_FORMAT);
			clgDetail.setInstituteId(Long.valueOf(rvo.getClg_id()));
			clgDetail.setCollegeImage(clgImgPath);
			clgDetail.setPrincipalImage(princiaplImgPath);
			collegeDetailRepo.save(clgDetail);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public List<CollegeDetail> clgDetailByInst(Long instituteId) {
		List<CollegeDetail> list=collegeDetailRepo.findByInstituteIdAndIsDeleted(instituteId, 0);
		return list;
	}

	
}
