package com.cogent.clgweb.serviceImpl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.cogent.clgweb.dto.CPDocumentMpgDTO;
import com.cogent.clgweb.dto.InfoDetailDTO;
import com.cogent.clgweb.dto.PageInstMpgDTO;
import com.cogent.clgweb.model.CPDocumentMpg;
import com.cogent.clgweb.model.PageInstMpg;
import com.cogent.clgweb.model.PageMaster;
import com.cogent.clgweb.model.registervo;
import com.cogent.clgweb.repo.CPDocumentMpgRepo;
import com.cogent.clgweb.repo.PageInstMpgRepo;
import com.cogent.clgweb.repo.PageMasterRepo;
import com.cogent.clgweb.service.InfoService;
import com.cogent.clgweb.utils.Constant;
import com.cogent.clgweb.utils.FileUploadUtill;
import com.cogent.clgweb.utils.Functions;

@Service
public class InfoServiceImpl implements InfoService {

	@Autowired
	PageInstMpgRepo pageInstMpgRepo;

	@Autowired
	CPDocumentMpgRepo cPDocumentMpgRepo;

	@Autowired
	PageMasterRepo pageMasterRepo;

	@Override
	public void addDetail(PageInstMpgDTO pageInstMpgDTO, registervo rvo, Long Id) {
		try {
			PageInstMpg pageInstMpg=null;
			if(Id==0) {
				pageInstMpg = new PageInstMpg();
				pageInstMpg.setCreatedBy(Long.valueOf(rvo.getRegid()));
			}else {
				pageInstMpg=pageInstMpgRepo.getOne(Id);
				pageInstMpg.setUpdatedAt(Functions.getCurrentTimeStamp());
				pageInstMpg.setUpdatedBy(Long.valueOf(rvo.getRegid()));
			}
			BeanUtils.copyProperties(pageInstMpgDTO, pageInstMpg);
			pageInstMpg.setId(Id);
			pageInstMpg.setInstituteId(Long.valueOf(rvo.getClg_id()));
			pageInstMpgRepo.save(pageInstMpg);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
	}

	@Override
	public void addInfoDetail(CPDocumentMpg cpdm, registervo rvo, MultipartFile file) {
		try {
			String docPath = FileUploadUtill.uploadFile(file, "Information", Constant.IMAGE_FORMAT);
			cpdm.setDocPath(docPath);
			cpdm.setCreatedBy(Long.valueOf(rvo.getRegid()));
			cPDocumentMpgRepo.save(cpdm);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public List<PageInstMpg> pageInstMpgList(Long pageId) {
		List<PageInstMpg> list = pageInstMpgRepo.findAllByPageIdAndIsDeleted(pageId, 0);
		return list;
	}

	@Override
	public List<InfoDetailDTO> infoDetaiList(registervo rvo) {
		List<InfoDetailDTO> instDetailDTOList = new ArrayList<>();

		List<PageInstMpg> pageInstMpgList = pageInstMpgRepo.findByInstituteIdAndIsDeleted(152L,
				0);

		for (PageInstMpg pageInstMpg : pageInstMpgList) {
			InfoDetailDTO InfoDetailDTO = new InfoDetailDTO();
			Optional<PageMaster> pageMasteropt = pageMasterRepo.findById(pageInstMpg.getPageId());
			if (pageMasteropt.isPresent()) {
				List<CPDocumentMpg> cpDocumentMpgList = cPDocumentMpgRepo
						.findByPageInstMpgIdAndIsDeleted(pageInstMpg.getId(), 0);
				List<CPDocumentMpgDTO> cPDocumentMpgDTOList = new ArrayList<>();
				for (CPDocumentMpg cPDocumentMpg1 : cpDocumentMpgList) {
					CPDocumentMpgDTO cpDocumentMpgDTO = new CPDocumentMpgDTO();
					BeanUtils.copyProperties(cPDocumentMpg1, cpDocumentMpgDTO);
					cPDocumentMpgDTOList.add(cpDocumentMpgDTO);
					InfoDetailDTO.setCPDocumentMpgDTOList(cPDocumentMpgDTOList);
				}
			}

			BeanUtils.copyProperties(pageInstMpg, InfoDetailDTO);
			InfoDetailDTO.setPageName(pageMasteropt.get().getName());
			instDetailDTOList.add(InfoDetailDTO);
		}
		return instDetailDTOList;
	}

	@Override
	public void deleteDetail(Long id, registervo rvo) {
		Optional<CPDocumentMpg> cPDocumentMpg = cPDocumentMpgRepo.findById(id);
		cPDocumentMpg.get().setIsActive(0);
		cPDocumentMpg.get().setIsDeleted(1);
		cPDocumentMpg.get().setDeletedBy(Long.valueOf(rvo.getRegid()));
		cPDocumentMpg.get().setDeletedAt(Functions.getCurrentTimeStamp());
		cPDocumentMpgRepo.save(cPDocumentMpg.get());
	}
}
