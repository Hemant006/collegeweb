package com.cogent.clgweb.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cogent.clgweb.model.AcademicCalender;
import com.cogent.clgweb.model.registervo;
import com.cogent.clgweb.repo.AcademicCalenderRepo;
import com.cogent.clgweb.service.AcademicCalenderService;
import com.cogent.clgweb.utils.Functions;

@Service
public class AcademicCalenderServiceImpl implements AcademicCalenderService{

	@Autowired
	AcademicCalenderRepo academicCalenderRepo;
	
	@Override
	public void addDetail(AcademicCalender academicCalender, registervo rvo) {
		academicCalender.setEventDate(Functions.getNormalDateFormat(academicCalender.getEventDate()));
		academicCalender.setCreatedBy(Long.valueOf(rvo.getRegid()));
		academicCalender.setInstituteId(Long.valueOf(rvo.getClg_id()));
		academicCalenderRepo.save(academicCalender);
	}

	@Override
	public List<AcademicCalender> academicCalenderByInst(Long instituteId) {
		List<AcademicCalender> list=academicCalenderRepo.findByInstituteIdAndIsDeleted(instituteId, 0);
		return list;
	}

	@Override
	public void deleteDetail(Long id, registervo rvo) {
		Optional<AcademicCalender> data = academicCalenderRepo.findById(id);
		data.get().setDeletedAt(Functions.getCurrentTimeStamp());
		data.get().setIsDeleted(1);
		data.get().setDeletedBy(Long.valueOf(rvo.getRegid()));
		data.get().setIsActive(0);
		academicCalenderRepo.save(data.get());
	}

}
