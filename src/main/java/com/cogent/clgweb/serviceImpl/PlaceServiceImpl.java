package com.cogent.clgweb.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.cogent.clgweb.model.Place;
import com.cogent.clgweb.model.registervo;
import com.cogent.clgweb.repo.PlaceRepo;
import com.cogent.clgweb.service.PlaceService;
import com.cogent.clgweb.utils.Constant;
import com.cogent.clgweb.utils.FileUploadUtill;
import com.cogent.clgweb.utils.Functions;

@Service
public class PlaceServiceImpl implements PlaceService{

	@Autowired
	PlaceRepo placerepo;
	
	@Override
	public void addDetail(Place pd, MultipartFile file, registervo rvo) {
		try {
			String docPath = FileUploadUtill.uploadFile(file, "Place", Constant.IMAGE_FORMAT);
			pd.setInstituteId(Long.parseLong(rvo.getClg_id()));
			pd.setCreatedBy(Long.valueOf(rvo.getRegid()));
			pd.setDocPath(docPath);
			placerepo.save(pd);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void deleteDetail(Long Id, registervo rvo) {
		Optional<Place> deletePlace = placerepo.findById(Id);
		deletePlace.get().setIsDeleted(1);
		deletePlace.get().setIsActive(0);
		deletePlace.get().setDeletedBy(Long.valueOf(rvo.getRegid()));
		deletePlace.get().setDeletedAt(Functions.getCurrentTimeStamp());
		placerepo.save(deletePlace.get());
	}

	@Override
	public List<Place> placeByInst(Long instituteId) {
		List<Place> list=placerepo.findByinstituteIdAndIsDeleted(instituteId, 0);
		return list;
	}
}
