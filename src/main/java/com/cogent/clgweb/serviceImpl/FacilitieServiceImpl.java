package com.cogent.clgweb.serviceImpl;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.cogent.clgweb.model.Facilitie;
import com.cogent.clgweb.model.registervo;
import com.cogent.clgweb.repo.FacilitieRepo;
import com.cogent.clgweb.request.FacilitiesByInst;
import com.cogent.clgweb.service.FacilitieService;
import com.cogent.clgweb.utils.Constant;
import com.cogent.clgweb.utils.FileUploadUtill;
import com.cogent.clgweb.utils.Functions;

@Service
public class FacilitieServiceImpl implements FacilitieService{

	@Autowired
	FacilitieRepo facilitieRepo;
	
	@Override
	public void addDetail(Facilitie facilitie, MultipartFile file, registervo rvo) {
		try {
			String docPath = FileUploadUtill.uploadFile(file, "Facilitie", Constant.IMAGE_FORMAT);
			facilitie.setDocPath(docPath);
			facilitie.setInstituteId(Long.valueOf(rvo.getClg_id()));
			facilitie.setCreatedBy(Long.valueOf(rvo.getRegid()));
			facilitieRepo.save(facilitie);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public List<FacilitiesByInst> facilitiesByInst(Long instituteId) {
		List<FacilitiesByInst> list=facilitieRepo.facilitiesByInst(instituteId);
		return list;
	}

	@Override
	public void deleteDetail(Long id, registervo rvo) {
		Optional<Facilitie> data = facilitieRepo.findById(id);
		data.get().setDeletedAt(Functions.getCurrentTimeStamp());
		data.get().setIsDeleted(1);
		data.get().setIsActive(0);
		data.get().setDeletedBy(Long.valueOf(rvo.getRegid()));
		facilitieRepo.save(data.get());
		
	}

}
