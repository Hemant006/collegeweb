package com.cogent.clgweb.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cogent.clgweb.model.Program;
import com.cogent.clgweb.model.registervo;
import com.cogent.clgweb.repo.ProgramRepo;
import com.cogent.clgweb.service.ProgramService;
import com.cogent.clgweb.utils.Functions;

@Service
public class ProgramServiceImpl implements ProgramService {

	@Autowired
	ProgramRepo programRepo;

	@Override
	public void addDetail(Program program, registervo rvo) {
		program.setInstituteId(Long.valueOf(rvo.getClg_id()));
		program.setCreatedBy(Long.valueOf(rvo.getRegid()));
		programRepo.save(program);

	}

	@Override
	public void deleteDetail(Long Id, registervo rvo) {
		Optional<Program> program = programRepo.findById(Id);
		program.get().setIsDeleted(1);
		program.get().setIsActive(0);
		program.get().setDeletedBy(Long.valueOf(rvo.getRegid()));
		program.get().setDeletedAt(Functions.getCurrentTimeStamp());
		programRepo.save(program.get());
	}

	@Override
	public List<Program> ProgramByInst(Long instituteId) {
		List<Program> list = programRepo.findByinstituteIdAndIsDeleted(instituteId, 0);
		return list;
	}
}
