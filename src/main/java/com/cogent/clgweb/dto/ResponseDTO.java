package com.cogent.clgweb.dto;

import java.util.Map;

public class ResponseDTO {

	private Integer success;
	private String message;
	private Object serviceResult;
	private Map<String, String> messageList;

	public Map<String, String> getMessageList() {
		return messageList;
	}

	public void setMessageList(Map<String, String> messageList) {
		this.messageList = messageList;
	}

	public Integer getSuccess() {
		return success;
	}

	public void setSuccess(Integer success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getServiceResult() {
		return serviceResult;
	}

	public void setServiceResult(Object serviceResult) {
		this.serviceResult = serviceResult;
	}

}
