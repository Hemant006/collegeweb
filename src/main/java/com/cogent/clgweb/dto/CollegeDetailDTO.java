package com.cogent.clgweb.dto;

public class CollegeDetailDTO {

	private Long Id;
	private Long instituteId;
	private String collegeImage;
	private String principalImage;
	private String course;
	private String googleMapURL;
	private String about;
	private String principalTitle;
	private String principalMsg;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public Long getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Long instituteId) {
		this.instituteId = instituteId;
	}

	public String getCollegeImage() {
		return collegeImage;
	}

	public void setCollegeImage(String collegeImage) {
		this.collegeImage = collegeImage;
	}

	public String getPrincipalImage() {
		return principalImage;
	}

	public void setPrincipalImage(String principalImage) {
		this.principalImage = principalImage;
	}

	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}

	public String getGoogleMapURL() {
		return googleMapURL;
	}

	public void setGoogleMapURL(String googleMapURL) {
		this.googleMapURL = googleMapURL;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public String getPrincipalTitle() {
		return principalTitle;
	}

	public void setPrincipalTitle(String principalTitle) {
		this.principalTitle = principalTitle;
	}

	public String getPrincipalMsg() {
		return principalMsg;
	}

	public void setPrincipalMsg(String principalMsg) {
		this.principalMsg = principalMsg;
	}

}
