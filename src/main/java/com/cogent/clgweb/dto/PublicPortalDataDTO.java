package com.cogent.clgweb.dto;

import java.util.List;

import com.cogent.clgweb.request.DepartmentByInstitute;


public class PublicPortalDataDTO {
	
	List<SliderDetailDTO> SliderDetaillist;
	
	List<DepartmentByInstitute> departmentByInstitute;

	public List<SliderDetailDTO> getSliderDetaillist() {
		return SliderDetaillist;
	}

	public void setSliderDetaillist(List<SliderDetailDTO> sliderDetaillist) {
		SliderDetaillist = sliderDetaillist;
	}

	public List<DepartmentByInstitute> getDepartmentByInstitute() {
		return departmentByInstitute;
	}

	public void setDepartmentByInstitute(List<DepartmentByInstitute> departmentByInstitute) {
		this.departmentByInstitute = departmentByInstitute;
	}
	
}
