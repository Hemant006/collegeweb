package com.cogent.clgweb.dto;


public class SliderDetailDTO {	
	
	private Long instituteId;	
	private String docPath;	
	private Integer seqenceNo;
	public Long getInstituteId() {
		return instituteId;
	}
	public void setInstituteId(Long instituteId) {
		this.instituteId = instituteId;
	}
	public String getDocPath() {
		return docPath;
	}
	public void setDocPath(String docPath) {
		this.docPath = docPath;
	}
	public Integer getSeqenceNo() {
		return seqenceNo;
	}
	public void setSeqenceNo(Integer seqenceNo) {
		this.seqenceNo = seqenceNo;
	}
	
	

}
