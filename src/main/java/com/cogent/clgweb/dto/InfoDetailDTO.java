package com.cogent.clgweb.dto;

import java.util.List;

public class InfoDetailDTO {

	private Long Id; 
	
	private Long instituteId;

	private Long pageId;

	private String description;
	
	private String pageName;
	
	private List<CPDocumentMpgDTO> CPDocumentMpgDTOList;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public Long getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Long instituteId) {
		this.instituteId = instituteId;
	}

	public Long getPageId() {
		return pageId;
	}

	public void setPageId(Long pageId) {
		this.pageId = pageId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<CPDocumentMpgDTO> getCPDocumentMpgDTOList() {
		return CPDocumentMpgDTOList;
	}

	public void setCPDocumentMpgDTOList(List<CPDocumentMpgDTO> cPDocumentMpgDTOList) {
		CPDocumentMpgDTOList = cPDocumentMpgDTOList;
	}

	public String getPageName() {
		return pageName;
	}

	public void setPageName(String pageName) {
		this.pageName = pageName;
	}
	
	
	
}
