package com.cogent.clgweb.dto;

public class CPDocumentMpgDTO {
	
	private Long Id;
	private Long pageInstMpgId;
	private String docPath;
	private String docTitle;

	public Long getPageInstMpgId() {
		return pageInstMpgId;
	}

	public void setPageInstMpgId(Long pageInstMpgId) {
		this.pageInstMpgId = pageInstMpgId;
	}

	public String getDocPath() {
		return docPath;
	}

	public void setDocPath(String docPath) {
		this.docPath = docPath;
	}

	public String getDocTitle() {
		return docTitle;
	}

	public void setDocTitle(String docTitle) {
		this.docTitle = docTitle;
	}

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

}
